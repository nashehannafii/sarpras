<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Ruangan;

/**
 * RuanganSearch represents the model behind the search form of `app\models\Ruangan`.
 */
class RuanganSearch extends Ruangan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'gedung_id', 'kapasitas', 'status_aktif'], 'integer'],
            [['nama', 'keterangan'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Ruangan::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'gedung_id' => $this->gedung_id,
            'kapasitas' => $this->kapasitas,
            'status_aktif' => $this->status_aktif,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }

    public function searchBeruntun($params, $cekBeruntun = null)
    {
        $query = Ruangan::find();

        // add conditions that should always apply here

        // CEK PEMINJAMAN BERUNTUN
        if (isset($cekBeruntun)) {
            $query->andWhere(['gedung_id' => $cekBeruntun['gedung']]);

            $Ruangan_id = null;
            foreach (Peminjaman::findBygedung($cekBeruntun)->all() as $booked) {
                $Ruangan_id = $booked->Ruangan_id;
            }

            $query->andWhere(['not', ['id' => $Ruangan_id]]);

        } else {
            $query->where(['id' => 0]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'gedung_id' => $this->gedung_id,
            'kapasitas' => $this->kapasitas,
            'status_aktif' => $this->status_aktif,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'keterangan', $this->keterangan]);

        return $dataProvider;
    }
}
