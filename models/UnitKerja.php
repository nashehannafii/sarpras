<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "unit_kerja".
 *
 * @property int $id
 * @property int|null $parent_id
 * @property int|null $jenjang_id
 * @property int|null $user_id
 * @property int|null $is_sent
 * @property string|null $jenis
 * @property string $nama
 * @property string|null $singkatan
 * @property string|null $surat
 * @property string|null $kode_prodi
 * @property string|null $email
 * @property string|null $penanggung_jawab
 *
 * @property User $user
 * @property User[] $users
 */
class UnitKerja extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'unit_kerja';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parent_id', 'jenjang_id', 'user_id', 'is_sent'], 'integer'],
            [['nama'], 'required'],
            [['jenis', 'nama'], 'string', 'max' => 100],
            [['singkatan'], 'string', 'max' => 20],
            [['surat'], 'string', 'max' => 5],
            [['kode_prodi'], 'string', 'max' => 10],
            [['email', 'penanggung_jawab'], 'string', 'max' => 150],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'parent_id' => Yii::t('app', 'Parent ID'),
            'jenjang_id' => Yii::t('app', 'Jenjang ID'),
            'user_id' => Yii::t('app', 'User ID'),
            'is_sent' => Yii::t('app', 'Is Sent'),
            'jenis' => Yii::t('app', 'Type'),
            'nama' => Yii::t('app', 'Name'),
            'singkatan' => Yii::t('app', 'Abbreviation'),
            'surat' => Yii::t('app', 'Letter'),
            'kode_prodi' => Yii::t('app', 'Prodi Code'),
            'email' => Yii::t('app', 'Email'),
            'penanggung_jawab' => Yii::t('app', 'Person Responsible'),
        ];
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }

    /**
     * Gets query for [[Users]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::class, ['unit_kerja_id' => 'id']);
    }
}
