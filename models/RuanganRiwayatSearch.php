<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\RuanganRiwayat;

/**
 * RuanganRiwayatSearch represents the model behind the search form of `app\models\RuanganRiwayat`.
 */
class RuanganRiwayatSearch extends RuanganRiwayat
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ruangan_id', 'kapasitas', 'status_aktif'], 'integer'],
            [['no_ruangan', 'nama', 'gambar_path', 'kapasitas', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $id = null)
    {
        $query = RuanganRiwayat::find();

        // add conditions that should always apply here
        $query->orderBy(['id' => SORT_DESC]);
        if ($id != null) {
            $query->andWhere(['ruangan_id' => $id]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'ruangan_id' => $this->ruangan_id,
            'kapasitas' => $this->kapasitas,
            'status_aktif' => $this->status_aktif,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'no_ruangan', $this->no_ruangan])
            ->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'gambar_path', $this->gambar_path]);

        return $dataProvider;
    }
}
