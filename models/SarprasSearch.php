<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Sarpras;

/**
 * SarprasSearch represents the model behind the search form of `app\models\Sarpras`.
 */
class SarprasSearch extends Sarpras
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'durasi_autodelete'], 'integer'],
            [['kepala_sarpras', 'peraturan_umum', 'jam_autocancel', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Sarpras::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'jam_autocancel' => $this->jam_autocancel,
            'durasi_autodelete' => $this->durasi_autodelete,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'kepala_sarpras', $this->kepala_sarpras])
            ->andFilterWhere(['like', 'peraturan_umum', $this->peraturan_umum]);

        return $dataProvider;
    }
}
