<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gedung".
 *
 * @property int $id
 * @property string $nama
 * @property string $keterangan
 * @property int $status_aktif
 * @property string|null $gambar_path
 *
 * @property Item[] $items
 */
class gedung extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'gedung';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nama', 'keterangan', 'status_aktif'], 'required'],
            [['status_aktif'], 'integer'],
            [['nama'], 'string', 'max' => 50],
            [['keterangan'], 'string', 'max' => 500],
            [['gambar_path'], 'string', 'max' => 255],
            [['gambar_path'], 'file', 'skipOnEmpty' => true, 'extensions' => ['png', 'jpg'], 'maxSize' => 1024 * 1024 * 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'nama' => Yii::t('app', 'Name'),
            'keterangan' => Yii::t('app', 'Description'),
            'status_aktif' => Yii::t('app', 'Active Status'),
            'gambar_path' => Yii::t('app', 'Image'),
        ];
    }

    /**
     * Gets query for [[Items]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRuangans()
    {
        return $this->hasMany(Ruangan::class, ['gedung_id' => 'id']);
    }
}
