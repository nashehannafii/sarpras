<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ruangan".
 *
 * @property int $id
 * @property int $gedung_id
 * @property string $nama
 * @property int $kapasitas
 * @property string $keterangan
 * @property int $status_aktif
 * @property string|null $gambar_path
 * @property int|null $status_id
 *
 * @property gedung $gedung
 * @property Label[] $labels
 * @property StatusRuangan $status 
 */
class Ruangan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ruangan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['gedung_id', 'nama', 'kapasitas', 'keterangan', 'status_aktif'], 'required'],
            [['gedung_id', 'kapasitas', 'status_aktif', 'status_id'], 'integer'],
            [['nama'], 'string', 'max' => 50],
            [['keterangan'], 'string', 'max' => 500],
            [['gambar_path'], 'string', 'max' => 255],
            [['gedung_id'], 'exist', 'skipOnError' => true, 'targetClass' => gedung::class, 'targetAttribute' => ['gedung_id' => 'id']],
            [['gambar_path'], 'file', 'skipOnEmpty' => true, 'extensions' => ['png', 'jpg'], 'maxSize' => 1024 * 1024 * 5],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => StatusItem::class, 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'gedung_id' => Yii::t('app', 'Parent'),
            'nama' => Yii::t('app', 'Room Name'),
            'kapasitas' => Yii::t('app', 'Capacity'),
            'keterangan' => Yii::t('app', 'Description'),
            'status_aktif' => Yii::t('app', 'Active Status'),
            'gambar_path' => Yii::t('app', 'Image'),
            'status_id' => Yii::t('app', 'Item Status'),
        ];
    }

    public static function findReadyItem($tanggal, $gedung_id = null)
    {
        $items = Ruangan::find()
            ->alias('i');
        if ($gedung_id != null) {
            # code...
            $items->where(['i.gedung_id' => $gedung_id]);
        }
        $items = $items->andWhere([
            'not exists',
            PeminjamanRuangan::find()
                ->joinWith(['peminjaman as p'])
                ->alias('pi')
                ->innerJoinWith(['ruanganRiwayat r'])
                ->where(['r.ruangan_id' => new \yii\db\Expression('i.id')])
                ->andWhere(['p.status_peminjaman' => 1])
                ->andWhere(['tanggal_pinjam' => $tanggal])
        ])
            ->all();

        return $items;
    }

    /**
     * Gets query for [[gedung]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getgedung()
    {
        return $this->hasOne(gedung::class, ['id' => 'gedung_id']);
    }

    /**
     * Gets query for [[Labels]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getLabels()
    {
        return $this->hasMany(RuanganRiwayat::class, ['ruangan_id' => 'id']);
    }

    /** 
     * Gets query for [[StatusItems]]. 
     * 
     * @return \yii\db\ActiveQuery 
     */
    public function getStatus()
    {
        return $this->hasOne(StatusItem::class, ['id' => 'status_id']);
    }
}
