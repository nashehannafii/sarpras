<?php

namespace app\models;

use app\helpers\MyHelper;
use Yii;

/**
 * This is the model class for table "peminjaman_ruangan".
 *
 * @property int $id
 * @property int $peminjaman_id
 * @property string $tanggal_pinjam
 * @property int $mulai_id
 * @property int $selesai_id
 * @property int $ruangan_riwayat_id 
 * 
 * @property Label $label
 * @property Jam $mulai
 * @property Jam $selesai
 * @property Peminjaman $peminjaman
 */
class PeminjamanRuangan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'peminjaman_ruangan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['peminjaman_id', 'tanggal_pinjam', 'mulai_id', 'selesai_id', 'ruangan_riwayat_id'], 'required'],
            [['peminjaman_id', 'mulai_id', 'selesai_id', 'ruangan_riwayat_id'], 'integer'],
            [['tanggal_pinjam'], 'safe'],
            [['ruangan_riwayat_id'], 'exist', 'skipOnError' => true, 'targetClass' => RuanganRiwayat::class, 'targetAttribute' => ['ruangan_riwayat_id' => 'id']],
            [['peminjaman_id'], 'exist', 'skipOnError' => true, 'targetClass' => Peminjaman::class, 'targetAttribute' => ['peminjaman_id' => 'id']],
            [['mulai_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jam::class, 'targetAttribute' => ['mulai_id' => 'id']],
            [['selesai_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jam::class, 'targetAttribute' => ['selesai_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app',  'ID'),
            'peminjaman_id' => Yii::t('app', 'Booking ID'),
            'tanggal_pinjam' => Yii::t('app', 'Booking Date'),
            'ruangan_riwayat_id' => Yii::t('app', 'Label'),
            'mulai_id' => Yii::t('app', 'Start Time'),
            'selesai_id' => Yii::t('app', 'End Time'),
        ];
    }


    public static function findPeminjaman($ruangan_id, $tanggal_pinjam, $status_peminjaman = null)
    {
        $status_peminjaman = $status_peminjaman != null ? $status_peminjaman : 1;
        return PeminjamanRuangan::find()
            ->joinWith(['ruanganRiwayat as rr', 'peminjaman as p'])
            ->andWhere(['rr.ruangan_id' => $ruangan_id])
            ->andWhere(['p.status_peminjaman' => $status_peminjaman])
            ->andWhere(['tanggal_pinjam' => $tanggal_pinjam]);
    }

    public static function findSingle($peminjaman_id)
    {
        return PeminjamanRuangan::find()->where(['peminjaman_id' => $peminjaman_id])->one();
    }

    public static function findItemLabelPeminjaman($peminjaman_id)
    {
        $ruangan_riwayat_id = PeminjamanRuangan::findSingle($peminjaman_id)->ruangan_riwayat_id;
        return RuanganRiwayat::findOne($ruangan_riwayat_id);
    }

    public static function findSpesifikItem($ruangan_riwayat_id, $tanggal_pinjam, $peminjaman_id, $mulai_id, $selesai_id)
    {
        return PeminjamanRuangan::findOne(['ruangan_riwayat_id' => $ruangan_riwayat_id, 'tanggal_pinjam' => $tanggal_pinjam, 'peminjaman_id' => $peminjaman_id, 'mulai_id' => $mulai_id, 'selesai_id' => $selesai_id]);
    }

    /** 
     * Gets query for [[Label]]. 
     * 
     * @return \yii\db\ActiveQuery 
     */
    public function getRuanganRiwayat()
    {
        return $this->hasOne(RuanganRiwayat::class, ['id' => 'ruangan_riwayat_id']);
    }

    /**
     * Gets query for [[Peminjaman]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPeminjaman()
    {
        return $this->hasOne(Peminjaman::class, ['id' => 'peminjaman_id']);
    }

    /** 
     * Gets query for [[Mulai]]. 
     * 
     * @return \yii\db\ActiveQuery 
     */
    public function getMulai()
    {
        return $this->hasOne(Jam::class, ['id' => 'mulai_id']);
    }

    /** 
     * Gets query for [[Selesai]]. 
     * 
     * @return \yii\db\ActiveQuery 
     */
    public function getSelesai()
    {
        return $this->hasOne(Jam::class, ['id' => 'selesai_id']);
    }
}
