<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Peminjaman;

/**
 * PeminjamanSearch represents the model behind the search form of `app\models\Peminjaman`.
 */
class PeminjamanSearch extends Peminjaman
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'user_id', 'status_peminjaman', 'jenis_peminjaman'], 'integer'],
            [['nama_acara', 'tema_acara', 'deskripsi_acara', 'ketua_acara', 'nomor_surat', 'created_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $onlySelf = null, $status_peminjaman = null)
    {
        $query = Peminjaman::find();

        // add conditions that should always apply here
        $query->andWhere(['temp' => '-']);

        if ($onlySelf) {
            $query->andWhere([
                'user_id' => Yii::$app->user->identity->id,
            ]);
        }

        if ($status_peminjaman) {
            $query->andWhere([
                'status_peminjaman' => $status_peminjaman,
            ]);
        }

        $query->orderBy(['id' => SORT_DESC]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'user_id' => $this->user_id,
            'status_peminjaman' => $this->status_peminjaman,
            'jenis_peminjaman' => $this->jenis_peminjaman,
            'created_at' => $this->created_at,
        ]);

        $query->andFilterWhere(['like', 'nama_acara', $this->nama_acara])
            ->andFilterWhere(['like', 'tema_acara', $this->tema_acara])
            ->andFilterWhere(['like', 'deskripsi_acara', $this->deskripsi_acara])
            ->andFilterWhere(['like', 'ketua_acara', $this->ketua_acara])
            ->andFilterWhere(['like', 'nomor_surat', $this->nomor_surat]);

        return $dataProvider;
    }

}
