<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "peminjaman".
 *
 * @property int $id
 * @property int $user_id
 * @property int|null $status_peminjaman
 * @property string|null $nama_acara
 * @property string|null $tema_acara
 * @property string|null $deskripsi_acara
 * @property string|null $ketua_acara
 * @property int|null $jenis_peminjaman
 * @property string|null $nomor_surat
 * @property string|null $tanggal_mulai
 * @property int|null $is_sent
 * @property string|null $created_at
 * @property string|null $temp
 *
 * @property PeminjamanItem[] $peminjamanItems
 * @property User $user
 */
class Peminjaman extends \yii\db\ActiveRecord
{
    public $mulai;
    public $selesai;
    public $item;
    public $tanggal_pinjam;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'peminjaman';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'status_peminjaman', 'jenis_peminjaman', 'is_sent', 'mulai', 'selesai', 'item'], 'integer'],
            [['tanggal_mulai', 'created_at', 'tanggal_pinjam'], 'safe'],
            [['nama_acara', 'tema_acara', 'ketua_acara'], 'string', 'max' => 150],
            [['deskripsi_acara'], 'string', 'max' => 700],
            [['nomor_surat'], 'string', 'max' => 30],
            [['temp'], 'string', 'max' => 8000],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */

    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'user_id' => Yii::t('app', 'User'),
            'status_peminjaman' => Yii::t('app', 'Booking Status'),
            'nama_acara' => Yii::t('app', 'Event Name'),
            'tema_acara' => Yii::t('app', 'Event Theme'),
            'deskripsi_acara' => Yii::t('app', 'Event Description'),
            'ketua_acara' => Yii::t('app', 'Event Chairman'),
            'jenis_peminjaman' => Yii::t('app', 'Booking Type'),
            'nomor_surat' => Yii::t('app', 'Reference Letter Number'),
            'tanggal_mulai' => Yii::t('app', 'Booking Date Start'),
            'is_sent' => Yii::t('app', 'Is Sent'),
            'mulai' => Yii::t('app', 'Start Time'),
            'selesai' => Yii::t('app', 'End Time'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public static function findBygedung($gedung_id, $tanggal_mulai = null, $tanggal_selesai = null)
    {
        $query = Peminjaman::find()
            ->joinWith(['peminjamanItems as p', 'peminjamanItems.ruanganRiwayat as l', 'peminjamanItems.ruanganRiwayat.ruangan as i'])
            ->andWhere(['status_peminjaman' => 1])
            ->andWhere(['i.gedung_id' => $gedung_id]);

        if ($tanggal_mulai != null && $tanggal_selesai != null) $query->andFilterWhere(['between', 'p.tanggal_pinjam', $tanggal_mulai, $tanggal_selesai]);

        return $query;
    }

    public static function findByItem($ruangan_id, $tanggal, $status = 1)
    {
        $query = Peminjaman::find()
            ->joinWith(['peminjamanItems as pi', 'peminjamanItems.ruanganRiwayat as rr'])
            ->andWhere(['status_peminjaman' => $status])
            ->andWhere(['pi.tanggal_pinjam' => $tanggal])
            ->andWhere(['rr.ruangan_id' => $ruangan_id]);

        return $query;
    }

    public static function checkUserPeminjaman($user_id)
    {
        $query = Peminjaman::find()
            ->where(['user_id' => $user_id])
            ->andWhere(['not', ['temp' => '-']])
            ->orderBy(['id' => SORT_DESC])
            ->one();

        return $query;
    }
    /**
     * Gets query for [[PeminjamanItems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPeminjamanItems()
    {
        return $this->hasMany(PeminjamanRuangan::class, ['peminjaman_id' => 'id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::class, ['id' => 'user_id']);
    }
}
