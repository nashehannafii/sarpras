<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ruangan_riwayat".
 *
 * @property int $id
 * @property int $ruangan_id
 * @property string $no_ruangan
 * @property string $nama
 * @property int $kapasitas
 * @property string|null $gambar_path
 * @property int $status_aktif
 * @property string|null $created_at
 *
 * @property PeminjamanRuangan[] $peminjamanRuangans
 * @property Ruangan $ruangan
 */
class RuanganRiwayat extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ruangan_riwayat';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ruangan_id', 'no_ruangan', 'nama', 'kapasitas', 'status_aktif'], 'required'],
            [['ruangan_id', 'kapasitas', 'status_aktif'], 'integer'],
            [['created_at'], 'safe'],
            [['no_ruangan'], 'string', 'max' => 50],
            [['nama'], 'string', 'max' => 150],
            [['gambar_path'], 'string', 'max' => 255],
            [['ruangan_id'], 'exist', 'skipOnError' => true, 'targetClass' => Ruangan::class, 'targetAttribute' => ['ruangan_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app',  'ID'),
            'ruangan_id' => Yii::t('app', 'Ruangan ID'),
            'nama' => Yii::t('app', 'Label Name'),
            'no_ruangan' => Yii::t('app', 'No Ruangan'),
            'kapasitas' => Yii::t('app', 'Capacity'),
            'gambar_path' => Yii::t('app', 'Gambar Path'),
            'status_aktif' => Yii::t('app', 'Active Status'),
            'status_aktif' => Yii::t('app', 'Status Aktif'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }

    public static function findActive($ruangan_id)
    {
        return self::find()->where([
            'status_aktif' => 1,
            'ruangan_id' => $ruangan_id,
        ]);
    }

    /**
     * Gets query for [[PeminjamanRuangans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPeminjamanRuangans()
    {
        return $this->hasMany(PeminjamanRuangan::class, ['ruangan_riwayat_id' => 'id']);
    }

    /**
     * Gets query for [[Ruangan]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRuangan()
    {
        return $this->hasOne(Ruangan::class, ['id' => 'ruangan_id']);
    }
}
