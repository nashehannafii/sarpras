<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "jam".
 *
 * @property int $id
 * @property int $urutan
 * @property string $jam
 *
 * @property Peminjaman[] $peminjaman
 * @property Peminjaman[] $peminjaman0
 */
class Jam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jam';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['urutan', 'jam'], 'required'],
            [['urutan'], 'integer'],
            [['jam'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'urutan' => Yii::t('app', 'Sequence'),
            'jam' => Yii::t('app', 'Time'),
        ];
    }

    /**
     * Gets query for [[peminjaman]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getpeminjaman()
    {
        return $this->hasMany(Peminjaman::className(), ['mulai_id' => 'id']);
    }

    /**
     * Gets query for [[peminjaman0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getpeminjaman0()
    {
        return $this->hasMany(Peminjaman::className(), ['selesai_id' => 'id']);
    }
}
