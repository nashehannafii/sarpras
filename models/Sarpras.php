<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sarpras".
 *
 * @property int $id
 * @property string|null $kepala_sarpras
 * @property string|null $peraturan_umum
 * @property string|null $jam_autocancel
 * @property int|null $durasi_autodelete
 * @property string|null $created_at
 */
class Sarpras extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sarpras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['jam_autocancel', 'created_at'], 'safe'],
            [['durasi_autodelete'], 'integer'],
            [['kepala_sarpras'], 'string', 'max' => 150],
            [['peraturan_umum'], 'string', 'max' => 1500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kepala_sarpras' => Yii::t('app', 'Berau Chief'),
            'peraturan_umum' => Yii::t('app', 'General Rules'),
            'jam_autocancel' => Yii::t('app', 'Auto cancel time'),
            'durasi_autodelete' => Yii::t('app', 'Auto delete cart duration'),
            'created_at' => Yii::t('app', 'Created At'),
        ];
    }
}
