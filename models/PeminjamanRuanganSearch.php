<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\PeminjamanRuangan;

/**
 * PeminjamanRuanganSearch represents the model behind the search form of `app\models\PeminjamanRuangan`.
 */
class PeminjamanRuanganSearch extends PeminjamanRuangan
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'peminjaman_id', 'mulai_id', 'selesai_id', 'ruangan_riwayat_id'], 'integer'],
            [['tanggal_pinjam'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $ruangan_id = null, $tanggalMulai = null, $tanggalSelesai = null)
    {
        $query = PeminjamanRuangan::find();

        // add conditions that should always apply here

        if ($ruangan_id != null) {
            $query->joinWith('ruanganRiwayat as rr')
                ->orderBy(['tanggal_pinjam' => SORT_ASC])
                ->where(['rr.ruangan_id' => $ruangan_id]);
        }

        if ($tanggalMulai != null && $tanggalSelesai != null)
            $query->andWhere(['between', 'tanggal_pinjam', $tanggalMulai, $tanggalSelesai]);


        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'peminjaman_id' => $this->peminjaman_id,
            'tanggal_pinjam' => $this->tanggal_pinjam,
            'mulai_id' => $this->mulai_id,
            'selesai_id' => $this->selesai_id,
            'ruangan_riwayat_id' => $this->ruangan_riwayat_id,
        ]);

        return $dataProvider;
    }
}
