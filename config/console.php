<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\commands',
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
        '@tests' => '@app/tests',
    ],
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'queue' => [
            'class' => 'yii\queue\file\Queue',
            'path' => '@runtime/queue',
        ],
        'scheduler' => [
            'class' => 'yii\scheduler\Scheduler',
            'tasks' => [
                [
                    'name' => 'my-task',
                    'cron' => '*/5 * * * *', // Runs every 5 minute
                    'route' => 'my-task/my-task', // The route to the console command action
                ],
                // Add more tasks for other intervals as needed
            ],
        ],
    ],
    'params' => $params,
    'controllerMap' => [
        'cron' => 'app\commands\CronController',
    ],

    /*
    'controllerMap' => [
        'fixture' => [ // Fixture generation command line.
            'class' => 'yii\faker\FixtureController',
        ],
    ],
    */
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
