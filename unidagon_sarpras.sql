-- MySQL dump 10.13  Distrib 8.0.30, for Win64 (x86_64)
--
-- Host: localhost    Database: unidagon_sarpras
-- ------------------------------------------------------
-- Server version	8.0.30

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_assignment`
--

DROP TABLE IF EXISTS `auth_assignment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_assignment` (
  `item_name` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `user_id` int NOT NULL,
  `created_at` int DEFAULT NULL,
  PRIMARY KEY (`item_name`,`user_id`),
  KEY `idx-auth_assignment-user_id` (`user_id`),
  CONSTRAINT `auth_assignment_ibfk_1` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`),
  CONSTRAINT `auth_assignment_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`),
  CONSTRAINT `fk_auth_assignment_item_name` FOREIGN KEY (`item_name`) REFERENCES `auth_item` (`name`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_auth_assignment_user_id` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_assignment`
--

LOCK TABLES `auth_assignment` WRITE;
/*!40000 ALTER TABLE `auth_assignment` DISABLE KEYS */;
INSERT INTO `auth_assignment` VALUES ('admin',21,1687499532),('theCreator',1,NULL),('warek',28,1689260974);
/*!40000 ALTER TABLE `auth_assignment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item`
--

DROP TABLE IF EXISTS `auth_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_item` (
  `name` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `type` smallint NOT NULL,
  `description` text CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci,
  `rule_name` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci DEFAULT NULL,
  `data` blob,
  `created_at` int DEFAULT NULL,
  `updated_at` int DEFAULT NULL,
  PRIMARY KEY (`name`),
  KEY `rule_name` (`rule_name`),
  KEY `idx-auth_item-type` (`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item`
--

LOCK TABLES `auth_item` WRITE;
/*!40000 ALTER TABLE `auth_item` DISABLE KEYS */;
INSERT INTO `auth_item` VALUES ('admin',1,'ADMIN MASTER',NULL,NULL,1616483344,1616483344),('mahasiswa',1,'student',NULL,NULL,NULL,NULL),('theCreator',1,'Super Admin',NULL,NULL,1616483344,1616483344),('unitKerja',1,'unit bagian',NULL,NULL,NULL,NULL),('warek',1,'Wakil Rektor',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `auth_item` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_item_child`
--

DROP TABLE IF EXISTS `auth_item_child`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_item_child` (
  `parent` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `child` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  PRIMARY KEY (`parent`,`child`),
  KEY `child` (`child`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_item_child`
--

LOCK TABLES `auth_item_child` WRITE;
/*!40000 ALTER TABLE `auth_item_child` DISABLE KEYS */;
INSERT INTO `auth_item_child` VALUES ('theCreator','admin'),('theCreator','mahasiswa'),('theCreator','unitKerja');
/*!40000 ALTER TABLE `auth_item_child` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_rule`
--

DROP TABLE IF EXISTS `auth_rule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `auth_rule` (
  `name` varchar(64) CHARACTER SET utf8mb3 COLLATE utf8mb3_unicode_ci NOT NULL,
  `data` blob,
  `created_at` int DEFAULT NULL,
  `updated_at` int DEFAULT NULL,
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8mb3_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_rule`
--

LOCK TABLES `auth_rule` WRITE;
/*!40000 ALTER TABLE `auth_rule` DISABLE KEYS */;
INSERT INTO `auth_rule` VALUES ('isAuthor',_binary 'O:25:\"app\\rbac\\rules\\AuthorRule\":3:{s:4:\"name\";s:8:\"isAuthor\";s:9:\"createdAt\";i:1663587848;s:9:\"updatedAt\";i:1663587848;}',1663587848,1663587848);
/*!40000 ALTER TABLE `auth_rule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migration`
--

DROP TABLE IF EXISTS `migration`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migration`
--

LOCK TABLES `migration` WRITE;
/*!40000 ALTER TABLE `migration` DISABLE KEYS */;
INSERT INTO `migration` VALUES ('m000000_000000_base',1668528329),('m221111_144133_create_auth_item_child_table',1668528337),('m221115_150802_create_kelompok_table',1668528337),('m221115_152804_create_ruangan_table',1668528337),('m221115_152815_create_logistik_table',1668528337),('m221115_152830_create_peminjaman_table',1668528337),('m221115_152859_create_praktikum_table',1668528337),('m221115_153748_create_jenis_table',1668528337),('m221115_153757_create_satuan_table',1668528337),('m221218_033448_create_tempat_table',1672150109),('m221218_034129_create_dosen_table',1672150109),('m221218_034906_create_judul_table',1672150109),('m221218_035403_create_data_prodi_table',1672150109),('m221218_035902_create_ruangan_table',1672150110),('m221218_043010_create_barang_pinjam_table',1672150110),('m221220_075133_create_peminjaman_table',1672150110),('m221220_142130_create_barang_pinjam_table',1672150110),('m221220_170503_create_barang_pinjam_table',1672150110),('m221221_112538_add_foreign_key',1672150111),('m221221_121907_drop_column',1672150111),('m221221_123917_add_column_peminjaan',1672150111),('m221221_174958_create_barang_pinjam_table',1672150112),('m221222_021759_drop_columns',1672150112),('m221222_035941_table_barang_pinjam',1672150112),('m221227_135754_create_nama_gedung_table',1672150112),('m221227_141100_addColumn_no',1672150436),('m221227_141851_dropColumn_no',1672150768),('m221227_144900_create_bahan_table',1672152662),('m230103_030611_create_bahan_coba_table',1672715298),('m230103_043530_create_alat_table',1672807076),('m230103_044558_create_ruangan_table',1672895330),('m230103_163805_add_foreign_key',1672895674),('m230103_170136_create_gedung_table',1672906252),('m230104_040850_create_satuanr_table',1672805509),('m230105_161411_create_pegawai_table',1672935354),('m230105_162320_add_pegawai_colomn',1672935849),('m230105_173900_add_pegawai_colomn',1672940401),('m230105_175402_add_pegawai_colomn',1672941334),('m230105_175640_add_pegawai_colomn',1672943699),('m230105_184531_add_user_column',1672944519),('m230105_190518_create_biaya_penelitian_table',1672945552),('m230105_191306_create_responsible_table',1673031334),('m230106_190232_create_gedung_table',1673031785),('m230106_191349_create_anggota_table',1673032552),('m230106_191927_add_peminjaman_column',1673083345),('m230107_092305_add_peminjaman_column',1673083474),('m230108_032128_create_jenis_kegiatan_table',1673148178),('m230613_034327_add_column',1686627912),('m230613_035252_Relations',1686629650),('m230613_035704_Relations',1686629650),('m230615_071207_create_peminjaman_table',1686814656),('m230615_124832_full_migration',1687001190),('m230619_110458_full_migration',1687425114);
/*!40000 ALTER TABLE `migration` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `session`
--

DROP TABLE IF EXISTS `session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `session` (
  `id` char(40) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `expire` int DEFAULT NULL,
  `data` blob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `session`
--

LOCK TABLES `session` WRITE;
/*!40000 ALTER TABLE `session` DISABLE KEYS */;
INSERT INTO `session` VALUES ('2ism172t1d5nsh4nhir1p6t074',1689314488,_binary '__flash|a:0:{}'),('7bra4mhh9h7i3af91bgarrj0c1',1689579632,_binary '__flash|a:0:{}__id|i:1;__authKey|s:32:\"HucjOcfeu3YVm8hSQYIXVn8smHtYTFxP\";__expire|i:1690098032;'),('blv796c2rcc96o2sd8cn31sahq',1689560561,_binary '__flash|a:0:{}'),('jqg1ekercq2tsnkckkhcjh246r',1689314488,_binary '__flash|a:0:{}');
/*!40000 ALTER TABLE `session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) DEFAULT NULL,
  `uuid` varchar(255) DEFAULT NULL,
  `nama` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `auth_key` varchar(32) DEFAULT NULL,
  `password_hash` varchar(255) NOT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `account_activation_token` varchar(255) DEFAULT NULL,
  `otp` varchar(255) DEFAULT NULL,
  `status` varchar(20) NOT NULL,
  `access_role` varchar(255) DEFAULT NULL,
  `created_at` int NOT NULL,
  `updated_at` int NOT NULL,
  `nim` int DEFAULT NULL,
  `kontak` int DEFAULT NULL,
  `unit_kerja_id` int DEFAULT NULL,
  `lang` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_unit_kerja` (`unit_kerja_id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'superadmin','591deb6d-f126-4208-857e-bdb5de8dda95','Super Admin','pptik@unida.gontor.ac.id','HucjOcfeu3YVm8hSQYIXVn8smHtYTFxP','$2a$13$mRUypj2h/dQjFA4nLHRb4.4/0R6aJbachfZxGtbwTksIGg.JutrR2',NULL,NULL,NULL,'10','theCreator',1522377073,1522377073,NULL,NULL,NULL,'id'),(21,'nashehannafii','aa',NULL,'nashehannafii@unida.gontor.ac.id','bCdJDPoNhXTkLw7jcFP_utRhDuQhn6lx','$2y$13$VNNqJ89wRsltr.j3eqAd2ONRjgn37Bk8Jq8rRIhtdXWknbrIAQQ2S','n6VfqlTzKIpW2ojkp96BElxBENjbaU9q_1689238028',NULL,NULL,'10','admin',1687499532,1687499532,NULL,NULL,NULL,NULL),(28,'nasheh','nasheh',NULL,'nasheh@gmail.com','Ck8fYx_C5hZ4ChHvT0Ahh0QYDuViSaob','$2y$13$UZ5Kl2.9aHe3qhUub5MKdeXAkQ8pzFlOPjIiAiEbKh3HANtLqma2C',NULL,NULL,NULL,'10','warek',1689260974,1689260974,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2023-07-16 14:44:23
