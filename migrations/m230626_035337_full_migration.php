<?php

use yii\db\Migration;

/**
 * Class m230626_035337_full_migration
 */
class m230626_035337_full_migration extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        // Create the 'gedung' table
        $this->createTable('{{%gedung}}', [
            'id' => $this->primaryKey(),
            'nama' => $this->string(50)->notNull(),
            'keterangan' => $this->string(500)->notNull(),
            'status_aktif' => $this->integer()->notNull(),
            'gambar_path' => $this->string(255),
            'created_at' => $this->dateTime()->defaultValue(new \yii\db\Expression('NOW()')),
        ]);

        // Create the 'ruangan' table
        $this->createTable('{{%ruangan}}', [
            'id' => $this->primaryKey(),
            'gedung_id' => $this->integer()->notNull(),
            'no_ruangan' => $this->string(50)->notNull(),
            'nama' => $this->string(150)->notNull(),
            'kapasitas' => $this->integer()->notNull(),
            'keterangan' => $this->string(500)->notNull(),
            'gambar_path' => $this->string(255),
            'status_aktif' => $this->integer()->notNull(),
            'status_id' => $this->integer(),
            'created_at' => $this->dateTime()->defaultValue(new \yii\db\Expression('NOW()')),
        ]);

        // Create the 'ruangan_riwayat' table
        $this->createTable('{{%ruangan_riwayat}}', [
            'id' => $this->primaryKey(),
            'ruangan_id' => $this->integer()->notNull(),
            'no_ruangan' => $this->string(50)->notNull(),
            'nama' => $this->string(150)->notNull(),
            'kapasitas' => $this->integer()->notNull(),
            'gambar_path' => $this->string(255),
            'status_aktif' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->defaultValue(new \yii\db\Expression('NOW()')),

        ]);

        // Create the 'jam' table
        $this->createTable('{{%jam}}', [
            'id' => $this->primaryKey(),
            'urutan' => $this->integer()->notNull(),
            'jam' => $this->string()->notNull(),
            'created_at' => $this->dateTime()->defaultValue(new \yii\db\Expression('NOW()')),

        ]);

        // Create the 'peminjaman' table
        $this->createTable('{{%peminjaman}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'status_peminjaman' => $this->integer(),
            'nama_acara' => $this->string(150),
            'tema_acara' => $this->string(150),
            'deskripsi_acara' => $this->string(700),
            'ketua_acara' => $this->string(150),
            'jenis_peminjaman' => $this->integer(5),
            'nomor_surat' => $this->string(30),
            'tanggal_mulai' => $this->dateTime(),
            // 'tanggal_selesai' => $this->date(),
            'is_sent' => $this->integer(1),
            'created_at' => $this->dateTime()->defaultValue(new \yii\db\Expression('NOW()')),
            'temp' => $this->string(8000),

        ]);

        // Create the 'peminjaman_ruangan' table
        $this->createTable('{{%peminjaman_ruangan}}', [
            'id' => $this->primaryKey(),
            'peminjaman_id' => $this->integer()->notNull(),
            'tanggal_pinjam' => $this->date()->notNull(),
            'mulai_id' => $this->integer()->notNull(),
            'selesai_id' => $this->integer()->notNull(),
            'ruangan_riwayat_id' => $this->integer()->notNull(),
            'created_at' => $this->dateTime()->defaultValue(new \yii\db\Expression('NOW()')),
        ]);

        // Create the 'sarpras' table
        $this->createTable('{{%sarpras}}', [
            'id' => $this->primaryKey(),
            'kepala_sarpras' => $this->string(150),
            'peraturan_umum' => $this->string(1500),
            'jam_autocancel' => $this->time(),
            'durasi_autodelete' => $this->integer(2),
            'created_at' => $this->dateTime()->defaultValue(new \yii\db\Expression('NOW()')),
        ]);

        // Create the 'status ruangan' table
        $this->createTable('{{%status_ruangan}}', [
            'id' => $this->primaryKey(),
            'status' => $this->string(150),
            'created_at' => $this->dateTime()->defaultValue(new \yii\db\Expression('NOW()')),

        ]);

        // Create the 'unit_kerja' table
        $this->createTable('unit_kerja', [
            'id' => $this->primaryKey(),
            'parent_id' => $this->integer(),
            'jenjang_id' => $this->integer(),
            'user_id' => $this->integer(),
            'is_sent' => $this->integer(),
            'jenis' => $this->string(100),
            'nama' => $this->string(100)->notNull(),
            'singkatan' => $this->string(20),
            'surat' => $this->string(5),
            'kode_prodi' => $this->string(10),
            'email' => $this->string(150),
            'penanggung_jawab' => $this->string(150),
            'created_at' => $this->dateTime()->defaultValue(new \yii\db\Expression('NOW()')),

        ]);

        // Add foreign key constraints

        // 'ruangan' table
        $this->addForeignKey(
            'fk_ruangan_gedung',
            '{{%ruangan}}',
            'gedung_id',
            '{{%gedung}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // 'ruangan_riwayat' table
        $this->addForeignKey(
            'fk_ruangan_riwayat_ruangan',
            '{{%ruangan_riwayat}}',
            'ruangan_id',
            '{{%ruangan}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // 'peminjaman' table
        $this->addForeignKey(
            'fk_peminjaman_user',
            '{{%peminjaman}}',
            'user_id',
            'user',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_peminjaman_ruangan_mulai_jam',
            '{{%peminjaman_ruangan}}',
            'mulai_id',
            '{{%jam}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_peminjaman_ruangan_selesai_jam',
            '{{%peminjaman_ruangan}}',
            'selesai_id',
            '{{%jam}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // 'peminjaman_ruangan' table
        $this->addForeignKey(
            'fk_peminjaman_ruangan_peminjaman',
            '{{%peminjaman_ruangan}}',
            'peminjaman_id',
            '{{%peminjaman}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_peminjaman_ruangan_ruangan_riwayat',
            '{{%peminjaman_ruangan}}',
            'ruangan_riwayat_id',
            '{{%ruangan_riwayat}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        // 'unitKerja_user' table
        $this->addForeignKey(
            'fk_user_unit_kerja',
            '{{%user}}',
            'unit_kerja_id',
            '{{%unit_kerja}}',
            'id',
            'CASCADE',
            'CASCADE'
        );


        // 'ruangan_statusruangan' table
        $this->addForeignKey(
            'fk_ruangan_statusruangan',
            '{{%ruangan}}',
            'status_id',
            '{{%status_ruangan}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->insert('jam', ['urutan' => 1, 'jam' => "07:00"]);
        $this->insert('jam', ['urutan' => 2, 'jam' => "08:00"]);
        $this->insert('jam', ['urutan' => 3, 'jam' => "09:00"]);
        $this->insert('jam', ['urutan' => 4, 'jam' => "10:00"]);
        $this->insert('jam', ['urutan' => 5, 'jam' => "11:00"]);
        $this->insert('jam', ['urutan' => 6, 'jam' => "12:00"]);
        $this->insert('jam', ['urutan' => 7, 'jam' => "13:00"]);
        $this->insert('jam', ['urutan' => 8, 'jam' => "14:00"]);
        $this->insert('jam', ['urutan' => 9, 'jam' => "15:00"]);
        $this->insert('jam', ['urutan' => 10, 'jam' => "16:00"]);
        $this->insert('jam', ['urutan' => 11, 'jam' => "17:00"]);
        $this->insert('jam', ['urutan' => 12, 'jam' => "18:00"]);
        $this->insert('jam', ['urutan' => 13, 'jam' => "20:00"]);
        $this->insert('jam', ['urutan' => 14, 'jam' => "21:00"]);
        $this->insert('jam', ['urutan' => 15, 'jam' => "22:00"]);

        $this->insert('gedung', ['nama' => "Gedung Terpadu", 'keterangan' => "Gedung pusat unida", 'status_aktif' => 1, 'gambar_path' => 'http://s3.unida.gontor.ac.id:9000/sarpras/gedung/Gedung%20Terpadu-22-06-23-gedung%20terpadu.jpg']);
        $this->insert('gedung', ['nama' => "Gedung Utama Lama", 'keterangan' => "Gedung pusat unida", 'status_aktif' => 1, 'gambar_path' => 'http://s3.unida.gontor.ac.id:9000/sarpras/gedung/Gedung%20Utama%20Lama-22-06-23-gedung%20utama%20lama.jpg']);
        $this->insert('gedung', ['nama' => "CIOS", 'keterangan' => "Gedung pusat unida", 'status_aktif' => 1, 'gambar_path' => 'http://s3.unida.gontor.ac.id:9000/sarpras/gedung/CIOS-22-06-23-IMG_8572-1024x683.jpg']);
        $this->insert('gedung', ['nama' => "CIES", 'keterangan' => "Gedung pusat unida", 'status_aktif' => 1, 'gambar_path' => 'http://s3.unida.gontor.ac.id:9000/sarpras/gedung/CIES-22-06-23-CIES.jpeg']);
        $this->insert('gedung', ['nama' => "Sirah", 'keterangan' => "Gedung pusat unida", 'status_aktif' => 1, 'gambar_path' => 'http://s3.unida.gontor.ac.id:9000/sarpras/gedung/Sirah-22-06-23-download-7.jpg']);
        $this->insert('gedung', ['nama' => "UNIDA Inn", 'keterangan' => "Hotel Universitas Darussalam Gontor", 'status_aktif' => 1, 'gambar_path' => 'http://s3.unida.gontor.ac.id:9000/sarpras/gedung/UNIDA%20Inn-22-06-23-Unida-Gontor-Inn-1024x682.jpg']);

        $this->insert('ruangan', ['gedung_id' => 1, 'no_ruangan' => "310", 'nama' => "Hall Multimedia", 'kapasitas' => 150, 'keterangan' => 'test', 'status_aktif' => 1, 'gambar_path' => 'http://s3.unida.gontor.ac.id:9000/sarpras/ruangan/Hall%20Multimedia-23-06-23-WhatsApp-Image-2022-05-17-at-09.58.01-1024x768.jpeg']);
        $this->insert('ruangan', ['gedung_id' => 1, 'no_ruangan' => "110", 'nama' => "Lobby Lantai 1", 'kapasitas' => 450, 'keterangan' => 'test', 'status_aktif' => 1]);
        $this->insert('ruangan', ['gedung_id' => 1, 'no_ruangan' => "410", 'nama' => "Hall Lantai 4", 'kapasitas' => 400, 'keterangan' => 'test', 'status_aktif' => 1, 'gambar_path' => 'http://s3.unida.gontor.ac.id:9000/sarpras/ruangan/Hall%20Lt%204-23-06-23-292.-Seminar-International-Waqf-IZU.jpg']);
        $this->insert('ruangan', ['gedung_id' => 1, 'no_ruangan' => "102", 'nama' => "Ruang Rapat Senat", 'kapasitas' => 400, 'keterangan' => 'test', 'status_aktif' => 1]);
        $this->insert('ruangan', ['gedung_id' => 2, 'no_ruangan' => "104", 'nama' => "Kantor KBIH", 'kapasitas' => 15, 'keterangan' => 'test', 'status_aktif' => 1]);
        $this->insert('ruangan', ['gedung_id' => 3, 'no_ruangan' => "110", 'nama' => "Hall CIOS", 'kapasitas' => 150, 'keterangan' => 'test', 'status_aktif' => 1]);
        $this->insert('ruangan', ['gedung_id' => 4, 'no_ruangan' => "210", 'nama' => "Hall CIES", 'kapasitas' => 150, 'keterangan' => 'test', 'status_aktif' => 1]);
        $this->insert('ruangan', ['gedung_id' => 5, 'no_ruangan' => "210", 'nama' => "Hall Sirah", 'kapasitas' => 150, 'keterangan' => 'test', 'status_aktif' => 1]);
        $this->insert('ruangan', ['gedung_id' => 5, 'no_ruangan' => "110", 'nama' => "Perpustakaan", 'kapasitas' => 150, 'keterangan' => 'test', 'status_aktif' => 1]);
        $this->insert('ruangan', ['gedung_id' => 5, 'no_ruangan' => "109", 'nama' => "Lab Bahasa", 'kapasitas' => 150, 'keterangan' => 'test', 'status_aktif' => 1]);

        $this->insert('ruangan_riwayat', ['ruangan_id' => 1, 'no_ruangan' => "310", 'kapasitas' => 100, 'nama' => "Hall Multimedia", 'status_aktif' => 1, 'gambar_path' => 'http://s3.unida.gontor.ac.id:9000/sarpras/ruangan/Hall%20Multimedia-23-06-23-WhatsApp-Image-2022-05-17-at-09.58.01-1024x768.jpeg']);
        $this->insert('ruangan_riwayat', ['ruangan_id' => 2, 'no_ruangan' => "110", 'kapasitas' => 100, 'nama' => "Lobby Lantai 1", 'status_aktif' => 1]);
        $this->insert('ruangan_riwayat', ['ruangan_id' => 3, 'no_ruangan' => "410", 'kapasitas' => 100, 'nama' => "Hall Lantai 4", 'status_aktif' => 1, 'gambar_path' => 'http://s3.unida.gontor.ac.id:9000/sarpras/ruangan/Hall%20Lt%204-23-06-23-292.-Seminar-International-Waqf-IZU.jpg']);
        $this->insert('ruangan_riwayat', ['ruangan_id' => 4, 'no_ruangan' => "102", 'kapasitas' => 100, 'nama' => "Ruang Rapat Senat", 'status_aktif' => 1]);
        $this->insert('ruangan_riwayat', ['ruangan_id' => 5, 'no_ruangan' => "104", 'kapasitas' => 100, 'nama' => "Kantor KBIH", 'status_aktif' => 1]);
        $this->insert('ruangan_riwayat', ['ruangan_id' => 6, 'no_ruangan' => "110", 'kapasitas' => 100, 'nama' => "Hall CIOS", 'status_aktif' => 1]);
        $this->insert('ruangan_riwayat', ['ruangan_id' => 7, 'no_ruangan' => "210", 'kapasitas' => 100, 'nama' => "Hall CIES", 'status_aktif' => 1]);
        $this->insert('ruangan_riwayat', ['ruangan_id' => 8, 'no_ruangan' => "210", 'kapasitas' => 100, 'nama' => "Hall Sirah", 'status_aktif' => 1]);
        $this->insert('ruangan_riwayat', ['ruangan_id' => 9, 'no_ruangan' => "110", 'kapasitas' => 100, 'nama' => "Perpustakaan", 'status_aktif' => 1]);
        $this->insert('ruangan_riwayat', ['ruangan_id' => 10, 'no_ruangan' => "109", 'kapasitas' => 100, 'nama' => "Lab Bahasa", 'status_aktif' => 1]);

        $this->insert('sarpras', ['kepala_sarpras' => "Moh. Syifa`urrosyidin, S.E.I., M.Pd", 'peraturan_umum' => '<p><strong>PERATURAN PEMINJAMAN FASILITAS</strong></p> <ol> <li>Datang pakai baju yang sopan dan rapi,</li> <li>Datang di jam efektif (jam kantor),</li> <li>Merapikan atau mengembalikan Fasilitas yang telah di pakai</li> <li>Untuk keperluan diluar jam kantor harap menghubungi staf&nbsp;bagian&nbsp;sarpras</li> </ol> ', 'jam_autocancel' => '04:00', 'durasi_autodelete' => '6']);
    }

    public function down()
    {
        // Drop foreign key constraints

        // 'peminjaman_ruangan' table
        $this->dropForeignKey('fk_peminjaman_ruangan_peminjaman', '{{%peminjaman_ruangan}}');
        $this->dropForeignKey('fk_peminjaman_ruangan_ruangan_riwayat', '{{%peminjaman_ruangan}}');
        $this->dropForeignKey('fk_peminjaman_ruangan_mulai_jam', '{{%peminjaman_ruangan}}');
        $this->dropForeignKey('fk_peminjaman_ruangan_selesai_jam', '{{%peminjaman_ruangan}}');

        // 'peminjaman' table
        $this->dropForeignKey('fk_peminjaman_user', '{{%peminjaman}}');

        // 'ruangan_riwayat' table
        $this->dropForeignKey('fk_ruangan_riwayat_ruangan', '{{%ruangan_riwayat}}');

        // 'ruangan' table
        $this->dropForeignKey('fk_ruangan_gedung', '{{%ruangan}}');
        $this->dropForeignKey('fk_ruangan_statusruangan', '{{%ruangan}}');

        // 'unit-kerja' table
        $this->dropForeignKey('fk_user_unit_kerja', '{{%user}}');


        // Drop the tables
        $this->dropTable('{{%peminjaman_ruangan}}');
        $this->dropTable('{{%peminjaman}}');
        $this->dropTable('{{%jam}}');
        $this->dropTable('{{%ruangan_riwayat}}');
        $this->dropTable('{{%status_ruangan}}');
        $this->dropTable('{{%ruangan}}');
        $this->dropTable('{{%gedung}}');
        $this->dropTable('{{%sarpras}}');
        $this->dropTable('{{%unit_kerja}}');
    }
}
