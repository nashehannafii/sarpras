<?php

namespace app\commands;

use app\models\Peminjaman;
use app\models\PeminjamanRuangan;
use app\models\Sarpras;
use DateInterval;
use DateTime;
use yii\console\Controller;

class CronController extends Controller
{
    public function actionTenggatPeminjaman()
    {
        $peminjaman = Peminjaman::find()->where([
            'status_peminjaman' => 0
        ])->all();

        $batasWaktu = Sarpras::findOne(1)->jam_autocancel; // Time ex: 04:00:00

        $timeBefore = DateTime::createFromFormat('H:i:s', $batasWaktu);
        $timeBefore = $timeBefore->format('H:i');

        $timeAfter = DateTime::createFromFormat('H:i:s', $batasWaktu);
        $timeAfter->add(new DateInterval('PT10M'));
        $timeAfter = $timeAfter->format('H:i');

        date_default_timezone_set('Asia/Jakarta');
        $currentTime = date('H:i');

        if ($currentTime >= $timeBefore && $currentTime <= $timeAfter) {
            $countBerhasil = 0;
            $countGagal = 0;
            foreach ($peminjaman as $pinjam) {
                $model = $this->findPinjam($pinjam->id);
                $model->status_peminjaman = 2;

                if ($model->save()) {
                    $countBerhasil++;
                    echo $countBerhasil . " - [Canceled] " . $model->nama_acara . "\n";
                } else {
                    $countGagal++;
                    echo $countGagal . " - [Failed] " . $model->nama_acara . "\n";
                }
            }
            echo "---------------------------------------------\n";
            echo "[" . $countBerhasil . "] Berhasil di cancel\n";
            echo "[" . $countGagal . "] Gagal di cancel\n";
        } else {
            echo "Belum waktunya :)";
        }
    }

    public function actionClearCart()
    {
        $peminjaman = Peminjaman::find()->where([
            'status_peminjaman' => 0
        ])->all();
        $durasi = Sarpras::findOne(1)->durasi_autodelete; //Jam ex: 24 

        $countBerhasil = 0;
        $countGagal = 0;

        foreach ($peminjaman as $pinjam) {

            $connection = \Yii::$app->db;
            $transaction = $connection->beginTransaction();

            $createdAtTimestamp = strtotime($pinjam->created_at);
            $currentTimestamp = time();
            $timeDifferenceInMinutes = round(($currentTimestamp - $createdAtTimestamp) / 60);

            if ($timeDifferenceInMinutes <= 60 * $durasi) {

                $model = $this->findPinjam($pinjam->id);
                $deleteCart = PeminjamanRuangan::deleteAll(['peminjaman_id' => $model->id]);

                if ($deleteCart) {
                    $transaction->commit();
                    $countBerhasil++;
                    echo $countBerhasil . " - [Deleted] " . $model->nama_acara . "\n";
                } else {
                    $transaction->rollBack();
                    $countGagal++;
                    echo $countGagal . " - [Failed] " . $model->nama_acara . "\n";
                }
            }
        }

        echo "---------------------------------------------\n";
        echo "[" . $countBerhasil . "] Berhasil di hapus\n";
        echo "[" . $countGagal . "] Gagal di hapus\n";
    }

    protected function findPinjam($id)
    {
        if (($model = Peminjaman::findOne($id)) !== null) {
            return $model;
        }
    }
}
