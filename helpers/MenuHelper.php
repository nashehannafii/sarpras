<?php

namespace app\helpers;

use Yii;
use yii\helpers\Url;

/**
 * Css helper class.
 */
class MenuHelper
{


	public static function getMenuItems()
	{

		$menuItems = [];

		if (Yii::$app->user->can('admin') || Yii::$app->user->can('warek')) {
			# code...
			$menuItems[] = [
				'template' => '<a href="{url}">{label}</a>',
				'label' => '<i class="lnr lnr-home"></i><span>' . Yii::t('app', 'Dashboard') . '</span>',
				'url' => ['site/index'],
			];
		}


		$menuItems[] = [
			'template' => '<a href="{url}">{label}</a>',
			'visible' => !Yii::$app->user->can('warek'),
			'label' => '<i class="lnr lnr-calendar-full"></i><span>' . Yii::t('app', 'Booking Map') . '</span>',
			'url' => ['peminjaman/map'],
		];

		if (!Yii::$app->user->isGuest) {

			$menuItems[] = [
				'template' => '<a href="{url}">{label}</a>',
				'visible' => Yii::$app->user->can('admin') || Yii::$app->user->can('unitKerja'),
				'label' => '<i class="lnr lnr-layers"></i><span>' . Yii::t('app', 'Booking') . '</span><i class="icon-submenu lnr lnr-chevron-left"></i>',
				'url' => '#',
				'submenuTemplate' => "\n<div id='pages_ajuan' class='collapse'><ul class='nav'>\n{items}\n</ul></div>\n",
				'template' => '<a class="collapsed" data-toggle="collapse" href="#pages_ajuan">{label}</a>',
				'items' => [
					['label' => Yii::t('app', 'Booking History'), 'url' => ['/peminjaman/index']],
					['label' => Yii::t('app', 'Canceled History'), 'url' => ['/peminjaman/canceled']],

				]
			];


			$menuItems[] = [
				'template' => '<a href="{url}">{label}</a>',
				'visible' => Yii::$app->user->can('admin'),
				'label' => '<i class="lnr lnr-cog"></i><span>' . Yii::t('app', 'Master') . '</span><i class="icon-submenu lnr lnr-chevron-left"></i>',
				'url' => '#',
				'submenuTemplate' => "\n<div id='pages_master' class='collapse'><ul class='nav'>\n{items}\n</ul></div>\n",
				'template' => '<a class="collapsed" data-toggle="collapse" href="#pages_master">{label}</a>',
				'items' => [
					['label' => Yii::t('app', 'Auth Item'), 'url' => ['/auth-item/index'], 'visible' => Yii::$app->user->can('theCreator')],
					['label' => Yii::t('app', 'Auth Item Child'), 'url' => ['/auth-item-child/index'], 'visible' => Yii::$app->user->can('theCreator')],
					['label' => Yii::t('app', 'Venue'), 'url' => ['/gedung/index']],
					['label' => Yii::t('app', 'Rooms'), 'url' => ['/ruangan/index']],
					['label' => Yii::t('app', 'Times'), 'url' => ['/jam/index']],
					['label' => Yii::t('app', 'Sarpras'), 'url' => ['/sarpras/index']],
					['label' => Yii::t('app', 'Room Status'), 'url' => ['/status-ruangan/index']],
					['label' => Yii::t('app', 'Divisions'), 'url' => ['/unit-kerja/index']],
					['label' => Yii::t('app', 'Users'), 'url' => ['/user/index'], 'visible' => Yii::$app->user->can('theCreator')],

				]
			];
		}

		if (Yii::$app->user->isGuest) {
			$menuItems[] = [
				'template' => '<a href="{url}">{label}</a>',
				'label' => '<i class="lnr lnr-login"></i><span>' . Yii::t('app', 'Login') . '</span>',
				'url' => ['site/login'],
			];
		}


		return $menuItems;
	}

	public static function getTopMenus()
	{
		$menuItems = [];
		$list_apps = [];
		if (!Yii::$app->user->isGuest) {
			$key = Yii::$app->params['jwt_key'];
			$session = Yii::$app->session;
			if ($session->has('token')) {
				$token = $session->get('token');
				try {
					$decoded = \Firebase\JWT\JWT::decode($token, base64_decode(strtr($key, '-_', '+/')), ['HS256']);
					foreach ($decoded->apps as $d) {
						$list_apps[] = [
							'template' => '<a target="_blank" href="{url}">{label}</a>',
							'label' => $d->app_name,
							'url' => $d->app_url . $token
						];
					}
				} catch (\Exception $e) {
					// return Yii::$app->response->redirect(Yii::$app->params['sso_login']);
				}
			}
		}

		if (!Yii::$app->user->isGuest) {

			$menuItems[] = [
				'template' => '<a href="javascript:void(0)" >Logged in as {label}</a>',
				'label' => '<strong>' . Yii::$app->user->identity->access_role . '</strong>',
				// 'url' => ['site/change'],
				// 'url' => ['javascript:void(0)'],
			];

			$menuItems[] = [
				'template' => '<a href="{url}" class="icon-menu">{label}</a>',
				'visible' => Yii::$app->user->can('unitKerja') || Yii::$app->user->can('admin'),
				'label' => '<i class="lnr lnr-cart"></i><span class="badge bg-danger">' . MyHelper::getCountCart() . '</span>',
				'url' => ['peminjaman/cart'],
			];

			$menuItems[] = [
				'template' => '<a href="{url}" class="dropdown-toggle" data-toggle="dropdown"><i class="lnr lnr-layers"></i> <span>{label}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>',
				'label' => Yii::t('app', 'Your apps'),
				'submenuTemplate' => "\n<ul class='dropdown-menu'>\n{items}\n</ul>\n",
				'items' => $list_apps
			];

			$menuItems[] = [
				'template' => '<a href="{url}" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-language"></i> <span>{label}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>',
				// 'label' => "tess",
				'label' => '',
				'submenuTemplate' => "\n<ul class='dropdown-menu'>\n{items}\n</ul>\n",
				'items' => [
					[
						'template' => ' <a href="javascript:void(0)" class="language" id="id">
						<span class="icon indonesia">
							<svg xmlns="http://www.w3.org/2000/svg" id="flag-icons-id" viewBox="0 0 640 480">
								<path fill="#e70011" d="M0 0h640v240H0Z"/>
  								<path fill="#fff" d="M0 240h640v240H0Z"/>
							</svg>
						</span> {label}</a>',
						'label' => Yii::t('app', 'Indonesian'),
						'url' => ['site/logout']
					],
					[
						'template' => '<a href="javascript:void(0)" class="language" id="en">
						<span class="icon united-kingdom">
							<svg xmlns="http://www.w3.org/2000/svg" id="flag-icons-gb" viewBox="0 0 640 480">
								<path fill="#012169" d="M0 0h640v480H0z"/>
								<path fill="#FFF" d="m75 0 244 181L562 0h78v62L400 241l240 178v61h-80L320 301 81 480H0v-60l239-178L0 64V0h75z"/>
								<path fill="#C8102E" d="m424 281 216 159v40L369 281h55zm-184 20 6 35L54 480H0l240-179zM640 0v3L391 191l2-44L590 0h50zM0 0l239 176h-60L0 42V0z"/>
								<path fill="#FFF" d="M241 0v480h160V0H241zM0 160v160h640V160H0z"/>
								<path fill="#C8102E" d="M0 193v96h640v-96H0zM273 0v480h96V0h-96z"/>
							</svg>
						</span> {label} </a>',
						'label' => Yii::t('app', 'English'),
						'url' => ['site/logout']
					],
					// [
					// 	'template' => '<a href="{url}" data-method="POST">{label}</a>',
					// 	'label' => Yii::t('app', 'Sign Out'),
					// 	'url' => ['site/logout']
					// ],
				]

			];

			$menuItems[] = [
				'template' => '<a href="{url}" class="dropdown-toggle" data-toggle="dropdown"><i class="lnr lnr-user"></i> <span>{label}</span> <i class="icon-submenu lnr lnr-chevron-down"></i></a>',
				// 'label' => "tess",
				'label' => isset(Yii::$app->user->identity->unitKerja->nama) ? Yii::$app->user->identity->unitKerja->nama : 'Admin',
				'submenuTemplate' => "\n<ul class='dropdown-menu'>\n{items}\n</ul>\n",
				'items' => [
					// [
					// 	'template' => '<a href="{url}">{label}</a>',
					// 	'label' => 'My Profile',
					// 	'url' => ['data-diri/create'],
					// 	'visible' => !(Yii::$app->user->identity->access_role == 'Staf' || Yii::$app->user->identity->access_role == 'Tendik')
					// ],
					// [
					// 	'template' => '<a href="{url}">{label}</a>',
					// 	'label' => 'My Profile',
					// 	'url' => ['tendik/view'],
					// 	'visible' => (Yii::$app->user->identity->access_role == 'Staf' || Yii::$app->user->identity->access_role == 'Tendik')
					// ],
					// [
					// 	'template' => '<a href="{url}">{label}</a>',
					// 'label' => 'Change Role', 
					// 	'url' => ['site/change'],
					// 	'visible' => !(Yii::$app->user->identity->access_role == 'Staf' || Yii::$app->user->identity->access_role == 'Tendik')
					// ],
					[
						'template' => '<a href="{url}" data-method="POST">{label}</a>',
						'label' => Yii::t('app', 'Sign Out'),
						'url' => ['site/logout']
					]
				]

			];
		}


		return $menuItems;
	}

	public static function getUserMenus()
	{
		$menuItems = [];

		if (!Yii::$app->user->isGuest) {


			$menuItems[] = [
				'template' => '<a data-widget="pushmenu" href="{url}" role="button" class="nav-link">{label}</a>',
				'label' => '<i class="fas fa-bars"></i>',
				'url' => '#'
			];
		}


		return $menuItems;
	}
}
