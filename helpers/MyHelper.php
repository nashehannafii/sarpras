<?php

namespace app\helpers;

use app\models\Label;
use app\models\PeminjamanRuangan;
use app\models\RuanganRiwayat;
use Yii;
use yii\helpers\Json;
use yii\httpclient\Client;


if (!defined('STDIN')) {
    define('STDIN', fopen('php://stdin', 'r'));
}

class MyHelper
{

    public static function diffDate($tanggal_mulai, $tanggal_selesai, $jumlah_hari = null)
    {
        $tanggal_mulai = new \DateTime($tanggal_mulai);

        $tanggal_selesai = new \DateTime($tanggal_selesai);


        if ($tanggal_mulai > $tanggal_selesai) return "tanggal tidak sesuai";

        if ($jumlah_hari == true) return $tanggal_selesai->diff($tanggal_mulai)->days;

        $perbedaan = $tanggal_selesai->diff($tanggal_mulai);

        $perbedaan_bulan = $perbedaan->m;
        $perbedaan_hari = $perbedaan->d;


        return ($perbedaan_bulan == 0 ? '' : $perbedaan_bulan . " bulan, ")  . $perbedaan_hari . " hari.";
    }

    public static function cekNull($params)
    {
        return isset($params) ? $params : '-';
    }

    public static function listAkreditasi()
    {
        $list = [
            'U' => 'Unggul',
            'BS' => 'Baik Sekali',
            'BK' => 'Baik',
            'A' => 'A',
            'B' => 'B',
            'C' => 'C'
        ];

        return $list;
    }

    public static function detailPeminjaman()
    {
        return [
            'status_view' => [
                0 => '<span class="label label-warning label-transparent"> Waiting <i class="fa fa-angle-down"></i></span>',
                1 => '<span class="label label-success label-transparent"> Booked <i class="fa fa-angle-down"></i></span>',
                2 => '<span class="label label-danger label-transparent"> Canceled <i class="fa fa-angle-down"></i></span>',
            ],
            'status_persetujuan' => [
                0 => 'Waiting Approval',
                1 => 'Accepted',
                2 => 'Rejected',
            ],
            'jenis_peminjaman' => [
                1 => 'Basic',
                2 => 'Advance',
            ],
        ];
    }

    public static function statusRuangan()
    {
        return [
            'status_view' => [
                0 => '<span class="label label-warning label-transparent">Waiting</span>',
                1 => '<span class="label label-success label-transparent">Booked</span>',
                2 => '<span class="label label-danger label-transparent">Canceled</span>',
            ],
            // 'status_persetujuan' => [
            //     0 => 'Belum disetujui',
            //     1 => 'Disetujui',
            //     2 => 'Ditolak',
            // ],
        ];
    }

    public static function getMonthsAgo($jumlahBulan)
    {
        $months = array();
        $sekarang = date('Y-m-d');

        $bulan_kebelakang = strtotime('-' . ($jumlahBulan - 1) . ' months', strtotime($sekarang));

        for ($i = 0; $i < $jumlahBulan; $i++) {
            $bulan_tahun = date('F Y', $bulan_kebelakang);
            $bulan_kebelakang = strtotime('+1 month', $bulan_kebelakang);
            array_push($months, $bulan_tahun);
        }

        return $months;
    }

    public static function getMonthsByYear($jumlahBulan)
    {
        $months = array();
        $sekarang = date('Y-m-d');

        $bulan_kebelakang = strtotime('-' . ($jumlahBulan - 1) . ' months', strtotime($sekarang));

        for ($i = 0; $i < $jumlahBulan; $i++) {
            $bulan_tahun = date('F Y', $bulan_kebelakang);
            $bulan_kebelakang = strtotime('+1 month', $bulan_kebelakang);
            array_push($months, $bulan_tahun);
        }

        return $months;
    }

    public static function getCountCart()
    {
        $cart = PeminjamanRuangan::find();
        $cart->joinWith(['peminjaman as p'])
            ->where(['p.temp' => ""])
            ->andWhere(['p.user_id' => Yii::$app->user->identity->id]);
        $cart = $cart->count();

        return $cart;
    }

    public static function getYears()
    {
        $currentDate = date('Y-m-d');
        $oneYearAhead = date('Y', strtotime('+1 year', strtotime($currentDate)));
        $fourYearsAgo = date('Y', strtotime('-3 years', strtotime($currentDate)));
        
        $years = array_reverse(range($fourYearsAgo, $oneYearAhead));
        
        return array_combine($years, $years);
    }



    public static function getLabelList()
    {

        $labels = RuanganRiwayat::find()->where(['status_aktif' => 1])->all();

        $listLabel = [];
        foreach ($labels as $label) {
            # code...
            $listLabel[$label->ruangan_id] = $label->nama;
        }

        return $listLabel;
    }

    public static function listJenjangStudi()
    {
        $list_kriteria = [
            'D' => [
                'jenjang' => 'Diploma 4',
                'singkatan' => 'D4',
            ],
            'C' => [
                'jenjang' => 'Sarjana',
                'singkatan' => 'S1',
            ],
            'B' => [
                'jenjang' => 'Magister',
                'singkatan' => 'S2',
            ],
            'A' => [
                'jenjang' => 'Doktoral',
                'singkatan' => 'S3',
            ],
        ];

        return $list_kriteria;
    }

    public static function listRangeDate()
    {
        $now = date('Y-m-d');

        $list_range_date = [
            'One week' => [
                'now' => $now,
                'end' => date('Y-m-d', strtotime($now . ' + 1 week')),
            ],
            'Two weeks' => [
                'now' => $now,
                'end' => date('Y-m-d', strtotime($now . ' + 2 weeks')),
            ],
            'One month' => [
                'now' => $now,
                'end' => date('Y-m-d', strtotime($now . ' + 1 month')),
            ],
            'Two months' => [
                'now' => $now,
                'end' => date('Y-m-d', strtotime($now . ' + 2 months')),
            ],
            'Three months' => [
                'now' => $now,
                'end' => date('Y-m-d', strtotime($now . ' + 3 months')),
            ],
        ];


        return $list_range_date;
    }

    public static function getStatusAktif()
    {
        $roles =
            [
                'label' => [
                    '1' => '<span class="label label-success label-transparent">Active</span>',
                    '0' => '<span class="label label-danger label-transparent">Non-Active</span>',
                ],
                'filter' => [
                    '1' => 'Active',
                    '0' => 'Non-Active',
                ]
            ];


        return $roles;
    }

    public static function sendEmail($userMail)
    {
        if (Yii::$app->user->identity->access_role == 'theCreator')
            $userMail = 'nashehannafii@unida.gontor.ac.id';

        // echo '<pre>';print_r($userMail);die;

        return $userMail;
    }

    public static function getJenisHasilPustaka()
    {
        return ['0' => 'Bukan Hasil Riset/Abdimas Sendiri', '1' => 'Hasil Penelitian', '2' => 'Hasil Pengabdian kepada Masyarakat'];
    }

    public static function cleanSpecialChars($string)
    {
        // $string = str_replace(' ', '-', $string);
        return preg_replace('/[^A-Za-z0-9\-]/', ' ', $string); // Removes special chars.
    }

    public static function getStatusHapus()
    {
        return [
            '1' => 'Hapus',
            '0' => 'Aktif'
        ];
    }

    public static function clean($string)
    {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.

        return preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
    }

    public static function split_name($name)
    {
        $name = trim($name);
        $last_name = (strpos($name, ' ') === false) ? '' : preg_replace('#.*\s([\w-]*)$#', '$1', $name);
        $first_name = trim(preg_replace('#' . preg_quote($last_name, '#') . '#', '', $name));
        return array($first_name, $last_name);
    }
    public static function getJenisMBKM()
    {
        return [
            '1' => 'Dalam PT',
            '2' => 'PT Lain',
            '3' => 'Non-PT',
            '0' => 'Non-MBKM'
        ];
    }
    public static function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if (!$length) {
            return true;
        }
        return substr($haystack, -$length) === $needle;
    }

    public static function logDebug($error)
    {
        echo '<pre>';
        print_r($error);
        die;
    }

    public static function getJenisFileClassroom()
    {
        return [
            'driveFile' => 'Google Drive',
            // 'youtubeVideo' => 'YouTube',
            'link' => 'Link',
            // 'form' => 'Form'
        ];
    }

    public static function getSisterToken()
    {
        $tokenPath = Yii::getAlias('@webroot') . '/credentials/sister_token.json';
        $sisterToken = '';

        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $sisterToken = $accessToken['id_token'];
            $created_at = $accessToken['created_at'];
            $date     = new \DateTime(date('Y-m-d H:i:s', strtotime($created_at)));
            $current  = new \DateTime(date('Y-m-d H:i:s'));
            $interval = $date->diff($current);
            // $inv = $interval->format('%I');
            $minutes = $interval->days * 24 * 60;
            $minutes += $interval->h * 60;
            $minutes += $interval->i;

            if ($minutes > 5) {
                if (!MyHelper::wsSisterLogin()) {
                    throw new \Exception("Error Creating SISTER Token", 1);
                } else {
                    $accessToken = json_decode(file_get_contents($tokenPath), true);
                    $sisterToken = $accessToken['id_token'];
                }
            } else {

                $accessToken = json_decode(file_get_contents($tokenPath), true);
                $sisterToken = $accessToken['id_token'];
            }
        } else {
            if (!MyHelper::wsSisterLogin()) {
                throw new \Exception("Error Creating SISTER Token", 1);
            } else {
                $accessToken = json_decode(file_get_contents($tokenPath), true);
                $sisterToken = $accessToken['id_token'];
            }
        }

        return $sisterToken;
    }

    public static function wsSisterLogin()
    {
        try {
            $sister_baseurl = Yii::$app->params['sister']['baseurl'];
            $sister_id_pengguna = Yii::$app->params['sister']['id_pengguna'];
            $sister_username = Yii::$app->params['sister']['username'];
            $sister_password = Yii::$app->params['sister']['password'];
            $headers = ['content-type' => 'application/json'];
            $client = new \GuzzleHttp\Client([
                'timeout'  => 10.0,
                'headers' => $headers,
                // 'base_uri' => 'http://sister.unida.gontor.ac.id/api.php/0.1'
            ]);
            // $full_url = $sister_baseurl.'/Login';
            $id_token = '';
            $full_url = $sister_baseurl . '/authorize';

            $response = $client->post($full_url, [
                'body' => json_encode([
                    'username' => $sister_username,
                    'password' => $sister_password,
                    'id_pengguna' => $sister_id_pengguna
                ]),
                'headers' => ['Content-type' => 'application/json']

            ]);

            $response = json_decode($response->getBody());

            $data = [
                'id_token' => $response->token,
                'created_at' => date('Y-m-d H:i:s')
            ];

            $tokenPath = Yii::getAlias('@webroot') . '/credentials/sister_token.json';


            file_put_contents($tokenPath, json_encode($data));
            return true;
        } catch (\Exception $e) {
            print_r($e->getMessage());
            exit;
            return false;
        }
    }

    public static function getStatusAktifDosen()
    {
        $roles = [
            'aktif' => 'AKTIF',
            'cuti' => 'CUTI',
            'tugasbelajar' => 'Tugas Belajar',
            'resign' => 'Resign',
            'nonaktif' => 'Non-Aktif'
        ];


        return $roles;
    }

    public static function getListHariEn()
    {
        $list_hari = [
            'SABTU' => 'Saturday',
            'AHAD' => 'Sunday',
            'SENIN' => 'Monday',
            'SELASA' => 'Tuesday',
            'RABU' => 'Wednesday',
            'KAMIS' => 'Thursday'
        ];

        return $list_hari;
    }

    public static function getBagian()
    {
        $list_bagian = [
            '1' => 'Auditi',
            '2' => 'Auditor',
            '3' => 'Admin',
        ];

        return $list_bagian;
    }

    public static function getListHari()
    {
        $list_hari = [
            'SABTU' => 'SABTU',
            'AHAD' => 'AHAD',
            'SENIN' => 'SENIN',
            'SELASA' => 'SELASA',
            'RABU' => 'RABU',
            'KAMIS' => 'KAMIS'
        ];

        return $list_hari;
    }

    public static function getMatkulKurikulum($id_kurikulum, $kode_mata_kuliah)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl
        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'GetMatkulKurikulum',
            'token'   => $token,
            'filter' => "id_kurikulum='" . $id_kurikulum . "' AND kode_mata_kuliah = '" . $kode_mata_kuliah . "' "
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $resp = $response->getBody()->getContents();

        $resp = json_decode($resp);

        if ($resp->error_code == 0) {
            if (count($resp->data) > 0) {

                $result = [
                    'code' => 200,
                    'message' => 'Okay',
                    'data' => $resp->data[0],
                ];
            } else {
                $result = [
                    'code' => 404,
                    'message' => 'Okay',
                    'data' => null,
                ];
            }
        } else if ($resp->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error: ' . $resp->error_code . ' - ' . $resp->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'data' => null,
            ];
        }

        return $result;
    }

    public static function insertMatkulKurikulum($params)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl
        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'InsertMatkulKurikulum',
            'token'   => $token,
            'record' => $params
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $resp = $response->getBody()->getContents();

        $resp = json_decode($resp);

        if ($resp->error_code == 0) {
            $result = [
                'code' => 200,
                'message' => 'Okay',
                'id_matkul' => $resp->data->id_matkul,
                'id_kurikulum' => $resp->data->id_kurikulum
            ];
        } else if ($resp->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error: ' . $resp->error_code . ' - ' . $resp->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'id_matkul' => '',
                'id_kurikulum' => ''
            ];
        }

        return $result;
    }

    public static function insertKurikulum($params)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl
        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'InsertKurikulum',
            'token'   => $token,
            'record' => $params
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $resp = $response->getBody()->getContents();

        $resp = json_decode($resp);

        if ($resp->error_code == 0) {
            $result = [
                'code' => 200,
                'message' => 'Okay',
                'id_kurikulum' => $resp->data->id_kurikulum,
            ];
        } else if ($resp->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error Insert Kurikulum: ' . $resp->error_code . ' - ' . $resp->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'id_kurikulum' => '',
            ];
        }

        return $result;
    }


    public static function getDetailKurikulum($id_prodi, $nama_kurikulum)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl
        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'GetDetailKurikulum',
            'token'   => $token,
            'filter' => "id_prodi='" . $id_prodi . "' AND nama_kurikulum = '" . $nama_kurikulum . "' "
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $resp = $response->getBody()->getContents();

        $resp = json_decode($resp);

        if ($resp->error_code == 0) {
            if (count($resp->data) > 0) {

                $result = [
                    'code' => 200,
                    'message' => 'Okay',
                    'data' => $resp->data[0],
                ];
            } else {
                $result = [
                    'code' => 404,
                    'message' => 'Okay',
                    'data' => null,
                ];
            }
        } else if ($resp->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error: ' . $resp->error_code . ' - ' . $resp->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'data' => null,
            ];
        }

        return $result;
    }

    public static function getKelasKuliah($id_prodi, $id_semester, $id_dosen, $id_matkul, $kelas)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl

        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'GetDetailKelasKuliah',
            'token'   => $token,
            'filter' => "id_prodi='" . $id_prodi . "' AND id_semester='" . $id_semester . "' AND id_matkul = '" . $id_matkul . "' AND nama_kelas_kuliah = '" . $kelas . "' "
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $results = $response->getBody()->getContents();

        $results = json_decode($results);

        if ($results->error_code == 0) {

            $hasils = $results->data;
            if (count($hasils) > 0) {

                $result = [
                    'code' => 200,
                    'message' => 'Okay',
                    'data' => $hasils[0]
                ];
            } else {
                $result = [
                    'code' => 404,
                    'message' => 'Okay',
                    'data' => null
                ];
            }
        } else if ($results->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error: ' . $results->error_code . ' - ' . $results->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'data' => null
            ];
        }

        return $result;
    }

    public static function insertKelasKuliah($params)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl
        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'InsertKelasKuliah',
            'token'   => $token,
            'record' => $params
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $resp = $response->getBody()->getContents();

        $resp = json_decode($resp);

        if ($resp->error_code == 0) {
            $result = [
                'code' => 200,
                'message' => 'Okay',
                'id_kelas_kuliah' => $resp->data->id_kelas_kuliah,
            ];
        } else if ($resp->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error Insert Kelas Kuliah: ' . $resp->error_code . ' - ' . $resp->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'id_kelas_kuliah' => '',
            ];
        }

        return $result;
    }

    public static function getDosenPengajarKelasKuliah($id_kelas_kuliah, $id_registrasi_dosen)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl

        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'GetDosenPengajarKelasKuliah',
            'token'   => $token,
            'filter' => "id_kelas_kuliah='" . $id_kelas_kuliah . "' AND id_registrasi_dosen='" . $id_registrasi_dosen . "' "
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $results = $response->getBody()->getContents();

        $results = json_decode($results);

        if ($results->error_code == 0) {

            $hasils = $results->data;
            if (count($hasils) > 0) {

                $result = [
                    'code' => 200,
                    'message' => 'Okay',
                    'data' => $hasils[0]
                ];
            } else {
                $result = [
                    'code' => 404,
                    'message' => 'Dosen Pengajar Belum Ditemukan',
                    'data' => null
                ];
            }
        } else if ($results->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error: ' . $results->error_code . ' - ' . $results->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'data' => null
            ];
        }

        return $result;
    }

    public static function insertDosenPengajarKelasKuliah($params)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl
        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'InsertDosenPengajarKelasKuliah',
            'token'   => $token,
            'record' => $params
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $resp = $response->getBody()->getContents();

        $resp = json_decode($resp);

        if ($resp->error_code == 0) {
            $result = [
                'code' => 200,
                'message' => 'Okay',
                'id_aktivitas_mengajar' => $resp->data->id_aktivitas_mengajar,
            ];
        } else if ($resp->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error Insert Dosen Pengajar Kelas Kuliah: ' . $resp->error_code . ' - ' . $resp->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'id_aktivitas_mengajar' => '',
            ];
        }

        return $result;
    }

    public static function getPesertaKelasKuliah($id_kelas_kuliah, $id_registrasi_mahasiswa)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl

        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'GetPesertaKelasKuliah',
            'token'   => $token,
            'filter' => "id_kelas_kuliah='" . $id_kelas_kuliah . "' AND id_registrasi_mahasiswa='" . $id_registrasi_mahasiswa . "' "
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $results = $response->getBody()->getContents();

        $results = json_decode($results);

        if ($results->error_code == 0) {

            $hasils = $results->data;
            if (count($hasils) > 0) {

                $result = [
                    'code' => 200,
                    'message' => 'Okay',
                    'data' => $hasils[0]
                ];
            } else {
                $result = [
                    'code' => 404,
                    'message' => 'Mahasiswa Belum Ditemukan',
                    'data' => null
                ];
            }
        } else if ($results->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error: ' . $results->error_code . ' - ' . $results->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'data' => null
            ];
        }

        return $result;
    }

    public static function deletePesertaKelasKuliah($keys, $params)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl
        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'DeletePesertaKelasKuliah',
            'token'   => $token,
            'key' => $keys,
            'record' => $params
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $resp = $response->getBody()->getContents();

        $resp = json_decode($resp);

        if ($resp->error_code == 0) {
            $result = [
                'code' => 200,
                'message' => 'Okay',
                'id_kelas_kuliah' => '',
            ];
        } else if ($resp->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error Delete Peserta Kelas Kuliah: ' . $resp->error_code . ' - ' . $resp->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'id_kelas_kuliah' => '',
            ];
        }

        return $result;
    }

    public static function insertPesertaKelasKuliah($params)
    {

        $result = [];

        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl
        ]);
        $headers = ['Content-Type' => 'application/json'];
        refreshDetail:
        $token = MyHelper::getFeederToken();

        $params = [
            'act' => 'InsertPesertaKelasKuliah',
            'token'   => $token,
            'record' => $params
        ];

        $response = $client->request('POST', '/ws/live2.php', [
            'headers' => $headers,
            'body' => json_encode($params)
        ]);

        $resp = $response->getBody()->getContents();

        $resp = json_decode($resp);

        if ($resp->error_code == 0) {
            $result = [
                'code' => 200,
                'message' => 'Okay',
                'id_registrasi_mahasiswa' => $resp->data->id_registrasi_mahasiswa,
                'id_kelas_kuliah' => $resp->data->id_kelas_kuliah,
            ];
        } else if ($resp->error_code == 100) {
            MyHelper::wsFeederLogin();
            goto refreshDetail;
        } else {
            $errors = 'Error Insert Peserta Kelas Kuliah: ' . $resp->error_code . ' - ' . $resp->error_desc;
            $result = [
                'code' => 500,
                'message' => $errors,
                'id_registrasi_mahasiswa' => '',
                'id_kelas_kuliah' => '',
            ];
        }

        return $result;
    }

    public static function getFeederToken()
    {
        $feederToken = '';
        $tokenPath = Yii::getAlias('@webroot') . '/credentials/feeder_token.json';

        if (file_exists($tokenPath)) {
            $accessToken = json_decode(file_get_contents($tokenPath), true);
            $feederToken = $accessToken['id_token'];
        } else {
            if (!MyHelper::wsFeederLogin()) {
                throw new \Exception("Error Creating FEEDER Token", 1);
            } else {
                $accessToken = json_decode(file_get_contents($tokenPath), true);
                $feederToken = $accessToken['id_token'];
            }
        }

        return $feederToken;
    }

    public static function wsFeederLogin()
    {
        $feeder_baseurl = Yii::$app->params['feeder']['baseurl'];
        $feeder_username = Yii::$app->params['feeder']['username'];
        $feeder_password = Yii::$app->params['feeder']['password'];

        $client = new \GuzzleHttp\Client([
            'base_uri' => $feeder_baseurl

        ]);
        $token = '';
        $errors = '';
        try {
            $headers = ['Content-Type' => 'application/json'];

            $params = [
                'act' => 'GetToken',
                'username'   => $feeder_username,
                'password'   => $feeder_password,
            ];


            $response = $client->request('POST', '/ws/live2.php', [
                'headers' => $headers,
                'body' => json_encode($params)
            ]);

            $results = $response->getBody()->getContents();
            $results = json_decode($results);
            if ($results->error_code == 0) {
                $data = [
                    'id_token' => $results->data->token,
                    'created_at' => date('Y-m-d H:i:s')
                ];

                $tokenPath = Yii::getAlias('@webroot') . '/credentials/feeder_token.json';


                file_put_contents($tokenPath, json_encode($data));
            } else {
                $errors .= $results->error_desc;
                throw new \Exception;
            }

            return true;
        } catch (\Exception $e) {
            $errors .= $e->getMessage();
            print_r($errors);
            exit;
            return false;
        }
    }


    public static function getStatusBeasiswa()
    {
        return ['0' => 'Belum Diproses', '1' => 'Diterima', '2' => 'Ditolak'];
    }

    public static function getJenisBeasiswa()
    {
        return ['1' => 'Pemerintah', '2' => 'Universitas', '3' => 'Pondok'];
    }

    public static function getListJenjang()
    {
        return ['A' => 'Ph.D - S3', 'B' => 'Magister - S2', 'C' => 'Bachelor - S1', 'D' => 'Diploma 4 - D4'];
    }

    public static function gen_uuid()
    {
        return sprintf(
            '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
            // 32 bits for "time_low"
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),

            // 16 bits for "time_mid"
            mt_rand(0, 0xffff),

            // 16 bits for "time_hi_and_version",
            // four most significant bits holds version number 4
            mt_rand(0, 0x0fff) | 0x4000,

            // 16 bits, 8 bits for "clk_seq_hi_res",
            // 8 bits for "clk_seq_low",
            // two most significant bits holds zero and one for variant DCE1.1
            mt_rand(0, 0x3fff) | 0x8000,

            // 48 bits for "node"
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff),
            mt_rand(0, 0xffff)
        );
    }

    public static function getStatusSkripsi()
    {
        return [
            '1' => 'Belum Sidang Akhir', '2' => 'Sudah Sidang Akhir Belum Revisi', '3' => 'Sudah Sidang Akhir Sudah Revisi', '4' => 'Ganti Topik'
        ];
    }

    public static function getRubrikCatatanHarian()
    {
        return ['1' => 'Tidak ada catatan', '2' => 'Sudah ada catatan, namun hanya laporan', '3' => 'Sudah ada catatan, namun hanya laporan dan evaluasi', '4' => 'Sudah ada catatan dan lengkap, namun kemajuan tidak/kurang signifikan', '5' => 'Sudah ada catatan, lengkap, dan ada kemajuan signifikan'];
    }

    public static function getStatusProposal()
    {
        return [
            '1' => 'Belum Sidang', '2' => 'Sudah Sidang Belum Revisi', '3' => 'Sudah Sidang Sudah Revisi', '4' => 'Ganti Topik'
        ];
    }

    public static function getLamaHari()
    {
        return [
            '1' => '1 hari', '3' => '3 hari', '7' => '1 minggu', '14' => '2 minggu', '21' => '3 minggu', '30' => '1 bulan'
        ];
    }

    public static function appendZeros($str, $charlength = 6)
    {

        return str_pad($str, $charlength, '0', STR_PAD_LEFT);
    }

    public static function konversiEkdAngkaHuruf($skor)
    {
        $huruf = 'F';
        $ket  = '-';
        $color = 'default';
        if ($skor >= 126) {
            $huruf = 'A';
            $color = 'success';
            $ket  = 'Dilaporkan kepada Rektor untuk diberi reward sertifikat dan insentif tertentu penambah semangat kerja.';
        } else if ($skor >= 101) {

            $huruf = 'B';
            $color = 'warning';
            $ket  = 'Dilaporkan kepada Rektor untuk diberi reward sertifikat.';
        } else if ($skor >= 76) {
            $huruf = 'C';
            $color = 'warning';
            $ket  = 'Dilaporkan kepada Rektor bahwa yang bersangkutan telah mencukupi kinerjanya.';
        } else if ($skor >= 51) {
            $huruf = 'D';
            $color = 'warning';
            $ket  = 'Dilaporkan kepada Rektor untuk diberi peringatan.';
        } else if ($skor >= 30) {
            $huruf = 'E';
            $color = 'danger';
            $ket  = 'Dilaporkan kepada Rektor untuk diberikan sanksi tertentu yang mendukung peningkatan kinerjanya.';
        }

        return ['huruf' => $huruf, 'ket' => $ket, 'color' => $color];
    }

    public static function numberToAlphabet($index)
    {
        $alphabet = range('A', 'Z');

        return $alphabet[$index]; // returns D
    }

    public static function getListAbsensiFull()
    {
        $list = [
            '1' => 'Hadir',
            '2' => 'Izin',
            '3' => 'Saki',
            '4' => 'Ghoib'

        ];

        return $list;
    }

    public static function getListAbsensi()
    {
        $list = [
            '1' => 'H',
            '2' => 'I',
            '3' => 'S',
            '4' => 'G'

        ];

        return $list;
    }

    public static function getListIzin()
    {
        $list = [
            '1' => 'Belum Disetujui',
            '2' => 'Disetujui',
            '3' => 'Ditolak'

        ];

        return $list;
    }

    public static function getListSemester()
    {
        $list_semester = [
            1 => 'Semester 1',
            2 => 'Semester 2',
            3 => 'Semester 3',
            4 => 'Semester 4',
            5 => 'Semester 5',
            6 => 'Semester 6',
            7 => 'Semester 7',
            8 => 'Semester 8',
            9 => 'Semester 9 ke atas',
        ];

        return $list_semester;
    }

    public static function getSemester()
    {
        $list_semester = [
            0 => [1 => 1, 2 => 2],
            1 => [3 => 3, 4 => 4],
            2 => [5 => 5, 6 => 6],
            3 => [7 => 7, 8 => 8],
        ];

        return $list_semester;
    }

    public static function convertTanggalEnLengkap($date)
    {
        $timestamp = strtotime($date);
        return date("l, d F Y", $timestamp);
    }

    public static function convertTanggalIndo($date)
    {
        $bulan = array(
            1 =>   'Januari',
            'Februari',
            'Maret',
            'April',
            'Mei',
            'Juni',
            'Juli',
            'Agustus',
            'September',
            'Oktober',
            'November',
            'Desember'
        );
        $pecahkan = explode('-', $date);

        // variabel pecahkan 0 = tanggal
        // variabel pecahkan 1 = bulan
        // variabel pecahkan 2 = tahun
        if (!empty($pecahkan[2]) && !empty($pecahkan[1]) && !empty($pecahkan[0]))
            return $pecahkan[2] . ' ' . $bulan[(int)$pecahkan[1]] . ' ' . $pecahkan[0];
        else
            return 'invalid date format';
    }

    public static function converTanggalIndoLengkap($date)
    {
        $timestamp = strtotime($date);

        // Convert the day name to Indonesian
        $dayName = date("l", $timestamp);
        $dayNames = array(
            'Sunday' => 'Minggu',
            'Monday' => 'Senin',
            'Tuesday' => 'Selasa',
            'Wednesday' => 'Rabu',
            'Thursday' => 'Kamis',
            'Friday' => 'Jum\'at',
            'Saturday' => 'Sabtu'
        );
        $dayNameIndonesian = $dayNames[$dayName];
        return $dayNameIndonesian . ', ' . MyHelper::convertTanggalIndo($date);
    }

    public static function getTerbilang($nilai)
    {
        $declare    = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");

        if ($nilai < 12)                return " " . $declare[$nilai];
        elseif ($nilai < 20)            return MyHelper::getTerbilang($nilai - 10) . " belas";
        elseif ($nilai < 100)           return MyHelper::getTerbilang($nilai / 10) . " puluh" . MyHelper::getTerbilang($nilai % 10);
        elseif ($nilai < 200)           return " seratus" . MyHelper::getTerbilang($nilai - 100);
        elseif ($nilai < 1000)          return MyHelper::getTerbilang($nilai / 100) . " ratus" . MyHelper::getTerbilang($nilai % 100);
        elseif ($nilai < 2000)          return " seribu" . MyHelper::getTerbilang($nilai - 1000);
        elseif ($nilai < 1000000)       return MyHelper::getTerbilang($nilai / 1000) . " ribu" . MyHelper::getTerbilang($nilai % 1000);
        elseif ($nilai < 1000000000)    return MyHelper::getTerbilang($nilai / 1000000) . " juta" . MyHelper::getTerbilang($nilai % 1000000);
        elseif ($nilai < 1000000000000) return MyHelper::getTerbilang($nilai / 1000000000) . " milyar" . MyHelper::getTerbilang($nilai % 1000000000);
    }

    public static function getStatusAktivitas()
    {
        $roles = [
            'A' => 'AKTIF', 'C' => 'CUTI', 'D' => 'DO', 'K' => 'KELUAR', 'L' => 'LULUS', 'N' => 'NON-AKTIF', 'G' => 'DOUBLE DEGREE', 'M' => 'MUTASI'
        ];


        return $roles;
    }

    public static function getStatusAmi()
    {
        $roles = [
            '0' => 'PROSES', '1' => 'SELESAI'
        ];


        return $roles;
    }

    public static function isBetween($sd, $ed)
    {
        return (($sd >= $ed) && ($sd <= $ed));
    }

    public static function dmYtoYmd($tgl)
    {
        $date = str_replace('/', '-', $tgl);
        return date('Y-m-d H:i:s', strtotime($date));
    }

    public static function YmdtodmY($tgl)
    {
        return date('d-m-Y H:i:s', strtotime($tgl));
    }

    public static function ymdhisToYmd($tgl)
    {
        return date('1 jS \of F Y h:i:s', strtotime($tgl));
    }



    public static function hitungDurasi($date1, $date2)
    {
        $date1 = new \DateTime($date1);
        $date2 = new \DateTime($date2);
        $interval = $date1->diff($date2);

        $elapsed = '';
        if ($interval->d > 0)
            $elapsed = $interval->format('%a hari %h jam %i menit %s detik');
        else if ($interval->h > 0)
            $elapsed = $interval->format('%h jam %i menit %s detik');
        else
            $elapsed = $interval->format('%i menit %s detik');


        return $elapsed;
    }

    public static function hitungUmur($tgl_lahir)
    {
        $tanggal = new \DateTime($tgl_lahir);

        $today = new \DateTime('today');

        // tahun
        $y = $today->diff($tanggal)->y;

        // bulan
        $m = $today->diff($tanggal)->m;

        // hari
        $d = $today->diff($tanggal)->d;
        return $y;
    }

    public static function logError($model)
    {
        $errors = '';
        foreach ($model->getErrors() as $attribute) {
            foreach ($attribute as $error) {
                $errors .= $error . ' ';
            }
        }

        return $errors;
    }

    public static function formatRupiah($value, $decimal = 0)
    {
        return number_format($value, $decimal, ',', '.');
    }

    public static function getSelisihHariInap($old, $new)
    {
        $date1 = strtotime($old);
        $date2 = strtotime($new);
        $interval = $date2 - $date1;
        return round($interval / (60 * 60 * 24)) + 1;
    }

    public static function getRandomString($minlength = 12, $maxlength = 12, $useupper = true, $usespecial = false, $usenumbers = true)
    {
        $key = '';
        $charset = "abcdefghijklmnopqrstuvwxyz";

        if ($useupper) $charset .= "ABCDEFGHIJKLMNOPQRSTUVWXYZ";

        if ($usenumbers) $charset .= "0123456789";

        if ($usespecial) $charset .= "~@#$%^*()_±={}|][";

        for ($i = 0; $i < $maxlength; $i++) $key .= $charset[(mt_rand(0, (strlen($charset) - 1)))];

        return $key;
    }
}
