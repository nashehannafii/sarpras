<?php

namespace app\controllers;

use app\helpers\MyHelper;
use Yii;
use app\models\RuanganRiwayat;
use app\models\RuanganRiwayatSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * RuanganRiwayatController implements the CRUD actions for RuanganRiwayat model.
 */
class RuanganRiwayatController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'delete', 'index', 'view'],
                'rules' => [
                    [
                        'actions' => [
                            'create', 'update', 'delete', 'index', 'view', 'ajax-save'
                        ],
                        'allow' => true,
                        'roles' => ['admin', 'theCreator'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RuanganRiwayat models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RuanganRiwayatSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RuanganRiwayat model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RuanganRiwayat model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RuanganRiwayat();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAjaxSave()
    {

        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        try {
            $dataPost = $_POST;

            if (!empty($dataPost)) {

                if ($dataPost['id'] == null) $model = new RuanganRiwayat();
                else $model = RuanganRiwayat::findOne($dataPost['id']);

                if ($dataPost['status_aktif'] == 1) {
                    $activeRuanganRiwayat = RuanganRiwayat::findActive($dataPost['ruangan_id'])->all();

                    foreach ($activeRuanganRiwayat as $RuanganRiwayat) {
                        $modelAktif = RuanganRiwayat::findOne($RuanganRiwayat->id);
                        $modelAktif->status_aktif = 0;
                        $modelAktif->save();
                    }
                }

                $model->attributes = $dataPost;

                if ($model->save()) {

                    $transaction->commit();
                    $results = [
                        'code'      => 200,
                        'message'   => 'Data successfully added'
                    ];
                }
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }

        echo json_encode($results);
        exit;
    }

    /**
     * Updates an existing RuanganRiwayat model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RuanganRiwayat model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id, $item)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['item/view', 'id' => $item]);
    }

    /**
     * Finds the RuanganRiwayat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RuanganRiwayat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RuanganRiwayat::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
