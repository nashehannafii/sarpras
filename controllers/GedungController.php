<?php

namespace app\controllers;

use app\helpers\MyHelper;
use Yii;
use app\models\gedung;
use app\models\gedungSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * gedungController implements the CRUD actions for gedung model.
 */
class GedungController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'delete', 'index', 'view', 'detail'],
                'rules' => [
                    [
                        'actions' => [
                            'create', 'update', 'delete', 'index', 'view', 'detail'
                        ],
                        'allow' => true,
                        'roles' => ['admin', 'theCreator'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all gedung models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new gedungSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = gedung::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['gedung']);
            $post = ['gedung' => $posted];
            if ($model->load($post)) {
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $errors = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => $errors]);
                }
            }

            echo $out;
            return;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single gedung model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    public function actionDetail($id)
    {
        return $this->render('detail', [
            'model' => $this->findModel($id),
            'listLabel' => MyHelper::getLabelList(),
        ]);
    }

    /**
     * Creates a new gedung model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */

    public function actionCreate()
    {

        $model          = new gedung;
        $connection     = \Yii::$app->db;
        $transaction    = $connection->beginTransaction();
        $s3config       = Yii::$app->params['s3'];

        $s3new          = new \Aws\S3\S3Client($s3config);
        $errors         = '';

        try {

            $dataPost   = $_POST;
            if (Yii::$app->request->post()) {

                $update = false;
                if (isset($dataPost['gedung']['id'])) {
                    $model = $this->findModel($dataPost['gedung']['id']);
                    $update = true;
                } else {
                    $model = new gedung;
                }

                $model->attributes  = $dataPost['gedung'];
                $model->gambar_path = UploadedFile::getInstance($model, 'gambar_path');
                // $jenis              = MasterJenis::findOne($model->jenis_id);

                if ($model->gambar_path) {
                    $curdate    = date('d-m-y');

                    $file_name  = $model->nama . '-' . $curdate;
                    $s3path     = $model->gambar_path->tempName;
                    $s3type     = $model->gambar_path->type;
                    $key        = 'gedung' . '/' . $file_name . '-' . $model->gambar_path->name;

                    $insert = $s3new->putObject([
                        'Bucket'        => 'sarpras',
                        'Key'           => $key,
                        'Body'          => 'Body',
                        'SourceFile'    => $s3path,
                        'ContenType'    => $s3type,
                    ]);

                    $plainUrl = $s3new->getObjectUrl('sarpras', $key);
                    $model->gambar_path = $plainUrl;

                    if ($model->save()) {

                        // foreach ($dataPost['Pengelompokan']['id'] as $value) {
                        //     $data = [
                        //         'pengelompokan_id'  => $value,
                        //         'gedung_id'        => $model->id
                        //     ];
                        //     $pengelompokanDatas[] = $data;
                        // }

                        // if ($update) {

                        //     Kelompokgedung::deleteAll(['gedung_id'  => $model->id]);
                        //     $batchBobots = Yii::$app->db->createCommand()->batchInsert('kelompok_gedung', [
                        //         'pengelompokan_id',
                        //         'gedung_id',
                        //     ], $pengelompokanDatas);
                        // } else {
                        //     $batchBobots = Yii::$app->db->createCommand()->batchInsert('kelompok_gedung', [
                        //         'pengelompokan_id',
                        //         'gedung_id',
                        //     ], $pengelompokanDatas);
                        // }

                        // if ($batchBobots->execute()) {
                        // } else {
                        //     $errors .= MyHelper::logError($batchBobots);
                        //     throw new \Exception;
                        // }

                        $transaction->commit();
                        Yii::$app->session->setFlash('success', "Data tersimpan");
                        return $this->redirect(['index']);
                    }
                }
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            Yii::$app->session->setFlash('danger', $errors);
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            Yii::$app->session->setFlash('danger', $errors);
        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }


    public function actionFoto($id)
    {
        $model = gedung::findOne($id);
        if (!empty($model->gambar)) {
            try {
                $image = imagecreatefromstring($this->getImage($model->gambar));

                header('Content-Type: image/png');
                imagepng($image);
            } catch (\Exception $e) {
            }
        }

        die();
    }

    /**
     * Updates an existing gedung model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {

        $model          = $this->findModel($id);

        $connection     = \Yii::$app->db;
        $transaction    = $connection->beginTransaction();
        $s3config       = Yii::$app->params['s3'];

        $s3new          = new \Aws\S3\S3Client($s3config);
        $errors         = '';

        try {

            $dataPost   = $_POST;
            if (Yii::$app->request->post()) {

                $model = $this->findModel($id);
                $gambar_path = $model->gambar_path;

                $model->attributes  = $dataPost['gedung'];
                $model->gambar_path      = UploadedFile::getInstance($model, 'gambar_path');

                if ($model->gambar_path) {
                    $curdate    = date('d-m-y');

                    $file_name  = $model->nama . '-' . $curdate;
                    $s3path     = $model->gambar_path->tempName;
                    $s3type     = $model->gambar_path->type;
                    $key        = 'gedung' . '/' . $file_name . '-' . $model->gambar_path->name;

                    $insert = $s3new->putObject([
                        'Bucket'        => 'sarpras',
                        'Key'           => $key,
                        'Body'          => 'Body',
                        'SourceFile'    => $s3path,
                        'ContenType'    => $s3type,
                    ]);

                    $plainUrl = $s3new->getObjectUrl('sarpras', $key);
                    $model->gambar_path = $plainUrl;

                    if ($model->save()) {

                        $transaction->commit();
                        Yii::$app->session->setFlash('success', "Data tersimpan");
                        return $this->redirect(['index']);
                    }
                } else {
                    $model->gambar_path = $gambar_path;
                    if ($model->save()) {

                        $transaction->commit();
                        Yii::$app->session->setFlash('success', "Data tersimpan");
                        return $this->redirect(['index']);
                    }
                }
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            Yii::$app->session->setFlash('danger', $errors);
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            Yii::$app->session->setFlash('danger', $errors);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing gedung model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the gedung model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return gedung the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = gedung::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
