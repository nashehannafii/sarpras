<?php

namespace app\controllers;

use app\helpers\MyHelper;
use app\models\gedung;
use Yii;
use app\models\RuanganRiwayatSearch;
use app\models\PeminjamanRuanganSearch;
use app\models\Ruangan;
use app\models\RuanganRiwayat;
use app\models\RuanganSearch;
use DateTime;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * ItemController implements the CRUD actions for Ruangan model.
 */
class RuanganController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'delete', 'index', 'view', 'detail', 'ajax-get-ruangan-kosong'],
                'rules' => [
                    [
                        'actions' => [
                            'create', 'update', 'delete', 'index', 'view', 'detail', 'ajax-get-ruangan-kosong'
                        ],
                        'allow' => true,
                        'roles' => ['admin', 'theCreator'],
                    ],
                    [
                        'actions' => [
                            'create', 'update', 'index', 'view', 'detail', 'ajax-get-ruangan-kosong'
                        ],
                        'allow' => true,
                        'roles' => ['unitKerja'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Item models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RuanganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = Ruangan::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Ruangan']);
            $post = ['Ruangan' => $posted];
            if ($model->load($post)) {
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $errors = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => $errors]);
                }
            }

            echo $out;
            return;
        }

        $gedung = ArrayHelper::map(gedung::find()->all(), 'id', 'nama');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'listLabel' => MyHelper::getLabelList(),
            'gedung' => $gedung,
        ]);
    }

    /**
     * Displays a single Item model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $searchLabel = new RuanganRiwayatSearch();
        $dataLabel = $searchLabel->search(Yii::$app->request->queryParams, $id);

        $searchPeminjaman = new PeminjamanRuanganSearch();
        $dataPeminjaman = $searchPeminjaman->search(Yii::$app->request->queryParams, $id);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = RuanganRiwayat::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Label']);

            if ($posted['status_aktif'] == 1) {
                $activeLabel = RuanganRiwayat::findActive($model['ruangan_id'])->all();

                foreach ($activeLabel as $label) {
                    $modelAktif = RuanganRiwayat::findOne($label->id);
                    $modelAktif->status_aktif = 0;
                    $modelAktif->save();
                }
            }

            $post = ['Label' => $posted];
            if ($model->load($post)) {
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $errors = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => $errors]);
                }
            }

            echo $out;
            return;
        }

        return $this->render('view', [
            'model' => $this->findModel($id),
            'searchLabel' => $searchLabel,
            'dataLabel' => $dataLabel,
            'searchPeminjaman' => $searchPeminjaman,
            'dataPeminjaman' => $dataPeminjaman,
        ]);
    }

    public function actionDetail($id, $date = null)
    {

        $date = new DateTime($date == null ? date('Y-m-d') : $date);
        $weekStart = $date->modify('this week')->format('Y-m-d');
        $weekEnd = $date->modify('this week +6 days')->format('Y-m-d');

        $dates = array();
        $startDate = new DateTime($weekStart);
        $endDate = new DateTime($weekEnd);

        while ($startDate <= $endDate) {
            $dates[] = $startDate->format('Y-m-d');
            $startDate->modify('+1 day');
        }

        return $this->render('detail', [
            'model' => $this->findModel($id),
            'pekans' => $dates,
            'listLabel' => MyHelper::getLabelList(),
        ]);
    }

    /**
     * Creates a new Item model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {

        $model          = new Ruangan();
        $connection     = \Yii::$app->db;
        $transaction    = $connection->beginTransaction();
        $s3config       = Yii::$app->params['s3'];

        $s3new          = new \Aws\S3\S3Client($s3config);
        $errors         = '';

        try {

            $dataPost   = $_POST;
            if (Yii::$app->request->post()) {

                $update = false;
                if (isset($dataPost['Item']['id'])) {
                    $model = $this->findModel($dataPost['Item']['id']);
                    $update = true;
                } else {
                    $model = new Ruangan;
                }

                $model->attributes  = $dataPost['Item'];
                $model->gambar_path = UploadedFile::getInstance($model, 'gambar_path');
                // $jenis              = MasterJenis::findOne($model->jenis_id);

                if ($model->gambar_path) {
                    $curdate    = date('d-m-y');

                    $file_name  = $model->nama . '-' . $curdate;
                    $s3path     = $model->gambar_path->tempName;
                    $s3type     = $model->gambar_path->type;
                    $key        = 'ruangan' . '/' . $file_name . '-' . $model->gambar_path->name;

                    $insert = $s3new->putObject([
                        'Bucket'        => 'sarpras',
                        'Key'           => $key,
                        'Body'          => 'Body',
                        'SourceFile'    => $s3path,
                        'ContenType'    => $s3type,
                    ]);

                    $plainUrl = $s3new->getObjectUrl('sarpras', $key);
                    $model->gambar_path = $plainUrl;

                    if ($model->save()) {

                        $modelLabel = new RuanganRiwayat();
                        $modelLabel->ruangan_id = $model->id;
                        $modelLabel->nama = $model->nama;
                        $modelLabel->status_aktif = 1;

                        if ($modelLabel->save()) {

                            $transaction->commit();
                            Yii::$app->session->setFlash('success', "Data tersimpan");
                            return $this->redirect(['view', 'id' => $model->id]);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            Yii::$app->session->setFlash('danger', $errors);
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            Yii::$app->session->setFlash('danger', $errors);
        }


        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Item model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */

    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $s3config = Yii::$app->params['s3'];
        $s3new = new \Aws\S3\S3Client($s3config);
        $errors = '';

        try {
            if (Yii::$app->request->post()) {
                $dataPost = Yii::$app->request->post();
                $ruangan = $dataPost['Ruangan'];

                $model = $this->findModel($id);
                $gambar_path = $model->gambar_path;
                $riwayat = false;

                if ($model->nama != $ruangan['nama'] || $model->kapasitas != $ruangan['kapasitas']) {
                    $activeRuanganRiwayat = RuanganRiwayat::findActive($id)->all();
                    foreach ($activeRuanganRiwayat as $ruanganRiwayat) {
                        $ruanganRiwayat->status_aktif = 0;
                        $ruanganRiwayat->save();
                    }

                    $modelRiwayat = new RuanganRiwayat();
                    $modelRiwayat->ruangan_id = $model->id;
                    $modelRiwayat->no_ruangan = $model->no_ruangan;
                    $modelRiwayat->attributes = $ruangan;

                    $riwayat = true;
                }

                $model->attributes = $ruangan;
                $model->gambar_path = UploadedFile::getInstance($model, 'gambar_path');

                if ($model->gambar_path) {
                    $curdate = date('d-m-y');
                    $file_name = $model->nama . '-' . $curdate;
                    $s3path = $model->gambar_path->tempName;
                    $s3type = $model->gambar_path->type;
                    $key = 'ruangan' . '/' . $file_name . '-' . $model->gambar_path->name;

                    $insert = $s3new->putObject([
                        'Bucket' => 'sarpras',
                        'Key' => $key,
                        'Body' => 'Body',
                        'SourceFile' => $s3path,
                        'ContentType' => $s3type,
                    ]);

                    $plainUrl = $s3new->getObjectUrl('sarpras', $key);
                    $model->gambar_path = $plainUrl;

                    if ($riwayat == false) {
                        $activeRuanganRiwayat = RuanganRiwayat::findActive($id)->all();
                        foreach ($activeRuanganRiwayat as $ruanganRiwayat) {
                            $ruanganRiwayat->status_aktif = 0;
                            $ruanganRiwayat->save();
                        }

                        $modelRiwayat = new RuanganRiwayat();
                        $modelRiwayat->ruangan_id = $model->id;
                        $modelRiwayat->no_ruangan = $model->no_ruangan;
                        $modelRiwayat->attributes = $ruangan;
                        $modelRiwayat->gambar_path = $plainUrl;
                    } else {
                        $modelRiwayat->gambar_path = $plainUrl;
                    }

                    if ($model->save() && $modelRiwayat->save()) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t('app', "Data updated"));
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                } else {
                    $model->gambar_path = $gambar_path;
                    $modelRiwayat->gambar_path = $gambar_path;

                    if ($model->save() && $modelRiwayat->save()) {
                        $transaction->commit();
                        Yii::$app->session->setFlash('success', Yii::t('app', "Data updated"));
                        return $this->redirect(['view', 'id' => $model->id]);
                    }
                }
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            Yii::$app->session->setFlash('danger', $errors);
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            Yii::$app->session->setFlash('danger', $errors);
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }


    public function actionAjaxGetRuanganKosong()
    {
        $dataPost = $_POST;

        if (isset($dataPost)) {

            $items = Ruangan::findReadyItem($dataPost['tanggal'], $dataPost['gedung']);

            $data = [];
            $getLabel = MyHelper::getLabelList();
            foreach ($items as $i) {

                $item = [
                    'id' => $i->id,
                    'nama_label' => $getLabel[$i->id],
                    'nama_gedung' => $i->gedung->nama,
                ];
                $data[] = $item;
            }

            if (isset($data)) {
                $results = [
                    'code' => 200,
                    'tanggal' => $dataPost['tanggal'],
                    'item' => $data
                ];
            } else {
                $results = [
                    'code' => 500,
                    'item' => "Data tidak ditemukan"
                ];
            }

            echo json_encode($results);
            exit;
        }
    }

    public function actionAjaxGetRuangan()
    {
        $dataPost = $_POST;

        if (isset($dataPost)) {

            $ruangans = RuanganRiwayat::find()->where(['status_aktif' => 1])->all();

            $ruangan = [];
            foreach ($ruangans as $r) {
                $ruangan[] = $r->nama;
            }

            if (isset($ruangans)) {
                $results = [
                    'code' => 200,
                    'ruangan' => $ruangan,
                ];
            } else {
                $results = [
                    'code' => 500,
                    'item' => "Data tidak ditemukan"
                ];
            }

            echo json_encode($results);
            exit;
        }
    }

    /**
     * Deletes an existing Item model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Item model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Item the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Ruangan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
