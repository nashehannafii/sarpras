<?php

namespace app\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use app\helpers\MyHelper;

use app\models\LoginForm;
use app\models\User;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\ContactForm;
use app\models\gedung;
use app\models\Item;
use app\models\Label;
use app\models\Peminjaman;
use app\models\PeminjamanItem;
use app\models\PeminjamanRuangan;
use app\models\Ruangan;
use app\models\RuanganRiwayat;
use app\models\UnitKerja;
use DateTime;
use \Firebase\JWT\JWT;
use yii\httpclient\Client;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public $successUrl = '';
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['logout', 'signup', 'testing'],
                'rules' => [
                    [
                        'actions' => [
                            'testing'
                        ],
                        'allow' => true,
                        'roles' => ['theCreator'],
                    ],
                    [
                        'actions' => ['signup', 'test'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'successCallback'],
                'successUrl' => $this->successUrl
            ],
        ];
    }



    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {

        if (Yii::$app->user->isGuest) $this->redirect(['peminjaman/map']);

        $gedung = Gedung::find();

        foreach ($gedung->select('id')->column() as $gedung_id) {
            $dataFrekuensiGedung[] = (int)Peminjaman::findBygedung($gedung_id)->count();
        }

        return $this->render('index', [
            'parents' => $gedung->select('nama')->column(),
            'dataFrekuensiGedung' => $dataFrekuensiGedung,
            'months' => MyHelper::getMonthsAgo(12)
        ]);
    }

    public function actionLanguage()
    {
        if (isset($_POST['lang'])) {
            Yii::$app->language = $_POST['lang'];
            $cookie = new yii\web\Cookie([
                'name' => 'lang',
                'value' => $_POST['lang']
            ]);

            Yii::$app->getResponse()->getCookies()->add($cookie);
        }
    }

    public function actionGetBarChart()
    {

        $months = array();
        $tahun = $_POST['tahun'] ?? date('Y');
        $sekarang = date($tahun . '-11-31');

        $bulan_kebelakang = strtotime('-11 months', strtotime($sekarang));

        for ($i = 0; $i < 12; $i++) {
            $bulan_tahun = date('F Y', $bulan_kebelakang);
            $bulan_kebelakang = strtotime('+1 month', $bulan_kebelakang);
            array_push($months, $bulan_tahun);
        }

        $dataRekapgedung = [];

        $gedung = Gedung::find();
        foreach ($gedung->select('id')->column() as $gedung_id) {

            $dataIsi = [];
            $test = [];

            foreach ($months as $dateString) {
                $date = DateTime::createFromFormat('F Y', $dateString);
                $date->setDate($date->format('Y'), $date->format('n') - 1, 1);
                $date->modify('+1 month');

                $beginDate = $date->format('Y-m-d');
                $date->modify('last day of this month');
                $endDate = $date->format('Y-m-d');

                $test[] = $beginDate;

                $isi = (int)Peminjaman::findBygedung($gedung_id, $beginDate, $endDate)->count();
                $dataIsi[] = $isi;
            }

            $dataRekapgedung[] = [
                'name' => $gedung->select('nama')->where(['id' => $gedung_id])->scalar(),
                'data' => $dataIsi,
            ];
        }

        $response = array(
            'categories' => $months,
            'dataRekapgedung' => $dataRekapgedung,
        );

        header('Content-Type: application/json');
        echo json_encode($response);
    }


    public function actionGetTopRank()
    {
        $dataPost = $_POST;

        $tahun = $dataPost['tahun'] ?? date('Y');

        $start = date('Y-m-d', strtotime($tahun . '-01-01'));
        $end = date('Y-m-d', strtotime($tahun . '-12-31'));

        $topUser = Peminjaman::find()
            ->select(['user_id', 'COUNT(*) AS peminjaman_count'])
            ->groupBy('user_id')
            ->where(['between', 'created_at', $start, $end])
            ->andWhere(['status_peminjaman' => 1])
            ->orderBy(['peminjaman_count' => SORT_DESC])
            ->limit(3)
            ->asArray()
            ->all();

        $topRooms = PeminjamanRuangan::find()
            ->joinWith(['ruanganRiwayat as r', 'ruanganRiwayat.ruangan as rr', 'ruanganRiwayat.ruangan.gedung as rrg', 'peminjaman as p'])
            ->select(['rr.nama AS nama_ruangan', 'rrg.nama AS nama_gedung', 'COUNT(*) AS peminjaman_count'])
            ->where(['between', 'tanggal_pinjam', $start, $end])
            ->andWhere(['p.status_peminjaman' => 1])
            ->groupBy('r.ruangan_id')
            ->orderBy(['peminjaman_count' => SORT_DESC])
            ->limit(3)
            ->asArray()
            ->all();

        $topUsers = [];

        foreach ($topUser as $user) {
            $topUsers[] = [
                'nama_unit' => UnitKerja::find()->where(['user_id' => $user['user_id']])->select('nama')->one() ?? 'Admin',
                'peminjaman_count' => $user['peminjaman_count'],
            ];
        }

        $response = array(
            'topRooms' => $topRooms,
            'topUsers' => $topUsers,
        );

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function actionGetPieChart()
    {

        $dataRekapgedung = [];
        $dataRekapItem = [];

        $dataPost = $_POST;
        $monthId = $dataPost['month'] ?? null;

        if (isset($dataPost['mulai']) && isset($dataPost['selesai'])) {
            # code...
            $mulai = date('Y-m-d', strtotime($dataPost['mulai']));
            $selesai = date('Y-m-d', strtotime($dataPost['selesai']));

            $mulaiDate = DateTime::createFromFormat('d-m-Y', $dataPost['mulai']);
            $selesaiDate = DateTime::createFromFormat('d-m-Y', $dataPost['selesai']);

            $convertedMulai = $mulaiDate->format('d/m/Y');
            $convertedSelesai = $selesaiDate->format('d/m/Y');
        }

        if ($monthId != null) {
            # code...
            $months = array_reverse(MyHelper::getMonthsAgo(12));

            $dateString = $months[$monthId];
            $date = DateTime::createFromFormat('F Y', $dateString);
            $date->setDate($date->format('Y'), $date->format('n'), 1);
            $dateTimeStart = $date->format('Y-m-d');

            $date->modify('+1 month');
            $dateTimeEnd = $date->format('Y-m-d');
        } else {
            $dateTimeStart = $mulai;
            $dateTimeEnd = $selesai;
        }

        $gedung = Gedung::find()->all();
        $items = Ruangan::find();
        $peminjamanItems = PeminjamanRuangan::find();

        $totalItemCount = $peminjamanItems->joinWith(['peminjaman as p'])
            ->andWhere(['p.status_peminjaman' => 1])
            ->andWhere(['between', 'tanggal_pinjam', $dateTimeStart, $dateTimeEnd])
            ->count();

        foreach ($gedung as $i) {

            $dataPeminjamanItems = PeminjamanRuangan::find()
                ->joinWith(['ruanganRiwayat.ruangan as l', 'peminjaman as p'])
                ->where(['l.gedung_id' => $i->id])
                ->andWhere(['p.status_peminjaman' => 1])
                ->andWhere(['between', 'tanggal_pinjam', $dateTimeStart, $dateTimeEnd])
                ->all();

            $jumlahData = count($dataPeminjamanItems);

            if ($jumlahData != 0) {
                # code...
                $totalDatagedung = $jumlahData / $totalItemCount * 100;
            } else {
                $totalDatagedung = 0;
            }

            $dataRekapgedung[] = [
                'name' => $i->nama,
                'y' => $totalDatagedung,
                'drilldown' => $i->nama,
            ];

            $dataItems = [];

            foreach ($items->where(['gedung_id' => $i->id])->all() as $item) {

                $dataLabel = RuanganRiwayat::findActive($item->id)->one();
                $totalItem = PeminjamanRuangan::find()
                    ->joinWith(['peminjaman as p', 'ruanganRiwayat as rr'])
                    ->where(['rr.ruangan_id' => $item->id])
                    ->andWhere(['between', 'tanggal_pinjam', $dateTimeStart, $dateTimeEnd])
                    ->andWhere(['p.status_peminjaman' => 1])
                    ->all();

                if (count($totalItem) != 0) {
                    $persentaseItem = count($totalItem) / count($dataPeminjamanItems) * 100;
                } else {
                    $persentaseItem = 0;
                }
                $dataItems[] = [
                    $dataLabel->nama, $persentaseItem
                ];
            }

            $dataRekapItem[] = [
                'name' => $i->nama,
                'id' => $i->nama,
                'data' => $dataItems
            ];
        }

        $response = array(
            'dataRekapgedung' => $dataRekapgedung,
            'dataRekapItem' => $dataRekapItem,
            'month' => $months[$monthId] ?? $convertedMulai . ' - ' . $convertedSelesai,
        );

        header('Content-Type: application/json');
        echo json_encode($response);
    }

    public function actionRegister()
    {

        // if (Yii::$app->user->isGuest) {
        $user = new User();

        if (!$user->load(Yii::$app->request->post())) {
            return $this->render('register', ['user' => $user]);
        }

        $pwd = $user->password;
        $user->setPassword($pwd);
        $user->generateAuthKey();
        $user->status = '10';
        $user->access_role = 'mahasiswa';

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();

        if ($user->validate()) {
            try {

                $user->created_at = strtotime(date('YmdHis'));
                $user->updated_at = strtotime(date('YmdHis'));

                if ($user->save()) {
                    $auth = Yii::$app->authManager;
                    $role = $auth->getRole($user->access_role);
                    $info = $auth->assign($role, $user->getId());

                    if (!$info)
                        throw new \Exception('There was some error while saving user role.');

                    $transaction->commit();
                } else {
                    $errors = MyHelper::logError($user);
                    Yii::$app->session->setFlash('error', Yii::t('app', $errors));
                }
            } catch (\Exception $e) {
                $transaction->rollBack();
                $errors = $e->getMessage();


                Yii::$app->session->setFlash('error', Yii::t('app', $errors));
            }
        } else
            return $this->render('login');
        // } else {
        //     return $this->goBack();
        // }

        return $this->redirect('index');
    }

    public function actionAuthCallback()
    {
        $results = [];
        header('Content-Type: application/json');
        try {
            if (!empty($_SERVER['HTTP_X_JWT_TOKEN'])) {

                $request_method = $_SERVER["REQUEST_METHOD"];

                switch ($request_method) {
                    case 'POST':
                        $token = $_SERVER['HTTP_X_JWT_TOKEN'];
                        $key = Yii::$app->params['jwt_key'];
                        $decoded = JWT::decode($token, base64_decode(strtr($key, '-_', '+/')), ['HS256']);
                        $results = [
                            'code' => 200,
                            'message' => 'Valid'
                        ];
                        break;
                    default:
                        header("HTTP/1.0 405 Method Not Allowed");
                        $results = [
                            'code' => 405,
                            'message' => $request_method . ' Method not allowed '
                        ];

                        echo json_encode($results);
                        exit;
                        break;
                }
            } else {
                header("HTTP/1.0 401 Bad Request");

                $results = [
                    'code' => 401,
                    'message' => 'Unauthorized Request '
                ];

                echo json_encode($results);
                exit;
            }
        } catch (\Exception $e) {

            $results = [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }

        header('Content-Type: application/json');
        echo json_encode($results);

        die();
    }

    public function actionLoginSso($token)
    {
        // print_r($token);exit;

        $key = Yii::$app->params['jwt_key'];
        $decoded = JWT::decode($token, base64_decode(strtr($key, '-_', '+/')), ['HS256']);

        $uuid = $decoded->uuid; // will print "1"
        $user = \app\models\User::find()
            ->where([
                'uuid' => $uuid,
            ])
            ->one();

        if (!empty($user)) {

            $session = Yii::$app->session;
            $session->set('token', $token);


            // $exp = $total_bkd * 1000;
            // $exp += $totalCatatanHarian;
            // $level = MasterLevel::getLevel($exp);
            // $currentClass = GameLevelClass::getCurrentClass($level);
            // $nextLevel = MasterLevel::getNextLevel($exp);
            // $remainingExp = $nextLevel['nextExp'] - $exp;

            // $session->set('level',$user->level);
            // $session->set('class',$currentClass['class']);
            // $session->set('rank',$currentClass['rank']);
            // $session->set('stars',$currentClass['stars']);
            // $session->set('remainingExp',$remainingExp);
            // print_r($user);exit;
            // Yii::$app->user->login($user);
            $activity = new \wdmg\activity\models\Activity;
            $activity->setActivity(Yii::$app->user->identity->username . ' has successfully login.', 'login', 'info', 2);
            return $this->redirect(['site/index']);
        } else {


            return $this->redirect($decoded->iss . '/site/sso-callback?code=302')->send();
        }
    }

    public function actionLoginOtp($otp)
    {

        $user = User::find()
            ->where([
                'otp' => $otp,
            ])
            ->one();

        if (!empty($user)) {
            $api_baseurl = Yii::$app->params['invoke_token_uri'];
            $client = new Client(['baseUrl' => $api_baseurl]);
            $headers = [];

            $params = [
                'uuid' => $user->uuid
            ];

            $response = $client->get($api_baseurl, $params, $headers)->send();
            if ($response->isOk) {
                $res = $response->data;

                if ($res['code'] != '200') {
                    return $this->redirect(Yii::$app->params['sso_login']);
                } else {
                    $session = Yii::$app->session;
                    $session->set('token', $res['token']);
                    $user->otp = null;
                    $user->save(false, ['otp']);

                    $api_baseurl = Yii::$app->params['api_baseurl'];
                    $client = new Client(['baseUrl' => $api_baseurl]);
                    $client_token = Yii::$app->params['client_token'];
                    $headers = ['x-access-token' => $client_token];

                    $results = [];

                    // Yii::$app->user->login($user);
                    return $this->redirect(['site/index']);
                }
            }
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Invalid OTP. Please contact your department administrator.'));

            return $this->redirect(['site/login']);
        }
    }

    public function successCallback($client)
    {

        $attributes = $client->getUserAttributes();
        // print_r($client);exit;
        $user = User::find()
            ->where([
                'email' => $attributes['email'],
            ])
            ->one();

        if (!empty($user)) {

            // Yii::$app->user->login($user);
        } else {
            Yii::$app->session->setFlash('error', Yii::t('app', 'Invalid or Unregistered Email. Please use a valid unida.gontor.ac.id email address.'));

            return $this->redirect(['site/login']);
        }
    }



    public function actionHomelog()
    {

        if (Yii::$app->user->isGuest) {
            return $this->goBack();
        } else {
            $user = \app\models\User::findByEmail(Yii::$app->user->identity->email);
            $model = $user->dataDiri;
            return $this->render('homelog', ['model' => $model,]);
        }
    }


    public function actionUbahAkun()
    {
        $id = Yii::$app->user->identity->ID;
        // load user data
        $user = User::findOne($id);

        if (!$user->load(Yii::$app->request->post())) {
            return $this->render('ubahAkun', ['user' => $user]);
        }

        // only if user entered new password we want to hash and save it
        if ($user->password) {
            $user->setPassword($user->password);
        }


        if (!$user->save()) {
            return $this->render('ubahAkun', ['user' => $user]);
        }

        Yii::$app->session->setFlash('success', Yii::t('app', 'Data user telah diupdate.'));
        return $this->redirect(['ubah-akun']);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            // echo '<pre>';print_r('tes');die;
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {

        // $session = Yii::$app->session;
        // $session->remove('token');
        Yii::$app->user->logout();
        // $url = Yii::$app->params['sso_logout'];
        return $this->redirect(['index']);
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
