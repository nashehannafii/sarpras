<?php

namespace app\controllers;

use Yii;
use app\models\Jam;
use app\models\JamSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\Json;

/**
 * JamController implements the CRUD actions for Jam model.
 */
class JamController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'delete', 'index', 'view'],
                'rules' => [
                    [
                        'actions' => [
                            'create', 'update', 'delete', 'index', 'view'
                        ],
                        'allow' => true,
                        'roles' => ['admin', 'theCreator'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Jam models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JamSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = Jam::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);


            $posted = current($_POST['Jam']);
            $post = ['Jam' => $posted];
            if ($model->load($post)) {
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $errors = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => $errors]);
                }
            }

            echo $out;

            return;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Jam model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Jam model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Jam();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Jam model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionSubselesai()
    {
        $out = [];
        
        if (isset($_POST['depdrop_parents'])) {
            $parents = $_POST['depdrop_parents'];
            
            if ($parents != null) {
                $jam = $parents[0];
                $query = Jam::find()->where(['>', 'id', $jam]);
                $jams = $query->all();

                foreach ($jams as $jam) {
                    $out[$jam->id] = [
                        'id' => $jam->id,
                        'name' => $jam->jam
                    ];
                }
                echo Json::encode(['output' => $out, 'selected' => '']);
                die();
            }
        }
    }

    public function actionAjaxGetJams()
    {
        $dataPost = $_POST;

        if (isset($dataPost)) {

            $datas = Jam::find()->all();

            $data = [];
            
            foreach ($datas as $d) {

                $item = [
                    'id' => $d->id,
                    'jam' => $d->jam,
                ];
                $data[] = $item;
            }

            if (isset($data)) {
                $results = [
                    'code' => 200,
                    'item' => $data
                ];
            } else {
                $results = [
                    'code' => 500,
                    'item' => "Data tidak ditemukan"
                ];
            }

            echo json_encode($results);
            exit;
        }
    }

    /**
     * Deletes an existing Jam model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Jam model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Jam the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Jam::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
