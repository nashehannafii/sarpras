<?php

namespace app\controllers;

use app\helpers\MyHelper;
use app\models\Jam;
use Yii;
use app\models\PeminjamanRuangan;
use app\models\PeminjamanRuanganSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

/**
 * PeminjamanRuanganController implements the CRUD actions for PeminjamanItem model.
 */
class PeminjamanRuanganController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'delete', 'index', 'view', 'ajax-get-data-peminjaman', 'ajax-add-data-peminjaman', 'ajax-drop-data-peminjaman'],
                'rules' => [
                    [
                        'actions' => [
                            'create', 'update', 'delete', 'index', 'view', 'ajax-get-data-peminjaman', 'ajax-add-data-peminjaman', 'ajax-drop-data-peminjaman'
                        ],
                        'allow' => true,
                        'roles' => ['admin', 'theCreator'],
                    ],
                    [
                        'actions' => [
                            'ajax-get-data-peminjaman', 'ajax-add-data-peminjaman', 'ajax-drop-data-peminjaman'
                        ],
                        'allow' => true,
                        'roles' => ['unitKerja'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PeminjamanItem models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PeminjamanRuanganSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single PeminjamanItem model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new PeminjamanRuangan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PeminjamanRuangan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAjaxGetDataPeminjaman()
    {
        $dataPost = $_POST;
        $results = [];
        $data = [];

        $datas = PeminjamanRuangan::find()->where(['peminjaman_id' => $dataPost['peminjaman_id']])->all();

        foreach ($datas as $d) {
            $data[] = [
                'ruangan_riwayat_id' => $d->ruangan_riwayat_id,
                'label' => MyHelper::getLabelList()[$d->ruangan_riwayat_id],
                'gedung' => $d->ruanganRiwayat->ruangan->gedung->nama,
                'tanggal_pinjam' => $d->tanggal_pinjam,
                'mulai_id' => $d->mulai->jam,
                'selesai_id' => $d->selesai->jam,
            ];
        }

        if ($data) {

            $results = [
                'code' => 200,
                'message' => 'Data founded',
                'item' => $data,
            ];
        } else {
            $results = [
                'code' => 500,
                'message' => 'Data not found'
            ];
        }

        echo json_encode($results);
        exit;
    }

    public function actionCheckByPeminjamanId()
    {
        $dataPost = $_POST;
        $results = [];
        
        $datas = PeminjamanRuangan::find()->where(['peminjaman_id' => $dataPost['data']])->all();

        foreach ($datas as $d) {
            $data = [
                'id' => $d->ruangan_riwayat_id . $d->tanggal_pinjam
            ];

            $results[] = $data;
        }

        if (isset($datas)) {

            $results = [
                'code' => 200,
                'items' => $results
            ];
        } else {
            $results = [
                'code' => 500,
                'message' => 'Data not found'
            ];
        }

        echo json_encode($results);
        exit;
    }

    public function actionAjaxAddPeminjamanItem()
    {
        $dataPost = $_POST;
        $results = [];

        $model = new PeminjamanRuangan();

        $model->attributes = $dataPost;

        if ($model->save()) {

            $results = [
                'code' => 200,
                'message' => 'Data added'
            ];
        } else {
            $results = [
                'code' => 500,
                'message' => 'Data not found'
            ];
        }

        echo json_encode($results);
        exit;
    }

    public function actionAjaxDropPeminjamanItem()
    {
        $dataPost = $_POST;
        $results = [];

        if (strlen($_POST['mulai_id']) > 1) {
            $dataPost['mulai_id'] = Jam::find()->select('id')->where(['jam' => $dataPost['mulai_id']])->one();
            $dataPost['selesai_id'] = Jam::find()->select('id')->where(['jam' => $dataPost['selesai_id']])->one();
        }

        $model = PeminjamanRuangan::findSpesifikItem($dataPost['ruangan_riwayat_id'], $dataPost['tanggal_pinjam'], $dataPost['peminjaman_id'], $dataPost['mulai_id'], $dataPost['selesai_id']);

        if ($model !== null) {
            // Delete the model
            if ($model->delete()) {

                $results = [
                    'code' => 200,
                    'message' => 'Data removed'
                ];
            } else {
                $results = [
                    'code' => 500,
                    'message' => 'Data not found'
                ];
            }
        }

        echo json_encode($results);
        exit;
    }

    /**
     * Updates an existing PeminjamanItem model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing PeminjamanItem model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PeminjamanItem model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PeminjamanItem the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PeminjamanRuangan::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    // protected function findModelPeminjamanItem($ruangan_riwayat_id, $tanggal_pinjam, $peminjaman_id, $mulai_id, $selesai_id)
    // {
    //     $model = PeminjamanRuangan::find()->where([['ruangan_riwayat_id' => $ruangan_riwayat_id, 'tanggal_pinjam' => $tanggal_pinjam, 'peminjaman_id' => $peminjaman_id, 'mulai_id' => $mulai_id, 'selesai_id' => $selesai_id]])->one();
    //     if (($model) !== null) {
    //         return $model;
    //     }

    //     throw new NotFoundHttpException('The requested page does not exist.');
    // }
}
