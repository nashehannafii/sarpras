<?php

namespace app\controllers;

use app\helpers\MyHelper;
use Yii;
use app\models\UnitKerja;
use app\models\UnitKerjaSearch;
use app\models\User;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\httpclient\Client;
use yii\web\Response;

/**
 * UnitKerjaController implements the CRUD actions for UnitKerja model.
 */
class UnitKerjaController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'delete', 'index', 'ajax-import', 'ajax-import-simuda', 'ajax-send'],
                'rules' => [
                    [
                        'actions' => [
                            'create', 'update', 'delete', 'index', 'ajax-import', 'ajax-import-simuda', 'ajax-send'
                        ],
                        'allow' => true,
                        'roles' => ['theCreator', 'admin'],
                    ],

                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UnitKerja models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UnitKerjaSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UnitKerja model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UnitKerja model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UnitKerja();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    public function actionAjaxImport()
    {
        $results = [];
        $api_baseurl = Yii::$app->params['api_baseurl'];
        $client = new Client(['baseUrl' => $api_baseurl]);
        $client_token = Yii::$app->params['client_token'];
        $headers = ['x-access-token' => $client_token];

        $response = $client->get('/simpeg/list/unitkerja', [], $headers)->send();
        $list_dosen = [];
        $list_target_rasio = [];
        $list_jenjang = MyHelper::listJenjangStudi();
        if ($response->isOk) {
            $counter = 0;
            $list_unit = $response->data['values'];
            foreach ($list_unit as $unit) {
                // echo '<pre>';print_r($list_unit);die;

                $u = UnitKerja::findOne($unit['id']);
                if (empty($u)) {
                    $u = new UnitKerja;
                    $u->id = $unit['id'];
                }

                $u->nama = $unit['nama'];
                if ($u->save()) {
                    $counter++;
                } else {
                    $err = MyHelper::logError($u);
                    print_r($err);
                    exit;
                }
            }

            $results = [
                'code' => 200,
                'message' => $counter . ' data successfully imported'
            ];
        }

        echo \yii\helpers\Json::encode($results);
        exit;
    }

    public function actionAjaxImportSimuda()
    {
        $results = [];
        $api_baseurl = Yii::$app->params['api_simudaurl'];
        $client = new Client(['baseUrl' => $api_baseurl]);
        $client_token = Yii::$app->params['client_token'];
        $headers = ['x-access-token' => $client_token];

        $response = $client->get('/api/get-unit-kerja', [], $headers)->send();
        $list_dosen = [];
        $list_target_rasio = [];
        $list_jenjang = MyHelper::listJenjangStudi();
        if ($response->isOk) {
            $counter = 0;
            $list_unit = $response->data;
            foreach ($list_unit as $unit) {

                $u = UnitKerja::findOne($unit['id']);
                if (empty($u)) {
                    $u = new UnitKerja;
                    $u->id = $unit['id'];
                }

                $u->nama = $unit['nama'];
                $u->jenis = $unit['jenis'];
                $u->singkatan = $unit['singkatan'];
                $u->email = $unit['email'];
                $u->penanggung_jawab = $unit['penanggung_jawab'];
                
                if ($u->save()) {
                    $counter++;
                } else {
                    $err = MyHelper::logError($u);
                    print_r($err);
                    exit;
                }
            }

            $results = [
                'code' => 200,
                'message' => $counter . ' data successfully imported'
            ];
        }

        echo json_encode($results);
        exit;
    }

    public function actionAjaxSend()
    {
        $results = [];

        $errors = '';
        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();
        $dataPost = $_POST;

        $model = $this->findModel($dataPost['id']);


        try {
            if (!empty($dataPost)) {


                $model->is_sent = 1;
                $model->save();

                // Add User Auditee...

                $user = User::findOne([
                    'unit_kerja_id' => $model->id,
                ]);

                // echo '<pre>';print_r($user);die;
                $is_user = false;
                if (empty($user)) {
                    $user = new User;
                    $is_user = true;
                }

                $password = MyHelper::getRandomString(8, 8, true, false, false);

                $user->username = $model->email;
                $user->email = $model->email;
                $user->nama = $model->nama;
                $user->unit_kerja_id = $model->id;

                $user->setPassword($password);
                $user->status = User::STATUS_ACTIVE;
                $user->access_role = 'unitKerja';
                $user->created_at = date('Y-m-d');
                $user->updated_at = date('Y-m-d');

                $auth = Yii::$app->authManager;
                $oldRole = $auth->getRole('unitKerja');
                
                $user->access_role = $oldRole->name;

                // echo '<pre>';print_r($user);die;
                if ($user->save()) {

                    if ($is_user == true)
                        $auth->assign($oldRole, $user->id);

                    $to = MyHelper::sendEmail($user->email);

                    $emailTemplate = $this->renderPartial('email', [
                        'user' => $user,
                        'password' => $password
                    ]);

                    Yii::$app->mailer->compose()
                        ->setTo($to)
                        ->setFrom([Yii::$app->params['supportEmail'] => 'SARPRAS UNIDA Gontor'])
                        ->setSubject('[SARPRAS] Account Information')
                        ->setHtmlBody($emailTemplate)
                        ->send();


                    // $toBpm = MyHelper::emailBpm();
                    // Yii::$app->mailer->compose()
                    //     ->setTo($toBpm)
                    //     ->setFrom([Yii::$app->params['supportEmail'] => 'SIMUDA UNIDA Gontor'])
                    //     ->setSubject('[SIMUDA] Account Information')
                    //     ->setHtmlBody($emailTemplate)
                    //     ->send();
                        
                } else {
                    $errors .= MyHelper::logError($model);
                    throw new \Exception;
                }

                $transaction->commit();
                $results = [
                    'code' => 200,
                    'message' => 'Data successfully updated'
                ];
            } else {
                $errors .= 'Oops, you cannot POST empty data';
                throw new \Exception;
            }
        } catch (\Exception $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            // $errors .= 'Oops, your data is incomplete, please complete the data';

            $results = [
                'code' => 500,
                'message' => $errors
            ];
        } catch (\Throwable $e) {
            $transaction->rollBack();
            $errors .= $e->getMessage();
            $results = [
                'code' => 500,
                'message' => $errors
            ];
        }
        echo json_encode($results);
        exit;
    }

    /**
     * Updates an existing UnitKerja model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing UnitKerja model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the UnitKerja model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UnitKerja the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UnitKerja::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
