<?php

namespace app\controllers;

use app\helpers\MyHelper;
use app\models\gedung;
use Yii;
use app\models\Peminjaman;
use app\models\PeminjamanItem;
use app\models\PeminjamanRuangan;
use app\models\PeminjamanRuanganSearch;
use app\models\PeminjamanSearch;
use app\models\Ruangan;
use app\models\RuanganRiwayat;
use app\models\RuanganSearch;
use app\models\Sarpras;
use DateTime;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PeminjamanController implements the CRUD actions for Peminjaman model.
 */
class PeminjamanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'denyCallback' => function ($rule, $action) {
                    throw new \yii\web\ForbiddenHttpException('You are not allowed to access this page');
                },
                'only' => ['create', 'update', 'delete', 'index', 'submission', 'cetak-surat', 'ajax-get-data', 'canceled'],
                'rules' => [
                    [
                        'actions' => [
                            'create', 'update', 'delete', 'index', 'submission', 'cetak-surat', 'ajax-get-data', 'canceled'
                        ],
                        'allow' => true,
                        'roles' => ['theCreator'],
                    ],
                    [
                        'actions' => [
                            'index', 'submission', 'cetak-surat', 'ajax-get-data', 'canceled'
                        ],
                        'allow' => true,
                        'roles' => ['admin'],
                    ],
                    [
                        'actions' => [
                            'create', 'update', 'delete', 'index', 'ajax-get-data', 'canceled'
                        ],
                        'allow' => true,
                        'roles' => ['unitKerja'],
                    ],
                    [
                        'actions' => ['create', 'index', 'ajax-get-data'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Peminjaman models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PeminjamanSearch();
        $isAdmin = true;
        if (Yii::$app->user->can('admin')) $isAdmin = false;
        $status_peminjaman = [0, 1];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $isAdmin, $status_peminjaman);

        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $id = Yii::$app->request->post('editableKey');
            $model = Peminjaman::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);

            $posted = current($_POST['Peminjaman']);

            if ($posted['status_peminjaman'] != null) {

                $model->is_sent = 1;
                $email = Yii::$app->user->identity->unitKerja->email ?? 'nashehannafii@unida.gontor.ac.id';
                $status = $posted['status_peminjaman'];
                $this->sendMail($email, $status);
            }

            $post = ['Peminjaman' => $posted];
            if ($model->load($post)) {
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $errors = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => $errors]);
                }
            }

            echo $out;
            return;
        }

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionDetailData()
    {
        $searchModel = new PeminjamanRuanganSearch();

        $ruangan_id = RuanganRiwayat::find()->where(['nama' => $_GET, 'status_aktif' => 1])->select('ruangan_id')->one();

        $mulai = null;
        $selesai = null;

        if (isset($_GET['mulai']) && isset($_GET['selesai'])) {
            $mulai = date('Y-m-d', strtotime($_GET['mulai']));
            $selesai = date('Y-m-d', strtotime($_GET['selesai']));
        } else {
            $months = array_reverse(MyHelper::getMonthsAgo(12));

            $date = DateTime::createFromFormat('F Y', $months[$_GET['bulan'] != "" || $_GET['bulan'] != null ? $_GET['bulan'] : 0]);
            $date->setDate($date->format('Y'), $date->format('n') - 1, 1);
            $date->modify('+1 month');

            $mulai = $date->format('Y-m-d');
            $date->modify('last day of this month');
            $selesai = $date->format('Y-m-d');
        }

        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $ruangan_id, $mulai, $selesai);

        return $this->render('detail_data', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCanceled()
    {
        $searchModel = new PeminjamanSearch();
        $isAdmin = true;
        if (Yii::$app->user->can('admin')) $isAdmin = false;
        $status_peminjaman = [2];
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, $isAdmin, $status_peminjaman);

        return $this->render('canceled', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionMap($tanggal = null, $gedung = null)
    {

        $tanggal_filter = date('Y-m-d');
        $gedung_filter = gedung::find()->one();
        $gedung_filter = $gedung_filter->id;


        if (isset($tanggal)) $tanggal_filter = $tanggal;
        if (isset($gedung)) $gedung_filter = $gedung;

        return $this->render('map', [
            'tanggal' => $tanggal_filter,
            'gedung' => $gedung_filter,
            'listLabel' => MyHelper::getLabelList(),
        ]);
    }

    private function sendMail($email, $status)
    {
        $to = MyHelper::sendEmail($email);

        $emailTemplate = $this->renderPartial('mail_change_status', [
            // 'user' => $email,
            'status' => MyHelper::detailPeminjaman()['status_persetujuan'][$status],
        ]);

        Yii::$app->mailer->compose()
            ->setTo($to)
            ->setFrom([Yii::$app->params['supportEmail'] => 'SARPRAS UNIDA Gontor'])
            ->setSubject('[SARPRAS] booking Proposal Change Status Information')
            ->setHtmlBody($emailTemplate)
            ->send();
    }

    /**
     * Displays a single Peminjaman model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        if (Yii::$app->request->post('hasEditable')) {

            // instantiate your book model for saving
            $model = Peminjaman::findOne($id);

            // store a default json response as desired by editable
            $out = json_encode(['output' => '', 'message' => '']);


            $post = ['Peminjaman' => $_POST['Peminjaman']];

            if (Yii::$app->user->can('admin')) {
                $model->is_sent = 1;
                $email = Yii::$app->user->identity->unitKerja->email ?? 'nashehannafii@unida.gontor.ac.id';
                $status = $post['Peminjaman']['status_peminjaman'];
                $this->sendMail($email, $status);
            }

            if ($model->load($post)) {
                if ($model->save()) {
                    $out = json_encode(['output' => '', 'message' => '']);
                } else {
                    $errors = \app\helpers\MyHelper::logError($model);
                    $out = json_encode(['output' => '', 'message' => $errors]);
                }
            }

            echo $out;
            return;
        }

        $model = $this->findModel($id);

        $waktu = false;
        $status = false;

        $targetDateTime = new DateTime($model->tanggal_mulai);
        $currentDateTime = new DateTime();
        $targetDateTime->modify('+1 day');

        if ($currentDateTime >= $targetDateTime) $waktu = false;
        else $waktu = true;

        if ($model->status_peminjaman == 0) $status = true;

        return $this->render('view', [
            'model' => $model,
            'items' => $this->findItems($id),
            'waktu' => $waktu,
            'status' => $status,
        ]);
    }

    public function actionCart()
    {
        $peminjaman = Peminjaman::find()->where([
            'user_id' => Yii::$app->user->identity->id,
            'temp' => ""
        ])->one();

        return $this->render('cart', [
            'peminjaman' => $peminjaman->id ?? '',
        ]);
    }

    /**
     * Creates a new Peminjaman model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('danger', "<b>Access denied</b>, please login first!");
            return $this->redirect(['peminjaman/map']);
        }

        $model = new Peminjaman();
        $modelItem = new PeminjamanRuangan();

        $dataPost = $_POST;
        $data = [];
        $data['dataPost'] = $dataPost;

        if (!isset($dataPost['ruangan_id'])) return $this->redirect(['map']);
        if (empty($dataPost['Peminjaman'])) $data['item'] = Ruangan::findOne($dataPost['ruangan_id']);

        $connection = \Yii::$app->db;
        $transaction = $connection->beginTransaction();


        if (!empty($dataPost['Peminjaman'])) {

            if ($model->load($dataPost)) {

                $model->temp = '-';
                $model->tanggal_mulai = date('Y-m-d H:i:s');


                if ($model->save()) {

                    $modelItem->peminjaman_id = $model->id;
                    $modelItem->mulai_id = $model->mulai;
                    $modelItem->selesai_id = $model->selesai;
                    $modelItem->tanggal_pinjam = $model->tanggal_pinjam;
                    $modelItem->ruangan_riwayat_id = RuanganRiwayat::findActive($model->item)->one()->id;

                    if ($modelItem->save()) {

                        $transaction->commit();
                        Yii::$app->session->setFlash('success', "Data tersimpan");

                        return $this->redirect(['index']);
                    }
                }
            }
        }


        return $this->render('create', [
            'model' => $model,
            'data' => $data,
            'sarpras' => Sarpras::findOne(1),
        ]);
    }

    public function actionCreateAdvanced($step = 1)
    {
        if (Yii::$app->user->isGuest) {
            Yii::$app->session->setFlash('danger', "<b>Access denied</b>, please login first!");
            return $this->redirect(['peminjaman/map']);
        }

        switch ($step) {
            case 1:
                $peminjaman = Peminjaman::checkUserPeminjaman(Yii::$app->user->identity->id);

                if ($peminjaman == null) $model =  new Peminjaman();
                else $model = $peminjaman;

                $model->user_id = Yii::$app->user->identity->id;
                $model->jenis_peminjaman = 2;
                $model->status_peminjaman = 0;
                $model->temp = '';

                if (empty(Yii::$app->request->post()) && $peminjaman == null) $model->save();
                break;

            case 2:
                $model = Peminjaman::checkUserPeminjaman(Yii::$app->user->identity->id);
                break;

            case 3:
                $model = Peminjaman::checkUserPeminjaman(Yii::$app->user->identity->id);
                $model->tanggal_mulai = date('Y-m-d H:i:s');
                break;
        }


        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            Yii::$app->session->setFlash('success', "Data tersimpan");

            if ($step == 3) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                $step++;

                return $this->redirect(['create-advanced', 'step' => $step]);
            }
        }

        return $this->render('create_advanced', [
            'model' => $model,
            'step' => $step,
        ]);
    }

    public function actionCetakSurat($id)
    {

        $dataPeminjaman = Peminjaman::findOne($id);
        $dataSarpras = Sarpras::findOne(1);
        $items = PeminjamanRuangan::find()->where(['peminjaman_id' => $id])->orderBy(['ruangan_riwayat_id' => SORT_ASC, 'tanggal_pinjam' => SORT_ASC]);
        $jenis = $items->count() == 1 ? 1 : 2;

        switch ($jenis) {
            case 1:
                # code...
                $items = $items->one();
                break;
            case 2:
                # code...
                $items = $items->all();
                break;
        }

        $penanggung_jawab = !isset($dataPeminjaman->user->unitKerja->penanggung_jawab) ? 'admin' : $dataPeminjaman->user->unitKerja->penanggung_jawab;


        try {
            ob_start();

            // Layout
            $this->layout = '';
            $pdf = new \TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
            // $pdf = new \TCPDF('L', 'mm', 'F4', true, 'UTF-8', false);
            $pdf->SetPrintHeader(false);
            $pdf->SetPrintFooter(false);

            // Reg
            $fontpath = Yii::getAlias('@webroot') . '/klorofil/assets/fonts/pala.ttf';
            $fontpathbold = Yii::getAlias('@webroot') . '/klorofil/assets/fonts/palab.ttf';
            $fontpathitalic = Yii::getAlias('@webroot') . '/klorofil/assets/fonts/palai.ttf';
            $fontreg = \TCPDF_FONTS::addTTFfont($fontpath, 'TrueTypeUnicode', '', 86);
            $fontbold = \TCPDF_FONTS::addTTFfont($fontpathbold, 'TrueTypeUnicode', '', 86);
            $fontitalic = \TCPDF_FONTS::addTTFfont($fontpathitalic, 'TrueTypeUnicode', '', 86);
            $style      = array(
                'border' => false,
                'padding' => 0,
                'fgcolor' => array(0, 0, 0),
                'bgcolor' => false, //array(255,255,255)
            );
            $pdf->SetAutoPageBreak(false);
            $pdf->AddPage();

            // Lembar 1
            // Kop & Nomor Surat
            $kopIlkes = Yii::getAlias('@webroot') . '/klorofil/assets/img/kop-sarpras.png';
            $pdf->Image($kopIlkes, 1, 1, 210);

            // Content
            $pdf->setXY(10, 45);
            $pdf->SetFont($fontreg, '', 10);
            echo $this->renderPartial('cetak_surat', ['data' => $dataPeminjaman, 'items' => $items, 'sarpras' => $dataSarpras, 'jenis' => $jenis]);
            $data = ob_get_clean();
            $pdf->writeHTML($data);

            // Approval
            $pdf->setXY(10, 175);
            ob_start();
            $pdf->SetFont($fontreg, '', 10);
            echo $this->renderPartial('cetak_surat_ttd', ['data' => $dataPeminjaman, 'sarpras' => $dataSarpras]);
            $data = ob_get_clean();
            $pdf->writeHTML($data, 0, 0, 0, 0, 'C');

            // Logic Approval using QR
            $pdf->write2DBarcode("accepted_by#" . $penanggung_jawab . '#at#' . $dataPeminjaman->tanggal_pinjam, 'QRCODE,Q', 49, 185, 0, 18, $style, 'N');
            // $pdf->write2DBarcode("accepted_by#" . $dataPeminjaman->ketua_acara . '#at#' . $dataPeminjaman->tanggal_pinjam, 'QRCODE,Q', 143, 185, 0, 18, $style, 'N');

            if ($dataPeminjaman->status_peminjaman == 1) {
                # code...
                $pdf->write2DBarcode("accepted_by#" . $dataSarpras->kepala_sarpras, 'QRCODE,Q', 49, 230, 0, 18, $style, 'N');
                $pdf->write2DBarcode("accepted_by#staff_sarpras", 'QRCODE,Q', 143, 230, 0, 18, $style, 'N');
            }

            // Kop Bawah
            $pdf->setXY(10, $pdf->getPageHeight() - 10);
            $pdf->SetFont($fontreg, '', 8);
            $kopBawah = 'Head Office : Main Campus, University of Darussalam Gontor, Jl. Raya Siman, Ponorogo, East Java, Indonesia, 63471 <br> Phone (1234 5678 9123), Website : <a href="http://unida.gontor.ac.id/" target="_blank">http://unida.gontor.ac.id/</a>, email : <a href="mailto: bpp@unida.gontor.ac.id">bpp@unida.gontor.ac.id</a>';
            $y = $pdf->getY();
            $pdf->writeHTMLCell('', '', '', $y, $kopBawah, 0, 0, 0, true, 'C', true);

            if ($jenis == 2) {
                $pdf->AddPage();

                // // Lembar 1
                // // Kop & Nomor Surat
                $kopIlkes = Yii::getAlias('@webroot') . '/klorofil/assets/img/kop-sarpras.png';
                $pdf->Image($kopIlkes, 1, 1, 210);

                // Content
                $pdf->setXY(10, 45);
                $pdf->SetFont($fontreg, '', 10);
                echo $this->renderPartial('cetak_surat_lampiran', ['items' => $items]);
                $data = ob_get_clean();
                $pdf->writeHTML($data);


                $pdf->setXY(10, $pdf->getPageHeight() - 10);
                $pdf->SetFont($fontreg, '', 8);
                $kopBawah = 'Head Office : Main Campus, University of Darussalam Gontor, Jl. Raya Siman, Ponorogo, East Java, Indonesia, 63471 <br> Phone (1234 5678 9123), Website : <a href="http://unida.gontor.ac.id/" target="_blank">http://unida.gontor.ac.id/</a>, email : <a href="mailto: bpp@unida.gontor.ac.id">bpp@unida.gontor.ac.id</a>';
                $y = $pdf->getY();
                $pdf->writeHTMLCell('', '', '', $y, $kopBawah, 0, 0, 0, true, 'C', true);
            }


            //OUTPUT
            $pdf->Output('cetak_laporan_peminjaman' . '.pdf');
        } catch (\Exception $e) {
            echo $e;
            exit;
        }
        die();
    }

    public function actionDetailPeminjaman()
    {
        $searchModel = new PeminjamanSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('detail_peminjaman', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRange()
    {
        $dataPost = $_POST;
        $events = PeminjamanRuangan::find();
        $events->joinWith('peminjaman as p');


        if (isset($dataPost['data']['mulai']) && isset($dataPost['data']['selesai'])) {

            $start = date('Y-m-d', strtotime($dataPost['data']['mulai']));
            $end = date('Y-m-d', strtotime($dataPost['data']['selesai']));
        } else {

            $start = date('Y-m-d');
            $end = MyHelper::listRangeDate()[$_POST['data']]['end'];

            // if (isset($end)) $end = date('Y-m-d', strtotime($start . ' +1 week'));
        }

        $events->where(['between', 'tanggal_pinjam', $start, $end]);
        $events->andWhere(['p.status_peminjaman' => 1]);

        // if (isset($filter)) {
        // } else $events->where(['between', 'tanggal_pinjam', $currentDate, $oneWeekAfter]);

        $events = $events->orderBy(['tanggal_pinjam' => SORT_ASC])->all();

        $datas = [];

        foreach ($events as $event) {
            $data = [
                'nama' => $event->peminjaman->nama_acara,
                'tanggal' => $event->tanggal_pinjam,
                'gedung' => $event->ruanganRiwayat->ruangan->gedung->nama,
                'ruangan' => $event->ruanganRiwayat->nama,
            ];
            $datas[] = $data;
        }

        if (isset($datas)) {
            $results = [
                'code' => 200,
                'item' => $datas
            ];
        } else {
            $results = [
                'code' => 500,
                'item' => "Data tidak ditemukan"
            ];
        }

        echo json_encode($results);
        exit;
    }

    public function actionAdvanced()
    {
        $searchModel = new RuanganSearch();
        $dataProvider = $searchModel->searchBeruntun(Yii::$app->request->queryParams);

        if (!empty($_GET['gedung'])) {
            $cekBeruntun = [
                'gedung' => $_GET['gedung'],
                'tanggal-mulai' => $_GET['tanggal-mulai'],
                'tanggal-selesai' => $_GET['tanggal-selesai'],
            ];
            $dataProvider = $searchModel->searchBeruntun(Yii::$app->request->queryParams, $cekBeruntun);
        }

        return $this->render('advanced', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAjaxGetData()
    {
        $dataPost = $_POST;
        if (isset($_POST)) {

            $item = Peminjaman::findOne($dataPost['pinjam']);
            // echo '<pre>';print_r($item);die;

            if (isset($item)) {
                $results = [
                    'code' => 200,
                    'item' => [
                        'ketua'         => $item->ketua_acara,
                        'unit'          => isset($item->user->unitKerja->nama) ? $item->user->unitKerja->nama : 'admin',
                        'pelindung'     => isset($item->user->unitKerja->penanggung_jawab) ? $item->user->unitKerja->penanggung_jawab : 'admin',
                        'nama_acara'    => $item->nama_acara,
                        'status'        => MyHelper::detailPeminjaman()['status_persetujuan'][$item->status_peminjaman],
                    ]
                ];
            } else {
                $results = [
                    'code' => 500,
                    'item' => "Data tidak ditemukan"
                ];
            }

            echo json_encode($results);
            exit;
        }
    }

    /**
     * Updates an existing Peminjaman model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', "Data tersimpan");
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Peminjaman model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        $items = PeminjamanRuangan::deleteAll(['peminjaman_id' => $id]);


        return $this->redirect(['index']);
    }

    /**
     * Finds the Peminjaman model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Peminjaman the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Peminjaman::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    protected function findItems($id)
    {
        if (($model = PeminjamanRuangan::find()->where(['peminjaman_id' => $id])->all()) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
