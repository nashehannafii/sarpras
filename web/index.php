<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

require __DIR__ . '/../vendor/autoload.php';
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

$config = require __DIR__ . '/../config/web.php';

$config['on beforeRequest'] = function () {
    if (Yii::$app->getRequest()->getCookies()->has('lang')) {
        Yii::$app->language = Yii::$app->getRequest()->getCookies()->getValue('lang');
    }
};

(new yii\web\Application($config))->run();