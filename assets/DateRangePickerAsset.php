<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use Yii;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @author Nenad Zivkovic <nenad@freetuts.org>
 * 
 * @since 2.0
 */
class DateRangePickerAsset extends AssetBundle
{
	public $sourcePath = '@npm/daterangepicker';
    // public $baseUrl = '@themes';

    public $css = [
        'daterangepicker.css'
    ];

    public $js = [
        'daterangepicker.js'
    ];

    public $depends = [
        'yii\web\YiiAsset',

    ];

}