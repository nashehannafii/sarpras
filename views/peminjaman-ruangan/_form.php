<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\PeminjamanItem */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="peminjaman-item-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'peminjaman_id',['options' => ['tag' => false]])->textInput() ?>

    <?= $form->field($model, 'tanggal_pinjam',['options' => ['tag' => false]])->textInput() ?>

    <?= $form->field($model, 'mulai_id',['options' => ['tag' => false]])->textInput() ?>

    <?= $form->field($model, 'selesai_id',['options' => ['tag' => false]])->textInput() ?>

    <?= $form->field($model, 'ruangan_riwayat_id',['options' => ['tag' => false]])->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
