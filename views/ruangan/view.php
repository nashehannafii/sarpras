<?php

use app\helpers\MyHelper;
use kartik\editable\Editable;
use kartik\grid\GridView;
use richardfan\widget\JSRegister;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Item */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Items'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

if ($dataLabel->getCount() == 0) Yii::$app->session->setFlash('danger', 'This item has not been labeled, to add label click (+ label) on bellow');
?>

<style>
    .rounded-image {
        width: 200px;
        height: 100px;
        background-size: cover;
        background-position: center;
        border-radius: 10px;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3><b><?= Html::encode($this->title) ?></b></h3>

                <?= Html::a('<i class="fa fa-pencil"></i> Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('<i class="fa fa-trash"></i> Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>

            </div>

            <div class="panel-body ">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'attribute' => 'gambar_path',
                            'format' => 'raw',
                            'value' => function ($model) {
                                $img = '';
                                $img .= '<div class="rounded-image" style="background-image: url(' . $model->gambar_path . ');"></div>';
                                return $img;
                            }
                        ],
                        [
                            'attribute' => 'gedung.nama',
                            'label' => Yii::t('app', 'Parent')
                        ],
                        'nama',
                        'kapasitas',
                        'keterangan',
                        [
                            'attribute' => 'status_aktif',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return MyHelper::getStatusAktif()['label'][$data->status_aktif];
                            }
                        ],
                    ],
                ]) ?>

            </div>


            <div class="panel-body ">

                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    [
                        'attribute' => 'gambar_path',
                        'format' => 'raw',
                        'value' => function ($model) {
                            $img = '';
                            $img .= '<div class="rounded-image" style="background-image: url(' . $model->gambar_path . ');"></div>';
                            return $img;
                        }
                    ],
                    'no_ruangan',
                    'nama',
                    'kapasitas',
                    [
                        'attribute' => 'status_aktif',
                        'format' => 'raw',
                        'filter' => MyHelper::getStatusAktif()['filter'],
                        'value' => function ($data) {
                            return MyHelper::getStatusAktif()['label'][$data->status_aktif];
                        },
                    ],
                    // ['class' => 'yii\grid\ActionColumn']
                ];
                ?>
                <?= GridView::widget([
                    'dataProvider' => $dataLabel,
                    'filterModel' => $searchLabel,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title . " History", 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    'id' => 'ruangan-riwayat',
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]);
                ?>

            </div>


            <div class="panel-body ">

                <?php
                $statusPeminjaman = MyHelper::detailPeminjaman()['status_view'];
                $gridColumnsPeminjaman =  [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    'peminjaman.nama_acara',
                    'peminjaman.ketua_acara',
                    'ruanganRiwayat.nama',
                    'tanggal_pinjam',
                    [
                        'attribute' => 'peminjaman',
                        'format' => 'raw',
                        'value' => function ($data) use ($statusPeminjaman) {
                            return $statusPeminjaman[$data->peminjaman->status_peminjaman];
                        }
                    ],
                    // ['class' => 'yii\grid\ActionColumn']
                ];
                ?>
                <?php
                echo GridView::widget([
                    'dataProvider' => $dataPeminjaman,
                    // 'filterModel' => $searchPeminjaman,
                    'columns' => $gridColumnsPeminjaman,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => "item borrowing history", 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    'id' => 'peminjaman-riwayat',
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]);
                ?>

            </div>
        </div>

    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Booked Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>

            <form action="" method="post" id="label-form">

                <div class="modal-body">

                    <div class="col-md-12">

                        <?= Html::hiddenInput('id', '', ['id' => 'modal-label-id']) ?>
                        <?= Html::hiddenInput('ruangan_id', $model->id) ?>
                        <div class="form-group form-float">
                            <label for="peminjaman_booked_keperluan">Label Name</label>
                            <div class="form-line">
                                <!-- file input with post mode not use model -->
                                <input type="text" name="nama" id="modal-label-nama" class="form-control" placeholder="Input Item Label Name">
                            </div>
                        </div>

                        <div class="form-group form-float">
                            <label for="peminjaman_booked_keperluan">Active Status</label>
                            <?= Html::radioList('status_aktif', 1, MyHelper::getStatusAktif()['filter'], ['class' => '', 'id' => 'modal-label-status-aktif']) ?>
                        </div>

                    </div>

                    <p style="padding-left: 3%;"><small>*</small></p>
                </div>

            </form>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary waves-effect btn-save-label" data-dismiss="modal"><i class="fa fa-save"></i> Save</button>
            </div>

        </div>
    </div>
</div>

<?php JSRegister::begin() ?>
<script>
</script>
<?php JSRegister::end() ?>