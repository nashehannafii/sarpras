<?php

use app\helpers\MyHelper;
use app\models\Sarpras;
use app\models\User;
use kartik\editable\Editable;
use richardfan\widget\JSRegister;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Peminjaman */

$this->title = $model->nama_acara . ' - ' . $model->ketua_acara;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Booking'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

echo Html::hiddenInput('jam_autocancel', Sarpras::findOne(1)->jam_autocancel, ['id' => 'jam_autocancel']);
?>
<div class="block-header">
</div>



<?php
$result = '';
if ($status == true && $waktu == true) {
    $result = '<div class="alert alert-danger alert-dismissible" role="alert">
        <p id="countdown"></p>
    </div>';
} ?>

<?= $result ?>


<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3><b><?= Html::encode($this->title) ?></b></h3>
            </div>
            <div style="padding-left: 25px;">
                <?= Html::button('<i class="fa fa-trash"></i> Delete', [
                    'class' => 'btn btn-danger',
                    'id' => 'deleteButton',
                ]) ?>
                <?php
                if (Yii::$app->user->can('admin')) {
                    echo Html::a('<i class="fa fa-print"></i> Print', ['cetak-surat', 'id' => $model->id], ['class' => 'btn btn-success', 'target' => '_blank']);
                }
                if ($model->status_peminjaman != 1) Yii::$app->session->setFlash('warning', "Belum disetujui");

                ?>
            </div>

            <?php
            // Create a hidden form with the necessary action and method
            $form = ActiveForm::begin([
                'id' => 'deleteForm',
                'action' => ['delete', 'id' => $model->id],
                'method' => 'post',
            ]);
            ActiveForm::end();
            ?>

            <div class="panel-body ">

                <?php
                $user_id = Yii::$app->user->id;
                $user = User::findOne($user_id);
                if ($user->access_role != 'unitKerja') {
                    $detailView = DetailView::widget([
                        'model' => $model,
                        'attributes' => [

                            'ketua_acara',
                            'nama_acara',
                            'tema_acara',
                            'deskripsi_acara',
                            'nomor_surat',
                            [
                                'label' => 'Satuan Kerja',
                                'value' => function ($data) {
                                    return $data->user->unitKerja->nama ?? 'Admin';
                                }
                            ],
                            [
                                'attribute' => 'status_peminjaman',
                                'format' => 'raw',
                                'value' => Editable::widget([
                                    'model' => $model,
                                    'attribute' => 'status_peminjaman',
                                    'asPopover' => true,
                                    'size' => 'lg',
                                    'header' => 'Name',
                                    'placement' => 'right',
                                    'format' => Editable::FORMAT_LINK,
                                    'inputType' => Editable::INPUT_DROPDOWN_LIST,
                                    'data' => MyHelper::detailPeminjaman()['status_persetujuan'],
                                    'displayValue' => MyHelper::detailPeminjaman()['status_view'][$model->status_peminjaman],
                                ]),
                            ],
                        ],
                    ]);
                } else {

                    $detailView = DetailView::widget([
                        'model' => $model,
                        'attributes' => [

                            [
                                'attribute' => 'ketua_acara',
                                'format' => 'raw',
                                'value' => Editable::widget([
                                    'model' => $model,
                                    'attribute' => 'ketua_acara',
                                    'asPopover' => true,
                                    'size' => 'lg',
                                    'header' => 'Name',
                                    'displayValueConfig' => $model->ketua_acara,
                                    'placement' => 'right',
                                    'format' => Editable::FORMAT_LINK,
                                    'inputType' => Editable::INPUT_TEXT,
                                    'data' => $model->ketua_acara, // any list of values
                                ]),
                            ],
                            [
                                'attribute' => 'nama_acara',
                                'format' => 'raw',
                                'readonly' => Yii::$app->user->can('admin'),
                                'value' => Editable::widget([
                                    'model' => $model,
                                    'attribute' => 'nama_acara',
                                    'asPopover' => true,
                                    'size' => 'lg',
                                    'header' => 'Name',
                                    'displayValueConfig' => $model->nama_acara,
                                    'placement' => 'right',
                                    'format' => Editable::FORMAT_LINK,
                                    'inputType' => Editable::INPUT_TEXT,
                                    'data' => $model->nama_acara, // any list of values
                                ]),
                            ],
                            [
                                'attribute' => 'tema_acara',
                                'format' => 'raw',
                                'value' => Editable::widget([
                                    'model' => $model,
                                    'attribute' => 'tema_acara',
                                    'asPopover' => true,
                                    'size' => 'lg',
                                    'header' => 'Name',
                                    'displayValueConfig' => $model->tema_acara,
                                    'placement' => 'right',
                                    'format' => Editable::FORMAT_LINK,
                                    'inputType' => Editable::INPUT_TEXT,
                                    'data' => $model->tema_acara, // any list of values
                                ]),
                            ],
                            [
                                'attribute' => 'deskripsi_acara',
                                'format' => 'raw',
                                'value' => Editable::widget([
                                    'model' => $model,
                                    'attribute' => 'deskripsi_acara',
                                    'asPopover' => true,
                                    'size' => 'lg',
                                    'header' => 'Name',
                                    'displayValueConfig' => $model->deskripsi_acara,
                                    'placement' => 'right',
                                    'format' => Editable::FORMAT_LINK,
                                    'inputType' => Editable::INPUT_TEXTAREA,
                                    'data' => $model->deskripsi_acara, // any list of values
                                ]),
                            ],
                            [
                                'attribute' => 'nomor_surat',
                                'format' => 'raw',
                                'value' => Editable::widget([
                                    'model' => $model,
                                    'attribute' => 'nomor_surat',
                                    'asPopover' => true,
                                    'size' => 'lg',
                                    'header' => 'Name',
                                    'displayValueConfig' => $model->nomor_surat,
                                    'placement' => 'right',
                                    'format' => Editable::FORMAT_LINK,
                                    'inputType' => Editable::INPUT_TEXT,
                                    'data' => $model->nomor_surat, // any list of values
                                ]),
                            ],
                            // 'user.username',
                            [
                                'label' => 'Satuan Kerja',
                                'value' => function ($data) {
                                    return $data->user->nama;
                                }
                            ],
                            [
                                'attribute' => 'status_peminjaman',
                                'value' => function ($model) {
                                    return MyHelper::detailPeminjaman()['status_persetujuan'][$model->status_peminjaman];
                                },
                            ],
                        ],
                    ]);
                }

                ?>

                <?= $detailView ?>



            </div>
        </div>
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title">Detail Items</h3>
            </div>
            <div class="panel-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>No Room</th>
                            <th>Item Name</th>
                            <th style="text-align: center;">Date booking</th>
                            <th style="text-align: center;">Start time</th>
                            <th style="text-align: center;">End time</th>
                        </tr>
                    </thead>
                    <tbody>

                        <?php
                        foreach ($items as $item) : ?>
                            <tr>
                                <td><?= $item->ruanganRiwayat->no_ruangan ?></td>
                                <td><?= $item->ruanganRiwayat->nama ?></td>
                                <td style="text-align: center;"><?= MyHelper::converTanggalIndoLengkap($item->tanggal_pinjam) ?></td>
                                <td style="text-align: center;"><?= $item->mulai->jam ?></td>
                                <td style="text-align: center;"><?= $item->selesai->jam ?></td>
                            </tr>
                        <?php endforeach ?>

                    </tbody>
                </table>

            </div>
        </div>

    </div>
</div>

<?php JSRegister::begin() ?>
<script>

    function countdown() {
        var currentDate = new Date();
        currentDate.setHours(0, 0, 0, 0);
        var timeString = $("#jam_autocancel").val();
        var timeParts = timeString.split(":");
        var hours = parseInt(timeParts[0]);
        var minutes = parseInt(timeParts[1]);
        var seconds = parseInt(timeParts[2]);

        currentDate.setHours(hours, minutes, seconds, 0);

        var endTime = currentDate.getTime();

        var currentTime = $.now();
        var timeDiff = endTime - currentTime;

        var days = Math.floor(timeDiff / (1000 * 60 * 60 * 24));
        var hours = Math.floor((timeDiff % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((timeDiff % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((timeDiff % (1000 * 60)) / 1000);

        document.getElementById("countdown").innerHTML = '<i class="fa fa-warning"></i> This proposal will be automatically canceled in <b>' + hours + " hours " + minutes + " minutes " + seconds + " seconds</b>";

        if (timeDiff <= 0) {
            clearInterval(interval);
            document.getElementById("countdown").innerHTML = "Waktu telah habis!";
        }
    }

    var interval = setInterval(countdown, 1000);


    $(document).ready(function() {
        $(document).ready(function() {
            $('#deleteButton').click(function(event) {
                event.preventDefault();

                Swal.fire({
                    title: 'Caution!',
                    text: 'Are you sure you want to delete this submission ?, deleting a submission will delete all item data in this submission!',
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: '<i class=\"fa fa-trash\"></i> Delete',
                    cancelButtonText: '<i class=\"fa fa-close\"></i> Cancel',
                }).then((result) => {
                    if (result.isConfirmed) {
                        $('#deleteForm').submit();
                    }
                });
            });
        });
    });
</script>
<?php JSRegister::end() ?>