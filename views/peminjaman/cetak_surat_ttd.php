<?php 

setlocale(LC_ALL, 'id_ID', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');


?>

<table>
    <tr>
        <td colspan="2"><i>Mengetahui,</i></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td width="50%"><b><u><?= isset($data->user->unitKerja->penanggung_jawab) ? $data->user->unitKerja->penanggung_jawab :'Admin' ?></u></b><br>Kepala Fakultas/Prodi/Satuan Kerja</td>
        <td width="50%"><b><u><?= $data->ketua_acara ?></u></b><br>Ketua Panitia</td>
    </tr>
    <tr><td></td></tr>
    <tr>
        <td colspan="2"><i>Menyetujui,</i></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td></td>
    </tr>
    <tr>
        <td width="50%"><b><u><?= $sarpras->kepala_sarpras ?></u></b><br>Kabag. Sarana dan Prasarana</td>
        <td width="50%"><b><u></u></b><br>Staf Bag. Sarana dan Prasarana</td>
    </tr>
</table>