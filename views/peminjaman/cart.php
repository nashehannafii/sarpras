<?php

use app\helpers\MyHelper;
use app\models\Sarpras;
use app\models\User;
use kartik\editable\Editable;
use richardfan\widget\JSRegister;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Peminjaman */

$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Cart'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

?>

<div class="alert alert-danger alert-dismissible" role="alert">
    <p><?= Yii::t('app', 'Carts that are not processed will disappear automatically within') ?> <?= Yii::t('app', '{n, plural, one{# Hour} other{# Hours}}', ['n' => Sarpras::findOne(1)->durasi_autodelete]) ?></p>
</div>


<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3><b><?= Html::encode($this->title) ?></b></h3>
            </div>

            <div class="panel-body ">

                <input type="hidden" name="peminjaman_id" id="peminjaman_id" value="<?= $peminjaman ?>">

                <div class="col-lg-12">

                    <div class="card">
                        <div class="card-heading">
                            <h4><?= Yii::t('app', 'Your Cart') ?></h4>
                        </div>
                        <div class="card-body">
                            <table class="table table-hover" id="table-items">
                                <thead>
                                    <tr>
                                        <th>Room</th>
                                        <th>Venue</th>
                                        <th>Date</th>
                                        <th>Time</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <hr>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="pull-right">
                            <?= Html::a(Yii::t('app', 'Continue Proccess') . ' <i class="fa fa-arrow-right"></i>', ['peminjaman/create-advanced?step=2'], ['class' => 'btn btn-success', 'id' => 'saveButton']) ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
</div>

<?php JSRegister::begin() ?>
<script>
    var obj = new Object
    obj.peminjaman_id = $("#peminjaman_id").val()

    $.ajax({
        type: 'POST',
        url: '/peminjaman-ruangan/ajax-get-data-peminjaman',
        data: obj,
        success: function(data) {
            var hasils = $.parseJSON(data)
            var row = '';

            if (hasils.code == 200) {
                $.each(hasils.item, function(i, object) {
                    row += '<tr id="' + object.ruangan_riwayat_id + object.tanggal_pinjam + '">';
                    row += '<td>' + object.label + '</td>';
                    row += '<td>' + object.gedung + '</td>';
                    row += '<td>' + object.tanggal_pinjam + '</td>';
                    row += '<td>' + object.mulai_id + ' - ' + object.selesai_id + '</td>';
                    row += '<td><div class="btn btn-xs btn-danger btn-drop-item" data-label="' + object.label + '" data-item="' + object.ruangan_riwayat_id + '" data-tanggal="' + object.tanggal_pinjam + '" data-mulai="' + object.mulai_id + '" data-selesai="' + object.selesai_id + '"><i class="fa fa-trash"></i> Drop</div></td>';
                    row += '</tr>';
                });
            } else {
                row = '<tr><td colspan="5" style="text-align:center;">Not Found</td></tr>'
                $('#saveButton').attr('disabled', 'disabled');
                $('#saveButton').attr('href', 'javascript:void(0)');
            }


            $('#table-items > tbody').append(row);

        }
    })


    $(document).on("click", ".btn-drop-item", function(e) {
        e.preventDefault();

        // Get the data attributes from the button
        dataItem = $(this).data('item');
        dataTanggal = $(this).data('tanggal');
        peminjaman_id = $("#peminjaman_id").val()
        mulai_id = $(this).data('mulai');
        selesai_id = $(this).data('selesai');

        var obj = new Object
        obj.ruangan_riwayat_id = dataItem
        obj.tanggal_pinjam = dataTanggal
        obj.peminjaman_id = peminjaman_id
        obj.mulai_id = mulai_id
        obj.selesai_id = selesai_id

        $.ajax({
            type: 'POST',
            url: '/peminjaman-ruangan/ajax-drop-peminjaman-item',
            data: obj,
            success: function(data) {
                var hasil = $.parseJSON(data)

                $('#' + dataItem + dataTanggal).remove();
            }
        })


    });
</script>
<?php JSRegister::end() ?>