<?php

use app\helpers\MyHelper;
use app\models\gedung;
use app\models\Jam;
use app\models\Sarpras;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Peserta */
/* @var $form yii\widgets\ActiveForm */

$sarpras = Sarpras::findOne(1);

echo Html::hiddenInput('jam_autocancel', Sarpras::findOne(1)->jam_autocancel, ['id' => 'jam_autocancel']);
?>
<style>
    #myTable {
        border-collapse: collapse;
        width: 100%;
        border: 1px solid #ddd;
        font-size: 16px;
    }

    #myTable th,
    #myTable td {
        text-align: left;
        padding: 8px;
    }

    #myTable tr {
        border-bottom: 0.5px solid #ddd;
    }

    #myTable tr.header,
    #myTable tr:hover {
        background-color: #f1f1f1;
    }
</style>
<form id="form">
    <ul id="progressbar">
        <li id="step1"><strong>Get Items</strong></li>
        <li id="step2"><strong>For What</strong></li>
        <li class="active" id="step3"><strong>Term and Conditions</strong></li>
    </ul>


</form>


<?php $form = ActiveForm::begin(); ?>

<div class="col-lg-12 col-md-12 col-sm-12">

    <?= $form->field($model, 'id', ['options' => ['tag' => false]])->hiddenInput(['id' => 'peminjaman_id'])->label(false) ?>

    <div class="form-group form-float">
        <div class="form-line">

            <div class="card-heading">
                <h4><b><?= Yii::t('app', 'Your Cart') ?></b></h4>
            </div>

            <table class="table table-hover" id="myTable">
                <thead>
                    <tr>
                        <th style="text-align: center;">Item</th>
                        <th style="text-align: center;">Date</th>
                        <th style="text-align: center;">Time</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>

            <small>*<?= Yii::t('app', 'Make sure the suitability of the item to be borrowed!') ?></small>
        </div>

    </div>

    <div class="form-group form-float">
        <div class="form-line">
            <?= $sarpras->peraturan_umum ?>
        </div>

    </div>
    <label class="fancy-checkbox">
        <input type="checkbox" class='syarat'>
        <span>I agree with <a href="javascript:void(0)" class="syarat-ketentuan">the terms and conditions</a></span>

    </label>

    <hr>
</div>

<div class="row">
    <?= $form->field($model, 'temp', ['options' => ['tag' => false]])->hiddenInput(['value' => '-'])->label(false) ?>

    <div class="col-md-12">

        <div class="pull-left">
            <?= Html::a('<i class="fa fa-arrow-left"></i> Prev ', ['peminjaman/create-advanced', 'step' => 2], ['class' => 'btn btn-default']) ?>
        </div>
        <div class="pull-right">
            <?= Html::submitButton('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success', 'id' => 'saveButton', 'disabled' => 'disabled']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>



<?php JSRegister::begin() ?>
<script>
    var obj = new Object
    obj.peminjaman_id = $("#peminjaman_id").val()

    $.ajax({
        type: 'POST',
        url: '/peminjaman-ruangan/ajax-get-data-peminjaman',
        data: obj,
        success: function(data) {
            var hasils = $.parseJSON(data)
            var row = '';

            $('#myTable > tbody').empty();

            if (hasils.code == 200) {
                $.each(hasils.item, function(i, object) {
                    row += '<tr id="' + object.ruangan_riwayat_id + object.tanggal_pinjam + '">';
                    row += '<td>' + object.label + '</td>';
                    row += '<td style="text-align: center;">' + object.tanggal_pinjam + '</td>';
                    row += '<td style="text-align: center;">' + object.mulai_id + ' - ' + object.selesai_id + '</td>';
                    row += '<td style="text-align: center;"><div class="btn btn-xs btn-danger btn-drop-item" data-label="' + object.label + '" data-item="' + object.ruangan_riwayat_id + '" data-tanggal="' + object.tanggal_pinjam + '" data-mulai="' + object.mulai_id + '" data-selesai="' + object.selesai_id + '"><i class="fa fa-trash"></i> Drop</div></td>';
                    row += '</tr>';
                });
            }

            $('#myTable > tbody').append(row);

        }
    })

    $(document).on("change", ".syarat", function(e) {
        e.preventDefault()
        var isChecked = $(".syarat").is(":checked");

        if (isChecked == true) $('#saveButton').prop('disabled', false);
        if (isChecked == false) $('#saveButton').prop('disabled', true);

    });

    $(document).ready(function() {
        $('#saveButton').click(function(event) {
            event.preventDefault();
            var jam_autocancel = $('#jam_autocancel').val();
            var jam_autocancel = jam_autocancel.substr(0, 5);
            text = 'Please communicate with the infrastructure facilities staff for approval !!, if your Application is not processed until ' + jam_autocancel + ', it will be automatically cancelled';

            Swal.fire({
                title: 'Caution !',
                text: text,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Submit'
            }).then((result) => {
                console.log(result);
                if (result.isConfirmed) {
                    console.log('tes');
                    $('#saveButton').closest('form').submit();
                }
            });

        });
    });


    $(document).on("click", ".btn-drop-item", function(e) {
        e.preventDefault();

        // Get the data attributes from the button
        dataItem = $(this).data('item');
        dataTanggal = $(this).data('tanggal');
        peminjaman_id = $("#peminjaman_id").val()
        mulai_id = $(this).data('mulai');
        selesai_id = $(this).data('selesai');

        var obj = new Object
        obj.ruangan_riwayat_id = dataItem
        obj.tanggal_pinjam = dataTanggal
        obj.peminjaman_id = peminjaman_id
        obj.mulai_id = mulai_id
        obj.selesai_id = selesai_id

        console.log(obj);
        $.ajax({
            type: 'POST',
            url: '/peminjaman-ruangan/ajax-drop-peminjaman-item',
            data: obj,
            success: function(data) {
                var hasil = $.parseJSON(data)

                $('#' + dataItem + dataTanggal).remove();
            }
        })

    });
</script>
<?php JSRegister::end() ?>