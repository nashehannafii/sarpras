<?php

use app\helpers\MyHelper;

setlocale(LC_ALL, 'id_ID', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');


?>

<table>
    <tr>
        <td colspan="2">Lampiran</td>
    </tr>
    <tr>
        <td><i>Adapun tempat-tempat yang akan dipinjam sebagai berikut:</i></td>
    </tr>
    <tr>
        <td>
            <table border="1" style="padding: 4px;">
                <tr>
                    <td style="text-align: center;" width="15%">No</td>
                    <td width="55%">Tempat</td>
                    <td width="30%">Tanggal</td>
                </tr>
                <?php
                $listLabel = MyHelper::getLabelList();
                $no = 1;
                foreach ($items as $item) : ?>
                    <tr>
                        <td style="text-align: center;"><?= $no ?></td>
                        <td><?= $listLabel[$item->ruangan_riwayat_id] ?></td>
                        <td><?= $item->tanggal_pinjam ?></td>
                    </tr>
                <?php
                    $no++;
                endforeach; ?>
            </table>
        </td>
    </tr>
</table>