<?php

use app\helpers\MyHelper;
use app\models\gedung;
use app\models\Jam;
use app\models\Sarpras;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Peserta */
/* @var $form yii\widgets\ActiveForm */
// echo '<pre>';print_r(ArrayHelper::map(Jam::find()->all(), 'id', 'jam'));die;
?>

<style>
    * {
        box-sizing: border-box;
    }

    #myInput {
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 15px;
        padding: 6px 10px 6px 20px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
    }

    #myTable {
        border-collapse: collapse;
        width: 100%;
        border: 1px solid #ddd;
        font-size: 16px;
    }

    #myTable th,
    #myTable td {
        text-align: left;
        padding: 8px;
    }

    #myTable tr {
        border-bottom: 0.5px solid #ddd;
    }

    #myTable tr.header,
    #myTable tr:hover {
        background-color: #f1f1f1;
    }
</style>
<div class="alert alert-warning alert-dismissible" role="alert">
    <p><?= Yii::t('app', 'Carts that are not processed will disappear automatically within') ?> <?= Yii::t('app', '{n, plural, one{# Hour} other{# Hours}}', ['n' => Sarpras::findOne(1)->durasi_autodelete]) ?></p>
</div>

<form id="form">
    <ul id="progressbar">
        <li class="active" id="step1"><strong>Get Items</strong></li>
        <li id="step2"><strong>For What</strong></li>
        <li id="step3"><strong>Term and Conditions</strong></li>
    </ul>


</form>


<?php $form = ActiveForm::begin(); ?>

<div class="col-lg-12">

    <div class="row">

        <div class="col-md-4">

            <input type="text" id="myInput" placeholder="Search for items..">

        </div>
        <div class="col-md-4">

            <?= Select2::widget([
                'name' => 'gedung-filter',
                'data' => ArrayHelper::map(Gedung::find()->where(['status_aktif' => 1])->all(), 'id', 'nama'),
                // 'value' => ,
                'options' => [
                    'class' => 'form-control',
                    'prompt' => Yii::t('app', 'Select Venue'),
                    'id' => 'gedung-filter'
                ],
                // 'pluginOptions' => [
                //     'allowClear' => true,
                // ],
            ]); ?>

        </div>
        <div class="col-md-4">

            <?= DatePicker::widget([
                'name' => 'tanggal-filter',
                'id' => 'tanggal-filter',
                'options' => ['placeholder' => 'Select Date'],
                'convertFormat' => true,
                'value' => !empty($_GET['tanggal-mulai']) ? $_GET['tanggal-mulai'] : date('Y-m-d'),
                'pluginOptions' => [
                    'todayHighlight' => true,
                    'format' => 'php:Y-m-d',
                    'autoclose' => true,
                    'allowClear' => false,
                ]
            ]) ?>

        </div>
        <br><br>

        <div class="col-md-12">


            <div class="card">
                <div class="card-heading">
                    <h4><b>
                            <?= Yii::t('app', "Available Rooms") ?>
                        </b></h4>
                </div>
                <div class="card-body">
                    <table class="table table-hover" id="myTable"></table>
                </div>
            </div>

        </div>

    </div>

    <?= $form->field($model, 'user_id', ['options' => ['tag' => false]])->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'id', ['options' => ['tag' => false]])->hiddenInput(['id' => 'peminjaman_id'])->label(false) ?>



    <hr>
</div>

<!-- <div class="col-lg-6 col-md-12 col-sm-12">

    <div class="card hidden-item">
        <div class="card-heading">
            <h4><?= Yii::t('app', 'Your Cart') ?></h4>
        </div>
        <div class="card-body">
            <table class="table table-hover" id="table-items">
                <thead>
                    <tr>
                        <th>Room</th>
                        <th>Venue</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

    <hr>
</div> -->

<div class="row">
    <div class="col-md-12">
        <div class="pull-right">
            <?= Html::submitButton('Next <i class="fa fa-arrow-right"></i>', ['class' => 'btn btn-success', 'id' => 'saveButton']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>

<!-- Modal -->
<div class="modal fade" id="jamModal" tabindex="-1" role="dialog" aria-labelledby="jamModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="jamModalLabel"><b><?= Yii::t('app', 'Time') ?></b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>

            <div class="modal-body">
                <?= Html::hiddenInput('ruangan_riwayat_id_modal', $model->id, ['id' => 'ruangan_riwayat_id_modal']) ?>
                <?= Html::hiddenInput('peminjaman_id_modal', $model->id, ['id' => 'peminjaman_id_modal']) ?>
                <?= Html::hiddenInput('tanggal_pinjam_modal', $model->id, ['id' => 'tanggal_pinjam_modal']) ?>
                <?= Html::hiddenInput('ruangan_id_modal', $model->id, ['id' => 'ruangan_id_modal']) ?>

                <div class="col-md-6">
                    <div class="form-group form-float">
                        <label for="peminjaman_booked_keperluan">Start Time</label>
                        <div class="form-line">
                            <?= Select2::widget([
                                'name' => 'mulai_id_modal',
                                'data' => ArrayHelper::map(Jam::find()->orderBy(['urutan' => SORT_ASC])->all(), 'id', 'jam'),
                                'options' => [
                                    'class' => 'form-control',
                                    'prompt' => 'Select Start Time',
                                    'id' => 'mulai_id_modal'
                                ],
                                // 'pluginOptions' => [
                                //     'allowClear' => true,
                                // ],
                            ]); ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group form-float">
                        <label for="peminjaman_booked_keperluan">End Time</label>
                        <div class="form-line">
                            <?= DepDrop::widget([
                                'name' => 'selesai_id_modal',
                                'type' => DepDrop::TYPE_SELECT2,
                                'options' => ['id' => 'selesai_id_modal'],
                                // 'select2Options' => ['pluginOptions' => ['allowClear' => true]],
                                'pluginOptions' => [
                                    'depends' => ['mulai_id_modal'],
                                    'initialize' => true,
                                    'placeholder' => 'Select End Time',
                                    'url' => Url::to(['/jam/subselesai'])
                                ]
                            ]); ?>
                        </div>

                    </div>
                </div>
                <p style="padding-left: 3%;"><small>*</small></p>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-primary waves-effect btn-save-jam" data-dismiss="modal" disabled><i class="fa fa-save"></i> Save</button>
            </div>
        </div>

    </div>
</div>

<?php JSRegister::begin() ?>
<script>
    $('#table-items > tbody').empty();
    getItems("<?= date('Y-m-d') ?>", 1)

    $('.hidden-items').hide()

    $(document).ready(function() {
        $('#mulai_id_modal, #selesai_id_modal').on('change', function() {
            var mulaiVal = $('#mulai_id_modal').val();
            var selesaiVal = $('#selesai_id_modal').val();

            if (mulaiVal && selesaiVal) {
                $('.btn-save-jam').removeAttr('disabled');
            } else {
                $('.btn-save-jam').attr('disabled', 'disabled');
            }
        });
    });

    var obj = new Object
    obj.peminjaman_id = $("#peminjaman_id").val()

    $.ajax({
        type: 'POST',
        url: '/peminjaman-ruangan/ajax-get-data-peminjaman',
        data: obj,
        success: function(data) {
            var hasils = $.parseJSON(data)
            var row = '';

            if (hasils.code == 200) {
                $.each(hasils.item, function(i, object) {
                    row += '<tr id="' + object.ruangan_riwayat_id + object.tanggal_pinjam + '">';
                    row += '<td>' + object.label + '</td>';
                    row += '<td>' + object.gedung + '</td>';
                    row += '<td>' + object.tanggal_pinjam + '</td>';
                    row += '<td>' + object.mulai_id + ' - ' + object.selesai_id + '</td>';
                    row += '<td><div class="btn btn-xs btn-danger btn-drop-item" data-label="' + object.label + '" data-item="' + object.ruangan_riwayat_id + '" data-tanggal="' + object.tanggal_pinjam + '" data-mulai="' + object.mulai_id + '" data-selesai="' + object.selesai_id + '"><i class="fa fa-trash"></i> Drop</div></td>';
                    row += '</tr>';
                });
            }

            $('#table-items > tbody').append(row);

        }
    })

    $(document).on("change", "#tanggal-filter, #gedung-filter", function(e) {
        e.preventDefault()

        gedung = $("[name='gedung-filter']").val()
        tanggal = $("[name='tanggal-filter']").val()

        getItems(tanggal, gedung)
    });

    $(document).on("click", ".btn-add-item", function(e) {
        e.preventDefault()

        dataItem = $(this).data('item');
        dataLabel = $(this).data('label');
        dataTanggal = $(this).data('tanggal');
        peminjaman_id = $("#peminjaman_id").val()

        $("#jamModal").modal("show");
        $("#ruangan_id_modal").val(dataItem)
        $("#ruangan_riwayat_id_modal").val(dataLabel)
        $("#tanggal_pinjam_modal").val(dataTanggal)
        $("#peminjaman_id_modal").val(peminjaman_id)

    });

    $(document).on("click", ".btn-save-jam", function(e) {
        e.preventDefault()

        var jams = getJams()

        ruangan_riwayat_id = $("#ruangan_riwayat_id_modal").val()
        ruangan_id = $("#ruangan_id_modal").val()
        tanggal_pinjam = $("#tanggal_pinjam_modal").val()
        peminjaman_id = $("#peminjaman_id_modal").val()
        mulai_id = $("#mulai_id_modal").val()
        selesai_id = $("#selesai_id_modal").val()

        var obj = new Object
        var isIdExists = false

        obj.ruangan_riwayat_id = ruangan_id
        obj.tanggal_pinjam = tanggal_pinjam
        obj.peminjaman_id = peminjaman_id
        obj.mulai_id = mulai_id
        obj.selesai_id = selesai_id

        ruangan_id = ruangan_id + tanggal_pinjam

        $.ajax({
            type: 'POST',
            url: '/peminjaman-ruangan/check-by-peminjaman-id',
            data: {
                data: peminjaman_id
            },
            success: function(data) {
                var hasils = $.parseJSON(data)

                $.each(hasils.items, function(i, obj) {
                    if (obj.id == ruangan_id) {
                        isIdExists = true;
                        return isIdExists;
                    }
                })

                if (isIdExists == false) {

                    $.ajax({
                        type: 'POST',
                        url: '/peminjaman-ruangan/ajax-add-peminjaman-item',
                        data: obj,
                        success: function(data) {
                            var hasil = $.parseJSON(data)
                        }
                    })

                }
            }
        })

    });

    $(document).on("click", ".btn-drop-item", function(e) {
        e.preventDefault();

        // Get the data attributes from the button
        dataItem = $(this).data('item');
        dataTanggal = $(this).data('tanggal');
        peminjaman_id = $("#peminjaman_id").val()
        mulai_id = $(this).data('mulai');
        selesai_id = $(this).data('selesai');

        var obj = new Object
        obj.ruangan_riwayat_id = dataItem
        obj.tanggal_pinjam = dataTanggal
        obj.peminjaman_id = peminjaman_id
        obj.mulai_id = mulai_id
        obj.selesai_id = selesai_id

        $.ajax({
            type: 'POST',
            url: '/peminjaman-ruangan/ajax-drop-peminjaman-item',
            data: obj,
            success: function(data) {
                var hasil = $.parseJSON(data)

                $('#' + dataItem + dataTanggal).remove();
            }
        })


    });


    function getJams() {
        var jams = [];

        $.ajax({
            type: 'POST',
            url: '/jam/ajax-get-jams',
            data: obj,
            async: false,
            success: function(data) {
                var hasil = $.parseJSON(data);

                jams.push("00.00");

                $.each(hasil.item, function(index, item) {
                    jams.push(item.jam);
                });
            }
        });

        return jams;
    }


    function getItems(tanggal, gedung) {

        var obj = new Object;
        obj.tanggal = tanggal;
        obj.gedung = gedung;

        $.ajax({
            type: 'POST',
            url: '/ruangan/ajax-get-ruangan-kosong',
            data: obj,
            success: function(data) {
                var hasils = $.parseJSON(data)
                var row = '';

                $('#myTable').empty();

                row += '<thead><tr><th>Room</th><th>Venue</th><th>Date</th><th>Action</th></tr></thead>'
                row += '<tbody>';

                if (hasils.item.length > 0) {

                    $.each(hasils.item, function(i, objects) {

                        row += '<tr>';
                        row += '<td>' + objects.nama_label + '</td>';
                        row += '<td>' + objects.nama_gedung + '</td>';
                        row += '<td>' + hasils.tanggal + '</td>';
                        // row += '<td>' + objects.mulai_id + ' - ' + objects.selesai_id + '</td>';
                        row += '<td><div class="btn btn-xs btn-primary btn-add-item" data-label="' + objects.nama_label + '" data-item=' + objects.id + ' data-tanggal=' + tanggal + '><i class="fa fa-plus"></i> Add to cart</div></td>';
                        row += '</tr>';

                    });

                } else {

                    row += '<tr><td colspan="3" style="text-align: center;">Not available</td></tr>'

                }

                row += '</tbody>';

                $('#myTable').append(row);

            }
        })

    }

    $(document).ready(function() {
        $("#myInput").keyup(function() {
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("myInput");
            filter = input.value.toUpperCase();
            table = document.getElementById("myTable");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[0];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        });
    });
</script>
<?php JSRegister::end() ?>