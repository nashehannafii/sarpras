<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Peminjaman */

$this->title = Yii::t('app', 'Create booking Proposal Advanced');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Booking'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h3><?= Html::encode($this->title) ?></h3>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="panel-body ">

                <?php
                switch ($step) {
                    case 1:
                        echo $this->render('_form_advanced', [
                            'model' => $model,
                        ]);
                        break;
                    case 2:
                        echo $this->render('_form_advanced2', [
                            'model' => $model,
                        ]);
                        break;
                    case 3:
                        echo $this->render('_form_advanced3', [
                            'model' => $model,
                        ]);
                        break;
                }
                ?>

            </div>
        </div>
    </div>
</div>