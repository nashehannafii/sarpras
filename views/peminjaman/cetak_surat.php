<?php

// date_default_timezone_set("Asia/Jakarta");

use app\helpers\MyHelper;

setlocale(LC_ALL, 'id_ID', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');
$datetimeString = $data->tanggal_mulai;

$datetime = new DateTime($datetimeString);
$tanggal = $datetime->format('Y-m-d');

if ($jenis == 2) {

    $waktuPinjam = [];
    foreach ($items as $item) {
        $waktuPinjam[] = $item->tanggal_pinjam;
    }

    $waktuPinjam = MyHelper::converTanggalIndoLengkap(min($waktuPinjam)) . ' - ' . MyHelper::converTanggalIndoLengkap(max($waktuPinjam));
} else {
    $waktuPinjam = $items->mulai->jam . ' - ' . $items->selesai->jam;
}

?>

<p>
<table>
    <tr>
        <td width="2%"></td>
        <td width="95%">
            <table border="0">
                <tr>
                    <td width="60%">
                        <table>
                            <tr>
                                <td width="19%">Nomor</td>
                                <td width="2%">:</td>
                                <td width="79 %"><?= $data->nomor_surat != '' && $data->nomor_surat != null ? $data->nomor_surat : "" ?></td>
                            </tr>
                            <tr>
                                <td>Lampiran</td>
                                <td>:</td>
                                <td><?= $data->jenis_peminjaman == 2 ? "1" : "-" ?></td>
                            </tr>
                            <tr>
                                <td>Perihal</td>
                                <td>:</td>
                                <td><b>Peminjaman Fasilitas</b></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Kepada yang terhormat, </td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td><b>Kabag. Sarana dan Prasarana UNIDA Gontor</b></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>Di -</td>
                            </tr>
                            <tr>
                                <td></td>
                                <td></td>
                                <td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Tempat</td>
                            </tr>
                        </table>
                    </td>
                    <td width="40%">
                        <table>
                            <tr>
                                <td width="38%"></td>
                                <td width="100%">Ponorogo, <?= MyHelper::convertTanggalIndo($tanggal) ?></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <table border="0">
                            <tr>
                                <td width="13%"></td>
                                <td width="89%"><i>Bismillahirrahmanirrahim</i></td>
                                <td width="11%"></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><i>Assalamu’alaikum Warahmatullahi Wabarakatuh</i></td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <?php if ($jenis == 2) { ?>
                                <tr>
                                    <td></td>
                                    <td>&nbsp; &nbsp; Sehubung dengan akan diselenggarakannya acara <b><?= $data->nama_acara ?></b>, kami dari Panitia <?= $data->nama_acara ?> meminta izin untuk menggunakan fasilitas kampus berupa sebagai terlampir, yang insya Allah akan diselenggarakan pada:</td>
                                </tr>
                            <?php } else { ?>
                                <tr>
                                    <td></td>
                                    <td>&nbsp; &nbsp; Sehubung dengan akan diselenggarakannya <b><?= $data->nama_acara ?></b> dengan tema <b>"<?= $data->tema_acara ?>"</b>, yang insya Allah akan dilaksanakan pada:</td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td></td>
                                <td>
                                    <table border="0">
                                        <?php if ($jenis == 2) { ?>
                                            <tr>
                                                <td width="5%"></td>
                                                <td width="15%">Hari/Tanggal</td>
                                                <td width="80%">: <?= $waktuPinjam ?></td>
                                            </tr>
                                            <tr>
                                                <td width="5%"></td>
                                                <td width="15%">Pukul</td>
                                                <td>: 07.00 - 22.00 WIB</td>
                                            </tr>
                                        <?php } else { ?>
                                            <tr>
                                                <td width="5%"></td>
                                                <td width="15%">Hari/Tanggal</td>
                                                <td>: <?= MyHelper::converTanggalIndoLengkap($items->tanggal_pinjam) ?></td>
                                            </tr>
                                            <tr>
                                                <td width="5%"></td>
                                                <td width="15%">Waktu</td>
                                                <td>: <?= $waktuPinjam ?> WIB</td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </td>
                            </tr>
                            <?php if ($jenis == 2) { ?>
                                <tr>
                                    <td></td>
                                    <td><i>&nbsp; &nbsp; Adapun tempat-tempat terlampir.</i></td>
                                </tr>
                            <?php } else { ?>
                                <tr>
                                    <td></td>
                                    <td>Maka, kami dari panitia bermaksud meminta izin untuk menggunakan <b><?= MyHelper::getLabelList()[$items->ruangan_riwayat_id] ?>.</b></td>
                                </tr>
                            <?php } ?>
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td>&nbsp; &nbsp; Demikian surat ini kami buat dan kami sampaikan, semoga menjadi maklum adanya. Atas perhatian dan izinnya, kami ucapkan terima kasih.</td>
                            </tr>
                            <tr>
                                <td></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td><i>Wassalamu’alaikum Warahmatullahi Wabarakatuh</i></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
        <!-- <td width="3%"></td> -->
    </tr>
</table>
</p>