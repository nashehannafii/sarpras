<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Peminjaman */

$this->title = Yii::t('app', 'Create booking Proposal');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Booking'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<h3><?= Html::encode($this->title) ?></h3>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="panel-body ">
                <?= $this->render('_form', [
                    'model' => $model,
                    'data' => $data,
                    'sarpras' => $sarpras,
                ]) ?>

            </div>
        </div>
    </div>
</div>