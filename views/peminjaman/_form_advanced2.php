<?php

use app\helpers\MyHelper;
use app\models\gedung;
use app\models\Jam;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Peserta */
/* @var $form yii\widgets\ActiveForm */
?>

<form id="form">
    <ul id="progressbar">
        <li id="step1"><strong>Get Items</strong></li>
        <li class="active" id="step2"><strong>For What</strong></li>
        <li id="step3"><strong>Term and Conditions</strong></li>
    </ul>


</form>


<?php $form = ActiveForm::begin(); ?>

<div class="col-lg-12 col-md-12 col-sm-12">

    <?= $form->field($model, 'nama_acara', ['options' => ['tag' => false]])->textInput(['pleaceholder' => 'Masukkan nama acara/kegiatan', 'class' => 'form-control', 'maxlength' => true, 'placeholder' => Yii::t('app', 'Enter Event Name')]) ?>

    <?= $form->field($model, 'user_id', ['options' => ['tag' => false]])->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false) ?>

    <?= $form->field($model, 'tema_acara', ['options' => ['tag' => false]])->textInput(['pleaceholder' => 'Masukkan tema acara/kegiatan', 'class' => 'form-control', 'maxlength' => true, 'placeholder' => Yii::t('app', 'Enter Even Theme')]) ?>

    <?= $form->field($model, 'ketua_acara', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => Yii::t('app', 'Enter Even Chairman')]) ?>

    <?= $form->field($model, 'nomor_surat', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => Yii::t('app', 'Enter Reference Letter Number')]) ?>


    <?= $form->field($model, 'deskripsi_acara', ['options' => ['tag' => false]])->textArea(['class' => 'form-control', 'maxlength' => true, 'placeholder' => Yii::t('app', 'Enter Event Description')]) ?>

    <?= $form->field($model, 'status_peminjaman', ['options' => ['tag' => false]])->hiddenInput(['value' => 0])->label(false) ?>

    <?= $form->field($model, 'jenis_peminjaman', ['options' => ['tag' => false]])->hiddenInput(['value' => 2])->label(false) ?>

    <?= $form->field($model, 'is_sent', ['options' => ['tag' => false]])->hiddenInput(['value' => null])->label(false) ?>

    <hr>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="pull-left">
            <?= Html::a('<i class="fa fa-arrow-left"></i> Prev ', ['peminjaman/create-advanced', 'step' => 1], ['class' => 'btn btn-default']) ?>
        </div>
        <div class="pull-right">
            <?= Html::submitButton('Next <i class="fa fa-arrow-right"></i>', ['class' => 'btn btn-success', 'id' => 'saveButton']) ?>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>