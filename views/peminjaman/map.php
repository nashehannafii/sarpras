<?php

use app\assets\HighchartAsset;
use app\helpers\MyHelper;
use app\models\gedung;
use app\models\Item;
use app\models\Jam;
use app\models\Peminjaman;
use app\models\PeminjamanItem;
use app\models\PeminjamanRuangan;
use app\models\Ruangan;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use dosamigos\chartjs\ChartJs;

HighchartAsset::register($this);

/* @var $this yii\web\View */
setlocale(LC_ALL, 'id_ID', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

$gedung = isset($_GET['gedung']) ? $_GET['gedung'] : $gedung;
$gedung = gedung::findOne($gedung);

$tanggal = isset($_GET['tanggal']) ? $_GET['tanggal'] : $tanggal;
$tanggal_modify = new DateTime($tanggal);

$tanggal_sebelum = $tanggal_modify->modify('-1 day');
$tanggal_sebelum = $tanggal_sebelum->format('Y-m-d');

$tanggal_setelah = $tanggal_modify->modify('+2 day');
$tanggal_setelah = $tanggal_setelah->format('Y-m-d');

// $ruangans = Ruangan::find()->select('nama')->all();
$gedungs = gedung::find()->select('nama')->column();

$this->title = Yii::t('app', 'Booking Map');
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .tooltip-inner {
        max-width: 300px;
        /* Atur lebar maksimum tooltip */
        white-space: nowrap;
        /* Hindari pemisahan kata */
        overflow: hidden;
        text-align: left;
        /* Sembunyikan konten yang meluap */
        text-overflow: ellipsis;
        /* Tampilkan elipsis (...) jika konten terlalu panjang */
    }

    #desc-table td {
        padding: 5px;
        /* Ubah nilai ini sesuai dengan jarak yang diinginkan */
    }

    #desc-table td:first-child {
        padding-right: 5px;
        /* Ubah nilai ini sesuai dengan jarak yang diinginkan */
    }

    #desc-table td:last-child {
        padding-left: 5px;
        /* Ubah nilai ini sesuai dengan jarak yang diinginkan */
    }
</style>


<div class="row">
    <div class="col-lg-12">

        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title"><?= $this->title ?></h3>
            </div>
            <div class="panel-body">
                <table border="0">
                    <tr>
                        <td width="30%">
                            <div class="col-md-12">

                                <?= DatePicker::widget([
                                    'name' => 'tanggal',
                                    'id' => 'tanggal-filter',
                                    'value' => $tanggal,
                                    'options' => ['placeholder' => 'Pilih Tanggal'],
                                    'convertFormat' => true,
                                    'pluginOptions' => [
                                        'todayHighlight' => true,
                                        'format' => 'php:Y-m-d',
                                        'autoclose' => true,
                                    ]
                                ]) ?>

                            </div>
                        </td>
                        <td width="30%">
                            <div class="col-md-12">

                                <?= $list_approver = Select2::widget([
                                    'name' => 'gedung',
                                    'id' => 'gedung-filter',
                                    'data' => ArrayHelper::map(gedung::find()->all(), 'id', 'nama'),
                                    'value' => $gedung,
                                    'options' => [
                                        'class' => 'form-control',
                                        'prompt' => 'Select Parent',
                                        // 'multiple' => true,
                                    ],
                                    // 'pluginOptions' => [
                                    //     'allowClear' => true
                                    // ]
                                ]); ?>

                            </div>
                        </td>
                        <td width="30%">
                            <div class="col-md-12">

                                <?= Html::a('<i class="fa fa-plus"></i> '. Yii::t('app', ' Book Now!'), ['/peminjaman/create-advanced'], ['class' => 'btn btn-md btn-primary']) ?>

                            </div>
                        </td>
                    </tr>
                </table>

                <br>
                <div class="row">
                    <ul class="pager">
                        <li>
                            <h><b><?= MyHelper::convertTanggalEnLengkap($tanggal) ?> | <?= Html::a($gedung->nama, ['gedung/detail', 'id' => $gedung->id], ['class' => '']) ?></b></h>
                        </li>
                    </ul>
                    <ul class="pager">
                        <li class="previous">
                            <?= Html::a('&larr; ' . Yii::t('app', 'Yesterday'), ['peminjaman/map', 'tanggal' => $tanggal_sebelum, 'gedung' => $gedung->id], ['class' => '']); ?>
                        </li>
                        <li class="next">
                            <?= Html::a(Yii::t('app', 'Tomorrow') . ' &rarr;', ['peminjaman/map', 'tanggal' => $tanggal_setelah, 'gedung' => $gedung->id], ['class' => '']); ?>
                        </li>
                    </ul>
                </div>

                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">Waktu</th>
                                    <?php
                                    $ruangans = Ruangan::find()->where(['gedung_id' => [$gedung->id], 'status_aktif' => 1,])->all();

                                    foreach ($ruangans as $ruangan) :
                                        $ruangan = Html::a($ruangan->nama . ' | ' . $ruangan->kapasitas, ['ruangan/detail', 'id' => $ruangan->id], ['']);
                                    ?>
                                        <th style="text-align: center;">
                                            <?= $ruangan ?>
                                        </th>
                                    <?php endforeach; ?>
                                    <th style="text-align: center;">Waktu</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach (Jam::find()->orderBy(['urutan' => SORT_ASC])->all() as $jam) : ?>
                                    <tr style="text-align: center;">

                                        <th width="6%" style="text-align: center;"><?= $jam->jam ?></th>
                                        <?php
                                        $jumlah = Ruangan::find()->where([
                                            'status_aktif' => 1,
                                        ])->count();
                                        $hasil = 88 / ($jumlah != null ? $jumlah : 1);

                                        $ruangans = Ruangan::find()->where([
                                            'gedung_id' => [
                                                $gedung->id
                                            ],
                                            'status_aktif' => 1,
                                        ])->all();
                                        foreach ($ruangans as $ruangan) :
                                        ?>

                                            <?php
                                            // $peminjamans = Peminjaman::findByItem($ruangan->id, $tanggal, [0, 1])->all();

                                            $peminjamanItems = PeminjamanRuangan::findPeminjaman($ruangan->id, $tanggal, [0, 1])->all();

                                            $style = '';
                                            $avalilable = '';
                                            $avalilable .= '<form action="' . Yii::$app->urlManager->createUrl(['peminjaman/create']) . '" method="post">';
                                            $avalilable .= '<input type="hidden" name="' . Yii::$app->request->csrfParam . '" value="' . Yii::$app->request->csrfToken . '">';
                                            $avalilable .= '<input type="hidden" name="jam" value="' . $jam->id . '">';
                                            $avalilable .= '<input type="hidden" name="ruangan_id" value="' . $ruangan->id . '">';
                                            $avalilable .= '<input type="hidden" name="tanggal" value="' . $tanggal . '">';
                                            $avalilable .= '<button type="submit" class="btn btn-success btn-xs">';
                                            $avalilable .= '<i class="fa fa-home"></i></button></form>';

                                            if (isset($peminjamanItems)) {
                                                $result = $avalilable;
                                                $i = 0;
                                                foreach ($peminjamanItems as $ruangan) {

                                                    $pj = $ruangan->peminjaman->user->unitKerja->singkatan ?? '';

                                                    $tb = '';
                                                    $tb .= '<table><tr><th colspan="4" style="text-align:center;"><h4><b>Detail</b></h4></th></tr>';
                                                    $tb .= '<tr><td><b>Nama Acara </b></td><td>:</td><td> ' . $ruangan->peminjaman->nama_acara . '</td></tr>';
                                                    $tb .= '<tr><td><b>Unit </b></td><td>:</td><td> ' . $pj . '</td></tr>';
                                                    $tb .= '<tr><td><b>Ketua Acara </b></td><td>:</td><td> ' . $ruangan->peminjaman->ketua_acara . '</td></tr>';
                                                    $tb .= '<tr><td><b>Status peminjaman </b></td><td>:</td><td> ' . MyHelper::detailPeminjaman()['status_persetujuan'][$ruangan->peminjaman->status_peminjaman] . '</td></tr>';
                                                    $tb .= '</table>';

                                                    if ($i == 1) {
                                                        # code...
                                                    }                                                    

                                                    if ($ruangan->mulai == $jam || $ruangan->selesai->urutan > $jam->urutan && $ruangan->mulai->urutan <= $jam->urutan) {

                                                        if ($ruangan->peminjaman->status_peminjaman == 0) {

                                                            $result = Html::button('<i class="fa fa-hourglass-half"></i>', [
                                                                'class' => 'btn btn-xs btn-warning btn-booked',
                                                                'data-pinjam' => $ruangan->peminjaman->id,
                                                                'data' => [
                                                                    'toggle' => 'tooltip',
                                                                    'placement' => 'top',
                                                                    'html' => 'true',
                                                                    'title' => $tb,
                                                                ],
                                                            ]);
                                                        } elseif ($ruangan->peminjaman->status_peminjaman == 1) {

                                                            $result = Html::button('<i class="fa fa-check-square-o"></i>', [
                                                                'class' => 'btn btn-xs btn-primary btn-booked',
                                                                'data-pinjam' => $ruangan->peminjaman->id,
                                                                'data' => [
                                                                    'toggle' => 'tooltip',
                                                                    'placement' => 'top',
                                                                    'html' => 'true',
                                                                    'title' => $tb,
                                                                ],
                                                            ]);
                                                        }

                                                    }
                                                    $i++;

                                                }
                                            } else {
                                                $result = $avalilable;
                                            } 
                                            ?>

                                            <td width="<?= $hasil ?>%"><?= $result ?></td>

                                        <?php endforeach; ?>

                                        <th width="6%" style="text-align: center;"><?= $jam->jam ?></th>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tbody>
                                <tr style="text-align: center;">
                                    <th style="text-align: center;">Waktu</th>
                                    <?php
                                    $ruangans = Ruangan::find()->where([
                                        'gedung_id' => [
                                            $gedung->id
                                        ],
                                        'status_aktif' => 1,
                                    ])->all();
                                    foreach ($ruangans as $ruangan) :
                                        $ruangan = Html::a($ruangan->nama . ' | ' . $ruangan->kapasitas, ['ruangan/detail', 'id' => $ruangan->id], ['']);
                                    ?>
                                        <th style="text-align: center;">
                                            <?= $ruangan ?>
                                        </th>
                                    <?php endforeach; ?>
                                    <th style="text-align: center;">Waktu</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="card pull-right">
                    <div class="card-body">

                        <table id="desc-table">
                            <tr>
                                <td><button class="btn btn-xs btn-success"><i class="fa fa-home"></i></button></td>
                                <td><b>Avalilable</b></td>
                            </tr>
                            <tr>
                                <td><button class="btn btn-xs btn-warning"><i class="fa fa-hourglass-half"></i></button></td>
                                <td><b>Waiting</b></td>
                            </tr>
                            <tr>
                                <td><button class="btn btn-xs btn-primary"><i class="fa fa-check-square-o"></i></button></td>
                                <td><b>Approved</b></td>
                            </tr>
                        </table>

                    </div>

                </div>

            </div>

        </div>
    </div>


    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">

                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Booked Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>

                </div>

                <div class="modal-body">

                    <div class="col-md-12">
                        <div class="form-group form-float">
                            <label for="peminjaman_booked_keperluan">Ketua</label>
                            <div class="form-line">
                                <!-- file input with post mode not use model -->
                                <input type="ketua" name="ketua" id="peminjaman_booked_ketua" class="form-control" disabled>
                            </div>

                        </div>

                        <div class="form-group form-float">
                            <label for="peminjaman_booked_keperluan">Unit</label>
                            <div class="form-line">
                                <!-- file input with post mode not use model -->
                                <input type="unit" name="unit" id="peminjaman_booked_unit" class="form-control" disabled>
                            </div>

                        </div>

                        <div class="form-group form-float">
                            <label for="peminjaman_booked_keperluan">Pelindung</label>
                            <div class="form-line">
                                <!-- file input with post mode not use model -->
                                <input type="pelindung" name="pelindung" id="peminjaman_booked_pelindung" class="form-control" disabled>
                            </div>

                        </div>

                        <div class="form-group form-float">
                            <label for="peminjaman_booked_keperluan">Nama Acara/Kegiatan</label>
                            <div class="form-line">
                                <!-- file input with post mode not use model -->
                                <input type="peminjaman" name="peminjaman" id="peminjaman_booked_nama_acara" class="form-control" disabled>
                            </div>

                        </div>

                        <div class="form-group form-float">
                            <label for="peminjaman_booked_keperluan">Status</label>
                            <div class="form-line">
                                <!-- file input with post mode not use model -->
                                <input type="peminjaman" name="status" id="peminjaman_booked_status" class="form-control" disabled>
                            </div>

                        </div>
                    </div>

                    <p style="padding-left: 3%;"><small>*detail booked data</small></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><i class="fa fa-window-close"></i> Close</button>
                </div>

            </div>
        </div>
    </div>

    <?php JSRegister::begin() ?>
    <script>
        $(document).on("change", "#tanggal-filter, #gedung-filter", function(e) {
            e.preventDefault()
            tanggal = $('#tanggal-filter').val()
            gedung = $('#gedung-filter').val()

            url = '/peminjaman/map?'

            if (tanggal != "") url += 'tanggal=' + tanggal
            if (gedung != "") url += '&gedung=' + gedung

            window.location.href = url
        });

        $(document).ready(function() {
            $('[data-toggle="tooltip"]').tooltip();
        });

        $(document).on("click", ".btn-booked", function(e) {
            e.preventDefault()

            var pinjam = $(this).data("pinjam")


            $.ajax({
                type: 'POST',
                url: '/peminjaman/ajax-get-data',
                data: {
                    pinjam: pinjam
                },
                // async: true,
                success: function(data) {
                    var hasil = $.parseJSON(data)
                    if (hasil.code == 200) {

                        $('#peminjaman_booked_ketua').val(hasil.item.ketua);
                        $('#peminjaman_booked_unit').val(hasil.item.unit);
                        $('#peminjaman_booked_pelindung').val(hasil.item.pelindung);
                        $('#peminjaman_booked_nama_acara').val(hasil.item.nama_acara);
                        $('#peminjaman_booked_status').val(hasil.item.status);

                        $("#exampleModal").modal("show");

                    } else {

                        Swal.fire({
                            title: 'Oops!',
                            icon: 'error',
                            text: hasil.message
                        }).then((result) => {
                            if (result.value) {
                                location.reload();
                            }
                        });

                    }
                }
            })

        });
    </script>
    <?php JSRegister::end() ?>