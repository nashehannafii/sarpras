<?php

use app\helpers\MyHelper;
use app\models\Jam;
use app\models\Sarpras;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Peminjaman */
/* @var $form yii\widgets\ActiveForm */

echo Html::hiddenInput('jam_autocancel', Sarpras::findOne(1)->jam_autocancel, ['id' => 'jam_autocancel']);
?>

<div class="peminjaman-form">



    <div class="form-group">
        <?= Html::a('<i class="fa fa-plus"></i> ' . Yii::t('app', "Multi"), ['/peminjaman/create-advanced'], ['class' => 'btn btn-md btn-primary']) ?>

    </div>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'tanggal_mulai', ['options' => ['tag' => false]])->textInput(['value' => MyHelper::getLabelList()[$data['item']->id] ?? "Belum diberi label", 'disabled' => 'disabled'])->label('Item') ?>

    <?= $form->field($model, 'tanggal_pinjam', ['options' => ['tag' => false]])->textInput(['value' => $data['dataPost']['tanggal'], 'disabled' => 'disabled']) ?>

    <?= $form->field($model, 'tanggal_pinjam', ['options' => ['tag' => false]])->hiddenInput(['value' => $data['dataPost']['tanggal']])->label(false) ?>

    <?= $form->field($model, 'mulai', ['options' => ['tag' => false]])->dropDownList(ArrayHelper::map(Jam::find()->all(), 'id', 'jam'), ['prompt' => '- pilih parent -', 'value' => $data['dataPost']['jam'], 'disabled' => 'disabled'])->label('Jam Mulai') ?>

    <?= $form->field($model, 'mulai', ['options' => ['tag' => false]])->hiddenInput(['value' => $data['dataPost']['jam']])->label(false) ?>

    <?= $form->field($model, 'selesai')->widget(Select2::classname(), [
        'data' => ArrayHelper::map(Jam::find()->where(['>', 'id', $data['dataPost']['jam']])->all(), 'id', 'jam'),
        'options' => ['placeholder' => Yii::t('app', '- Select End Time -')],
        'pluginOptions' => [
            'allowClear' => true,
        ],
    ])->label('Jam Selesai') ?>

    <?= $form->field($model, 'nama_acara', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => Yii::t('app', 'Enter Event Name')]) ?>

    <?= $form->field($model, 'user_id', ['options' => ['tag' => false]])->hiddenInput(['value' => Yii::$app->user->identity->id])->label(false) ?>

    <?= $form->field($model, 'tema_acara', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => Yii::t('app', 'Enter Event Theme')]) ?>

    <?= $form->field($model, 'ketua_acara', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => Yii::t('app', 'Enter Event Chairman')]) ?>

    <?= $form->field($model, 'nomor_surat', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true, 'placeholder' => Yii::t('app', 'Enter Reference Letter Number')]) ?>

    <?= $form->field($model, 'deskripsi_acara', ['options' => ['tag' => false]])->textArea(['class' => 'form-control', 'maxlength' => true, 'placeholder' => Yii::t('app', 'Enter Event Description')]) ?>

    <?= $form->field($model, 'status_peminjaman', ['options' => ['tag' => false]])->hiddenInput(['value' => 0])->label(false) ?>

    <?= $form->field($model, 'jenis_peminjaman', ['options' => ['tag' => false]])->hiddenInput(['value' => 1])->label(false) ?>

    <?= $form->field($model, 'item', ['options' => ['tag' => false]])->hiddenInput(['value' => $data['item']->id])->label(false) ?>

    <?= $form->field($model, 'tanggal_mulai', ['options' => ['tag' => false]])->hiddenInput(['value' => null])->label(false) ?>

    <?= $form->field($model, 'is_sent', ['options' => ['tag' => false]])->hiddenInput(['value' => null])->label(false) ?>


    <p style="padding-left: 0%;"><small>*Ensure the suitability of the data before submitting a booking proposal</small></p>

    <div class="help-block"></div>

    <label class="fancy-checkbox">
        <input type="checkbox" class='syarat'>
        <span>I agree with <a href="javascript:void(0)" class="syarat-ketentuan">the terms and conditions</a></span>

    </label>

    <div class="help-block"></div>

    <div class="form-group">
        <?= Html::submitButton('<i class="fa fa-save"></i> Submit', ['class' => 'btn btn-success btn-save', 'disabled' => 'disabled', 'id' => 'saveButton']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel"><b>Terms and Conditions</b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>

            <div class="modal-body">

                <div class="col-md-12">
                    <div class="form-group form-float">
                        <div class="form-line">
                            <?= $sarpras->peraturan_umum ?>
                        </div>

                    </div>

                </div>

                <p style="padding-left: 3%;"><small>*Terms and Conditions</small></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><i class="fa fa-window-close"></i> Close</button>
            </div>

        </div>
    </div>
</div>

<?php JSRegister::begin() ?>
<script>
    $(document).on("change", ".syarat", function(e) {
        e.preventDefault()
        var isChecked = $(".syarat").is(":checked");

        if (isChecked == true) $('.btn-save').prop('disabled', false);
        if (isChecked == false) $('.btn-save').prop('disabled', true);

    });

    $(document).on("click", ".syarat-ketentuan", function(e) {
        e.preventDefault()

        $("#exampleModal").modal("show");

    });

    $(document).ready(function() {
        $('#saveButton').click(function(event) {
            event.preventDefault();
            var jam_autocancel = $('#jam_autocancel').val();
            var jam_autocancel = jam_autocancel.substr(0, 5);
            text = 'Please communicate with the infrastructure facilities staff for approval !!, if your Application is not processed until ' + jam_autocancel + ', it will be automatically cancelled';

            Swal.fire({
                title: 'Caution !',
                text: text,
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Submit'
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#saveButton').removeAttr('disabled');
                    $('#saveButton').closest('form').submit();
                }
            });
        });
    });
</script>
<?php JSRegister::end() ?>