<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Prodi */

$this->title = Yii::t('app', 'Update Program Study: ' . $model->id);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Program Study'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="panel-body">
                <?= $this->render('_form', [
                    'model' => $model,
                ]) ?>
            </div>
        </div>
    </div>
</div>