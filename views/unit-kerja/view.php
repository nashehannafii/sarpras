<?php

use richardfan\widget\JSRegister;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\UnitKerja */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Divissions'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
                <?= Html::a('<i class="fa fa-send"></i> Create & Send Account', 'javascript:void(0)', ['class' => 'btn btn-primary', 'id' => 'btn-send', 'data-unit' => $model->id]) ?>

                <form action="" id="form-unit">
                    <?= Html::hiddenInput('id', $model->id, ['id' => 'id_unit']) ?>
                </form>
                
            </div>

            <div class="panel-body ">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'id',
                        // 'parent_id',
                        // 'jenjang_id',
                        // 'is_sent',
                        'jenis',
                        'nama',
                        'singkatan',
                        // 'surat',
                        // 'kode_prodi',
                        'email:email',
                        'penanggung_jawab',
                    ],
                ]) ?>

            </div>
        </div>

    </div>
</div>

<?php JSRegister::begin() ?>
<script>
    $(document).on("click", "#btn-send", function(e) {
        e.preventDefault();

        var obj = $("#form-unit").serialize()

        $.ajax({
            url: "/unit-kerja/ajax-send",
            type: "POST",
            async: true,
            data: obj,
            error: function(e) {
                console.log(e.responseText)
            },
            beforeSend: function() {
                Swal.fire({
                    title: "Please wait",
                    html: "Sending Email..",

                    allowOutsideClick: false,
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    },

                })
            },
            success: function(data) {
                Swal.close()
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {
                    Swal.fire({
                        title: 'Yeay!',
                        icon: 'success',
                        text: hasil.message
                    }).then(res => {
                        window.location.reload()
                    });

                    $.pjax.reload({
                        container: "#pjax-container"
                    });
                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    })
                }
            }
        })
    });
</script>
<?php JSRegister::end() ?>