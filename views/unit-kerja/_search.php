<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\UnitKerjaSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="unit-kerja-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'parent_id') ?>

    <?= $form->field($model, 'jenjang_id') ?>

    <?= $form->field($model, 'is_sent') ?>

    <?php // echo $form->field($model, 'jenis') ?>

    <?php // echo $form->field($model, 'nama') ?>

    <?php // echo $form->field($model, 'singkatan') ?>

    <?php // echo $form->field($model, 'surat') ?>

    <?php // echo $form->field($model, 'kode_prodi') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'penanggung_jawab') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
