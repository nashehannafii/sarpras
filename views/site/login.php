<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';

//$img= Url::to("frontend/web/Images/logo_kamp.png");
// $BaseUrl=Yii::$app->urlManager->baseUrl."/Images/logo_kamp.png";

//$img= Url::to(Yii::getAlias('@frontend')."/web/Images/logo_kamp.png");
//Yii::setAlias('@logo_unida',$img);
// $img = Yii::$app->params['front'].'/Images/logo_kamp.png';
//print_r(Yii::getAlias('@logo_unida'));exit;
?>
<style>
    .clearfix {
        text-align: left;
        float: left;
        /* margin: 10px; */
    }
</style>

<!-- WRAPPER -->
<div id="wrapper">
    <div class="vertical-align-wrap">
        <div class="vertical-align-middle">
            <div class="auth-box ">
                <div class="left">
                    <div class="content">

                        <div class="header">
                            <div class="logo text-center">
                                <h2><strong>SARPRAS</strong></h2>
                            </div>
                            <p class="lead">Register your account</p>
                        </div>
                        <?php $form = ActiveForm::begin(['class' => "form-auth-small", 'id' => 'login-form']); ?>
                        <div class="form-group">
                            <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'placeholder' => 'Username or Email'])->label(false) ?>
                        </div>
                        <div class="form-group">
                            <?= $form->field($model, 'password')->passwordInput(['placeholder' => 'Password'])->label(false) ?>
                        </div>
                        <!-- <div class="form-group clearfix">
                            <label class="fancy-checkbox element-left">
                                <input type="checkbox">
                                <span>Remember me</span>
                            </label>
                        </div> -->
                        <button type="submit" class="btn btn-primary btn-lg btn-block">LOGIN</button>
                        <div class="bottom">
                            <span class="helper-text"><a href="/site/request-password-reset">Forgot your password?</a></span>
                        </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
                <div class="right">
                    <div class="overlay"></div>
                    <div class="content text">
                        <h1 class="heading">SARPRAS Login</h1>
                        <p>Develop by PPTIK UNIDA</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>
<!-- END WRAPPER -->