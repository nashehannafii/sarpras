<?php

use app\helpers\MyHelper;
use kartik\password\PasswordInput;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Pendaftaran SARPRAS');
// setlocale(LC_ALL, 'id_ID', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID');

$is_open = false;
if (!empty($periode)) {
    $sd = strtotime($periode->tanggal_buka);
    $ed = strtotime($periode->tanggal_tutup);
    $today = strtotime(date('Y-m-d H:i:s'));
    // print_r($sd);
    // print_r($today);
    // exit;
    $is_open = $today >= $sd && $today <= $ed;
}
?>

<div id="wrapper">
    <div class="vertical-align-wrap">
        <div class="vertical-align-middle">
            <div class="auth-box lockscreen clearfix">
                <div class="content">

                    <div class="header">
                        <div class="logo text-center">
                            <h2><strong>SARPRAS</strong></h2>
                        </div>
                        <p class="lead">Register your account</p>
                    </div>
                    <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>

                    <?= $form->field($user, 'username')->textInput(
                        ['placeholder' => Yii::t('app', 'Create your username'), 'autofocus' => true]
                    )->label(false) ?>

                    <?= $form->field($user, 'email')->input('email', ['placeholder' => Yii::t('app', 'Enter your e-mail')])->label(false) ?>

                    <?= $form->field($user, 'password')->widget(
                        PasswordInput::classname(),
                        ['options' => ['placeholder' => Yii::t('app', 'Create your password')]]
                    )->label(false) ?>


                    <div class="form-group">
                        <?= Html::submitButton(
                            Yii::t('app', 'Signup'),
                            ['class' => 'btn btn-primary btn-block', 'name' => 'signup-button']
                        ) ?>
                    </div>
                    <div class="bottom text-center">
                        <span class="helper-text">Already have an account? <a href="/site/login">Login</a></span>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>