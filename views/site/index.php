<?php

use app\assets\HighchartAsset;
use app\helpers\MyHelper;
use app\models\gedung;
use app\models\Jam;
use richardfan\widget\JSRegister;
use dosamigos\chartjs\ChartJs;
use kartik\date\DatePicker;
use kartik\depdrop\DepDrop;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

HighchartAsset::register($this);

/* @var $this yii\web\View */
setlocale(LC_ALL, 'id_ID', 'id_ID.UTF-8', 'id_ID.8859-1', 'id_ID', 'IND.UTF8', 'IND.UTF-8', 'IND.8859-1', 'IND', 'Indonesian.UTF8', 'Indonesian.UTF-8', 'Indonesian.8859-1', 'Indonesian', 'Indonesia', 'id', 'ID', 'en_US.UTF8', 'en_US.UTF-8', 'en_US.8859-1', 'en_US', 'American', 'ENG', 'English');

$this->title = Yii::t('app', 'Home page');


$gedungs = gedung::find()->select('nama')->column();

$rangeDate = [];

foreach (MyHelper::listRangeDate() as $key => $value) {
    $rangeDate[$key] = $key;
}

?>

<style>
    .myInput {
        background-position: 10px 10px;
        background-repeat: no-repeat;
        width: 100%;
        font-size: 15px;
        padding: 6px 10px 4px 10px;
        border: 1px solid #ddd;
        margin-bottom: 12px;
        cursor: pointer;
    }

    .highcharts-figure,
    .highcharts-data-table table {
        min-width: 320px;
        max-width: 660px;
        margin: 1em auto;
    }

    .highcharts-data-table table {
        font-family: Verdana, sans-serif;
        border-collapse: collapse;
        border: 1px solid #ebebeb;
        margin: 10px auto;
        text-align: center;
        width: 100%;
        max-width: 500px;
    }

    .highcharts-data-table caption {
        padding: 1em 0;
        font-size: 1.2em;
        color: #555;
    }

    .highcharts-data-table th {
        font-weight: 600;
        padding: 0.5em;
    }

    .highcharts-data-table td,
    .highcharts-data-table th,
    .highcharts-data-table caption {
        padding: 0.5em;
    }

    .highcharts-data-table thead tr,
    .highcharts-data-table tr:nth-child(even) {
        background: #f8f8f8;
    }

    .highcharts-data-table tr:hover {
        background: #f1f7ff;
    }
</style>

<h3 class="page-title"><strong><?= Yii::t('app', 'Universitas Darussalam Gontor Facility Booking System') ?></strong></h3>

<div class="row">

    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Yii::t('app', 'Monthly summary') ?></h3>
            </div>
            <div class="panel-body">

                <div class="month-filter-bar">
                    <?= Select2::widget([
                        'name' => 'filter_bar',
                        'data' => MyHelper::getYears(),
                        'value' => 1,
                        'options' => [
                            'class' => 'form-control',
                            'prompt' => Yii::t('app', 'Select Period'),
                            'id' => 'filter_bar'
                        ],
                    ]); ?>
                </div>


                <div class="chart-container">
                    <div id="container-highchart-4" style="min-width: 310px; height: 400px; max-width: 1600px; margin: 0 auto"></div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Yii::t('app', 'Top Rank') ?></h3>
            </div>
            <div class="panel-body">
                <div class="month-filter-top-rank">
                    <?= Select2::widget([
                        'name' => 'filter_date_top_rank',
                        'data' => MyHelper::getYears(),
                        'options' => [
                            'class' => 'form-control',
                            'prompt' => Yii::t('app', 'Select Period'),
                            'id' => 'filter_date_top_rank'
                        ],
                    ]); ?>
                </div>

                <div class="panel-body">
                    <div class="card">
                        <div class="card-heading">
                            <h4><?= Yii::t('app', 'Most borrowed room') ?></h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover" id="table-top-room">
                                    <thead>
                                        <tr>
                                            <th><?= Yii::t('app', 'Rank') ?></th>
                                            <th><?= Yii::t('app', 'Room') ?></th>
                                            <th><?= Yii::t('app', 'Venue') ?></th>
                                            <th><?= Yii::t('app', 'Frequency') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-heading">
                            <h4><?= Yii::t('app', 'Unit the most borrowing') ?></h4>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover" id="table-top-user">
                                    <thead>
                                        <tr>
                                            <th><?= Yii::t('app', 'Rank') ?></th>
                                            <th><?= Yii::t('app', 'Unit') ?></th>
                                            <th><?= Yii::t('app', 'Frequency') ?></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Yii::t('app', 'Venue Percentage Usage') ?></h3>
            </div>
            <div class="panel-body">
                <div class="col-sm-9">
                    <div class="month-filter-pie">
                        <?= Select2::widget([
                            'name' => 'filter_month_pie',
                            'data' => array_reverse($months),
                            // 'value' => 0,
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                            'options' => [
                                'class' => 'form-control',
                                'prompt' => Yii::t('app', 'Select Period'),
                                'id' => 'filter_month_pie'
                            ],
                        ]); ?>
                    </div>
                    <div class="date-filter-pie">
                        <input type="text" id="filter-date-pie" class="myInput" placeholder="<?= Yii::t('app', 'Custom range') ?>">
                    </div>

                </div>
                <div class="col-sm-3">
                    <a href="javascript:void(0)" class="btn btn-md btn-primary" id="exchange-pie"><i class="fa fa-exchange"></i></a>
                </div>

                <div class="col-sm-12">
                    <div class="chart-container">
                        <figure class="highcharts-figure">
                            <div id="container"></div>
                            <p class="highcharts-description">
                                <?= Yii::t('app', 'Pie chart where the individual slices can be clicked to expose more detailed data.') ?>
                            </p>
                        </figure>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-6">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Yii::t('app', 'Upcoming Events') ?></h3>
            </div>
            <div class="panel-body">
                <div class="col-sm-9">
                    <div class="month-filter-list">
                        <?= Select2::widget([
                            'name' => 'filter_date_list',
                            'data' => $rangeDate,
                            'value' => 'One week',
                            'options' => [
                                'class' => 'form-control',
                                'prompt' => Yii::t('app', 'Select Period'),
                                'id' => 'filter_date_list'
                            ],
                            'pluginOptions' => [
                                'allowClear' => true,
                            ],
                        ]); ?>
                    </div>
                    <div class="date-filter-list">
                        <input type="text" id="filter-date-list" class="myInput" placeholder="<?= Yii::t('app', 'Custom range') ?>">
                    </div>

                </div>
                <div class="col-sm-3">
                    <a href="javascript:void(0)" class="btn btn-md btn-primary" id="exchange-list"><i class="fa fa-exchange"></i></a>
                </div>

                <table class="table table-hover" id="table-list-event">
                    <thead>
                        <tr>
                            <th style="text-align: center;"><?= Yii::t('app', 'Event Name') ?></th>
                            <th style="text-align: center;"><?= Yii::t('app', 'Date') ?></th>
                            <th style="text-align: center;"><?= Yii::t('app', 'Venue') ?></th>
                            <th style="text-align: center;"><?= Yii::t('app', 'Room') ?></th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>

                </table>
            </div>
        </div>
    </div>

</div>

<!-- Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h4 class="modal-title" id="filterModalLabel"><b><?= Yii::t('app', 'Date Range Filter') ?></b></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>

            <div class="modal-body">
                <input type="hidden" name="chart" id="jenis_filter">

                <div class="col-md-6">
                    <div class="form-group form-float">
                        <label for="peminjaman_booked_keperluan"><?= Yii::t('app', 'Start Date') ?></label>
                        <div class="form-line">
                            <?= DatePicker::widget([
                                'name' => 'start_filter_date',
                                'id' => 'start_filter_date',
                                'options' => ['placeholder' => 'Select Start Date'],
                                'convertFormat' => true,
                                'pluginOptions' => [
                                    'todayHighlight' => true,
                                    'format' => 'php:d-m-Y',
                                    'autoclose' => true,
                                    'allowClear' => false,
                                    'disabled' => true,
                                ]
                            ]) ?>
                        </div>

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group form-float">
                        <label for="peminjaman_booked_keperluan"><?= Yii::t('app', 'End Date') ?></label>
                        <div class="form-line">
                            <?= DatePicker::widget([
                                'name' => 'end_filter_date',
                                'id' => 'end_filter_date',
                                'options' => ['placeholder' => 'Select End Date'],
                                'convertFormat' => true,
                                'pluginOptions' => [
                                    'todayHighlight' => true,
                                    'format' => 'php:d-m-Y',
                                    'autoclose' => true,
                                    'allowClear' => false,
                                    'disabled' => true,
                                ]
                            ]) ?>
                        </div>

                    </div>
                </div>
                <p style="padding-left: 3%;"><small>*</small></p>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-primary waves-effect btn-save-filter" data-dismiss="modal" disabled><i class="fa fa-save"></i> Submit</button>
            </div>
        </div>

    </div>
</div>

<?php

?>

<?php JSRegister::begin() ?>
<script>
    var obj = new Object();
    obj.month = 0;

    getListEvent('One week');
    getTopRank('<?= date('Y') ?>');
    getBarChart('<?= date('Y') ?>');
    getPieChart(obj)

    let pie = 1

    $(document).ready(function() {
        $('#filter_date_list').on('change', function() {
            var selectedValue = $(this).val();

            $('#table-list-event > tbody').empty();

            getListEvent(selectedValue)
        });

        $('#filter_bar').on('change', function() {
            var tahun = $(this).val();

            getBarChart(tahun)
        });

        $('#filter_date_top_rank').on('change', function() {
            var tahun = $(this).val();

            $('#table-top-room > tbody').empty();
            $('#table-top-user > tbody').empty();

            getTopRank(tahun)
        });
    });

    $(document).ready(function() {
        $('#start_filter_date, #end_filter_date').on('change', function() {
            var mulaiVal = $('#start_filter_date').val();
            var selesaiVal = $('#end_filter_date').val();

            if (mulaiVal && selesaiVal) {
                $('.btn-save-filter').removeAttr('disabled');
            } else {
                $('.btn-save-filter').attr('disabled', 'disabled');
            }
        });
    });

    $(document).ready(function() {
        // Hide the date filters initially
        $('.month-filter-pie').hide();
        $('.month-filter-list').hide();

        $('#exchange-pie').click(function() {
            $('#start_filter_date').val('');
            $('#end_filter_date').val('');

            $('#filter-date-pie').val('');
            $('#filter_month_pie').val(null)

            $('.date-filter-pie').toggle();
            $('.month-filter-pie').toggle();
        });

        $('#exchange-list').click(function() {
            $('#start_filter_date').val('');
            $('#end_filter_date').val('');

            $('#filter-date-list').val('');
            $('#filter_month_list').val(null)

            $('.date-filter-list').toggle();
            $('.month-filter-list').toggle();
        });

    });

    $(document).ready(function() {
        $("#filter-date-pie").on("click", function() {

            $('#jenis_filter').val('pie')
            $("#filterModal").modal("show");
        });


        $("#filter-date-list").on("click", function() {

            $('#jenis_filter').val('list')
            $("#filterModal").modal("show");
        });
    });

    $(document).ready(function() {
        $('.btn-save-filter').on('click', function() {
            var obj = new Object();
            obj.mulai = $('#start_filter_date').val();
            obj.selesai = $('#end_filter_date').val();

            var mulai = $('#start_filter_date').val();
            var selesai = $('#end_filter_date').val();

            var mulaiParts = mulai.split('-');
            var mulaiFormatted = mulaiParts[0] + '/' + mulaiParts[1] + '/' + mulaiParts[2];

            var selesaiParts = selesai.split('-');
            var selesaiFormatted = selesaiParts[0] + '/' + selesaiParts[1] + '/' + selesaiParts[2];

            switch ($('#jenis_filter').val()) {
                case 'pie':
                    $("#filter-date-pie").val(mulaiFormatted + ' - ' + selesaiFormatted)
                    getPieChart(obj);
                    break;

                case 'list':
                    $('#table-list-event > tbody').empty();
                    $("#filter-date-list").val(mulaiFormatted + ' - ' + selesaiFormatted)
                    getListEvent(obj);

                    break;

                default:
                    break;
            }
        })
    })

    $(document).ready(function() {
        $('#filter_month_pie').on('change', function() {
            var obj = new Object();

            obj.month = $(this).val();
            $('.btn-save-filter').attr('disabled', 'disabled');

            getPieChart(obj)
        });
    });

    function getListEvent(obj) {
        $.post('/peminjaman/range', {
            data: obj
        }, function(data) {
            var hasil = $.parseJSON(data)

            var row = '';

            if (hasil.item.length > 0) {

                $.each(hasil.item, function(i, object) {
                    row += '<tr>';
                    row += '<td>' + object.nama + '</td>';
                    row += '<td style="text-align: center;">' + object.tanggal + '</td>';
                    row += '<td>' + object.gedung + '</td>';
                    row += '<td>' + object.ruangan + '</td>';
                    row += '</tr>';

                });
            } else {
                row += '<tr><td colspan="4" style="text-align: center;">Not found</td></tr>'
            }

            $('#table-list-event > tbody').append(row);
        });
    }

    function getTopRank(tahun) {
        $.post('/site/get-top-rank', {
            tahun: tahun
        }, function(data) {
            var hasil = $.parseJSON(data)

            var row = '';

            if (hasil.topRooms.length > 0) {
                let rank = 1;
                $.each(hasil.topRooms, function(i, object) {
                    row += '<tr>';
                    row += '<td>' + rank + '</td>';
                    row += '<td>' + object.nama_ruangan + '</td>';
                    row += '<td>' + object.nama_gedung + '</td>';
                    row += '<td>' + object.peminjaman_count + '</td>';
                    row += '</tr>';

                    rank++;
                });
            } else {
                row += '<tr><td colspan="4" style="text-align: center;">Not found</td></tr>'
            }

            $('#table-top-room > tbody').append(row);

            var row = '';

            let rank = 1
            if (hasil.topUsers.length > 0) {
                $.each(hasil.topUsers, function(i, object) {
                    row += '<tr>';
                    row += '<td>' + rank + '</td>';
                    row += '<td>' + object.nama_unit + '</td>';
                    row += '<td>' + object.peminjaman_count + '</td>';
                    row += '</tr>';

                    rank++
                });
            } else {
                row += '<tr><td colspan="4" style="text-align: center;">Not found</td></tr>'
            }

            $('#table-top-user > tbody').append(row);
        });
    }

    function getBarChart(tahun) {
        $.ajax({
            url: '/site/get-bar-chart',
            type: 'POST',
            data: {
                tahun: tahun
            },
            dataType: 'json',
            success: function(response) {
                var categories = response.categories;
                var dataRekapgedung = response.dataRekapgedung;

                $("#container-highchart-4").highcharts({
                    chart: {
                        type: 'column',
                    },
                    title: {
                        text: '<?= Yii::t('app', 'Monthly booking Activity') ?>'
                    },
                    subtitle: {
                        text: '<?= Yii::t('app', 'Source: All bookings in the year') ?>'
                    },
                    xAxis: {
                        categories: categories,
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: '<?= Yii::t('app', 'Booking summary') ?>'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            '<td style="padding:0"><b>{point.y} times</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0,
                            events: {
                                click: function(event) {
                                    // Handle the series click event here
                                    var seriesName = this.name;
                                    var pointCategory = event.point.category;

                                    var monthYear = "July 2023";


                                    var monthNames = {
                                        'January': '01',
                                        'February': '02',
                                        'March': '03',
                                        'April': '04',
                                        'May': '05',
                                        'June': '06',
                                        'July': '07',
                                        'August': '08',
                                        'September': '09',
                                        'October': '10',
                                        'November': '11',
                                        'December': '12'
                                    };

                                    var monthNumber = monthNames[month];

                                    var convertedDate = year + "-" + monthNumber + "-" + day;

                                    if (convertedDate) {
                                        window.open('<?= Url::to(['peminjaman/detail-peminjaman']) ?>', "_blank")
                                    }

                                }
                            }
                        }
                    },
                    series: dataRekapgedung,
                });
            },
            error: function(xhr, status, error) {
                console.error(error);
            }
        });
    }

    function getPieChart(obj) {

        $.ajax({
            url: '/site/get-pie-chart',
            type: 'POST',
            data: obj,
            dataType: 'json',
            success: function(response) {

                var dataRekapgedung = response.dataRekapgedung
                var dataRekapItem = response.dataRekapItem

                var chart = Highcharts.chart('container', {
                    chart: {
                        type: 'pie'
                    },
                    title: {
                        text: '<?= Yii::t('app', 'Percentage Usage Venue Rooms.') ?> <br>' + response.month,
                        align: 'left'
                    },
                    subtitle: {
                        text: '<?= Yii::t('app', 'Click the slices to view detail items. Source: All Booking Activity') ?>',
                        align: 'left'
                    },

                    accessibility: {
                        announceNewData: {
                            enabled: true
                        },
                        point: {
                            valueSuffix: '%'
                        }
                    },

                    plotOptions: {
                        series: {
                            events: {
                                click: function(event) {
                                    // console.log(chart);
                                    // if (chart.drillUpButton) {
                                    //     console.log(chart);
                                    // }

                                    $.ajax({
                                        url: '/ruangan/ajax-get-ruangan',
                                        type: 'GET',
                                        dataType: 'json',
                                        success: function(data) {
                                            if (data.ruangan.includes(event.point.name)) {
                                                var url = '<?= Url::to(['peminjaman/detail-data?']) ?>';

                                                url += 'nama=' + event.point.name;

                                                if ($(".btn-save-filter").prop("disabled")) {
                                                    var bulan = $('#filter_month').val() ?? 0;

                                                    url += '&bulan=' + bulan;
                                                } else {
                                                    var mulaiVal = $('#start_filter_date').val();
                                                    var selesaiVal = $('#end_filter_date').val();

                                                    url += '&mulai=' + mulaiVal;
                                                    url += '&selesai=' + selesaiVal;
                                                }

                                                window.open(url, "_blank");
                                            }
                                        },
                                        error: function(xhr, status, error) {
                                            console.error(error);
                                        }
                                    });
                                }
                            },
                            borderRadius: 5,
                            dataLabels: {
                                enabled: true,
                                format: '{point.name}: {point.y:.1f}%'
                            }
                        }
                    },

                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
                    },

                    series: [{
                        name: 'Browsers',
                        colorByPoint: true,
                        data: dataRekapgedung
                    }],
                    drilldown: {
                        series: dataRekapItem,
                        click: function(button, breadcrumbs) {
                            // console.log(button);
                        }
                    },

                });

            }
        })
    }
</script>
<?php JSRegister::end() ?>