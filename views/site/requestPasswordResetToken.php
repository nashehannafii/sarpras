<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\PasswordResetRequestForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Request password reset';
$this->params['breadcrumbs'][] = $this->title;
?>


<div id="wrapper">
    <div class="vertical-align-wrap">
        <div class="vertical-align-middle">
            <div class="auth-box lockscreen clearfix">
                <div class="content">

                    <div class="header">
                        <div class="logo text-center">
                            <h2><strong>SARPRAS</strong></h2>
                        </div>
                        <p class="lead">Request password reset</p>

                        <p class="text-center">Please fill out your email. A link to reset password will be sent there.</p>
                    </div>
                    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>


                    <?= $form->field($model, 'email')->input('email', ['placeholder' => Yii::t('app', 'Enter your e-mail')])->label(false) ?>

                    <div class="form-group">
                        <?= Html::submitButton(
                            Yii::t('app', 'Send'),
                            ['class' => 'btn btn-primary btn-block', 'name' => 'signup-button']
                        ) ?>
                    </div>
                    <div class="bottom text-center">
                        <span class="helper-text">Already have an account? <a href="/site/login">Login</a></span>
                    </div>

                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>