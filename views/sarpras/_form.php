<?php

use dosamigos\ckeditor\CKEditor;
use kartik\time\TimePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Sarpras */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sarpras-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kepala_sarpras', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true]) ?>

    <?= $form->field($model, 'jam_autocancel', ['options' => ['tag' => false]])->widget(TimePicker::class, [
        'pluginOptions' => [
            'showMeridian' => false,
            'defaultTime' => '00:00',
        ],
        'readonly' => true,
    ]) ?>

    <?= $form->field($model, 'durasi_autodelete', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'type' => 'number', 'min' => '1', 'max' => '24', 'maxlength' => true]) ?>

    <?= $form->field($model, 'peraturan_umum')->widget(CKEditor::className(), [
        'options' => ['rows' => 6],
        'preset' => 'advance',

    ]); ?>


    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>