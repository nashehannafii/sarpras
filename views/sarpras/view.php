<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Sarpras */

$this->title = "Master Sarpras";
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Sarpras'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?php Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>

            <div class="panel-body ">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'id',
                        'kepala_sarpras',
                        [
                            'attribute' => 'jam_autocancel',
                            'value' => function ($model) {
                                $timeObj = DateTime::createFromFormat('H:i:s', $model->jam_autocancel);
                                $formattedTime = $timeObj->format('H:i');
                                return  Yii::t('app', "At") . " " . $formattedTime;
                            }
                        ],
                        [
                            'attribute' => 'durasi_autodelete',
                            'value' => function ($model) {
                                return Yii::t('app', '{n, plural, one{# Hour} other{# Hours}}', ['n' => $model->durasi_autodelete]);
                            }
                        ],
                        [
                            'attribute' => 'peraturan_umum',
                            'format' => 'raw',
                        ],
                    ],
                ]) ?>

            </div>
        </div>

    </div>
</div>