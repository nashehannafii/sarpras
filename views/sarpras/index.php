<?php

use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SarprasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Master Sarpras');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="panel-body ">

                <p>
                    <?php Html::a('Create Sarpras', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    // 'id',
                    'kepala_sarpras',
                    // 'peraturan_umum',
                    [
                        'attribute' => 'jam_autocancel',
                        'hAlign' => 'center',
                        'value' => function ($model) {
                            $timeObj = DateTime::createFromFormat('H:i:s', $model->jam_autocancel);
                            $formattedTime = $timeObj->format('H:i');
                            return  Yii::t('app', "At") . " " . $formattedTime;
                        }
                    ],
                    [
                        'attribute' => 'durasi_autodelete',
                        'hAlign' => 'center',
                        'value' => function ($model) {
                            return Yii::t('app', '{n, plural, one{# Hour} other{# Hours}}', ['n' => $model->durasi_autodelete]);
                        }
                    ],
                    [
                        'attribute' => 'peraturan_umum',
                        'format' => 'raw',
                    ],
                    [
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::a('<i class="fa fa-pencil"></i> Update', ['update', 'id' => $data->id], ['class' => 'btn btn-md btn-primary']);
                        }
                    ],
                    // ['class' => 'yii\grid\ActionColumn']
                ]; ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>