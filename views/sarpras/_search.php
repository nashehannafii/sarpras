<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SarprasSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sarpras-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'kepala_sarpras') ?>

    <?= $form->field($model, 'peraturan_umum') ?>

    <?= $form->field($model, 'jam_autocancel') ?>

    <?= $form->field($model, 'durasi_autodelete') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
