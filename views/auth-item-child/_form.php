<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\rbac\models\AuthItem;

/* @var $this yii\web\View */
/* @var $model app\models\AuthItemChild */
/* @var $form yii\widgets\ActiveForm */
$listAuth1 = AuthItem::find()->where(['type' => 1])->orderBy(['name' => SORT_ASC])->all();
$listAuth = ArrayHelper::map($listAuth1, 'name', 'name');
?>

<div class="body">

    <?php $form = ActiveForm::begin([
        'options' => [
            'id' => 'form_validation',
        ]
    ]); ?>



    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right">Parent</label>
        <div class="col-sm-9">
            <?= $form->field($model, 'parent', ['options' => ['tag' => false]])->dropDownList($listAuth, ['prompt' => '- pilih parent -'])->label(false) ?>
            
        </div>
    </div>
    
    <div class="form-group">
        <label class="col-sm-3 control-label no-padding-right">Child</label>
        <div class="col-sm-9">
            <?= $form->field($model, 'child', ['options' => ['tag' => false]])->dropDownList($listAuth, ['prompt' => '- pilih child -'])->label(false) ?>


        </div>
    </div>
    <?= Html::submitButton('Save', ['class' => 'btn btn-primary waves-effect']) ?>

    <?php ActiveForm::end(); ?>

</div>