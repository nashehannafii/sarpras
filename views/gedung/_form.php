<?php

use app\helpers\MyHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\gedung */
/* @var $form yii\widgets\ActiveForm */

?>

<style>
    /* file upload button */
    input[type="file"]::file-selector-button {
        border-radius: 4px;
        padding: 0 16px;
        height: 30px;
        cursor: pointer;
        background-color: white;
        border: 1px solid rgba(0, 0, 0, 0.16);
        box-shadow: 0px 1px 0px rgba(0, 0, 0, 0.05);
        margin-right: 16px;
        transition: background-color 200ms;
    }

    /* file upload button hover state */
    input[type="file"]::file-selector-button:hover {
        background-color: #f3f4f6;
    }

    /* file upload button active state */
    input[type="file"]::file-selector-button:active {
        background-color: #e5e7eb;
    }
</style>

<div class="gedung-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nama', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true]) ?>

    <?= $form->field($model, 'keterangan', ['options' => ['tag' => false]])->textInput(['class' => 'form-control', 'maxlength' => true]) ?>

    <?= $form->field($model, 'gambar_path', ['options' => ['tag' => false]])->fileInput(['class' => '', 'maxlength' => true]) ?>

    <?= $form->field($model, 'status_aktif', ['options' => ['tag' => false]])->radioList(MyHelper::getStatusAktif()['filter'], ['value' => 1]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>