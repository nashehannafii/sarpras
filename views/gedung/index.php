<?php

use app\helpers\MyHelper;
use kartik\editable\Editable;
use yii\helpers\Html;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\gedungSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Parent');
$this->params['breadcrumbs'][] = $this->title;
?>


<style>
    .rounded-image {
        width: 100px;
        height: 50px;
        background-size: cover;
        background-position: center;
        border-radius: 10px;
    }

    /* .rounded-image {
        width: 100%;
        height: auto;
        max-width: 100%;
        border-radius: 10px;
    } */
</style>

<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <div class="panel-body ">

                <p>
                    <?= Html::a('<i class="fa fa-plus"></i> Add gedung', ['create'], ['class' => 'btn btn-success']) ?>
                </p>
                <?php
                $gridColumns = [
                    [
                        'class' => 'kartik\grid\SerialColumn',
                        'contentOptions' => ['class' => 'kartik-sheet-style'],
                        'width' => '36px',
                        'pageSummary' => 'Total',
                        'pageSummaryOptions' => ['colspan' => 6],
                        'header' => '',
                        'headerOptions' => ['class' => 'kartik-sheet-style']
                    ],
                    // 'id',
                    [
                        'attribute' => 'gambar_path',
                        'format' => 'raw',
                        'value' => function ($model) {
                            $img = '';
                            $img .= '<div class="rounded-image" style="background-image: url(' . $model->gambar_path . ');"></div>';
                            return $img;
                        }
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'nama',
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => Editable::INPUT_TEXTAREA,
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'keterangan',
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => Editable::INPUT_TEXTAREA,
                        ],
                    ],
                    [
                        'class' => 'kartik\grid\EditableColumn',
                        'attribute' => 'status_aktif',
                        'refreshGrid' => true,
                        'format' => 'raw',
                        'filter' => MyHelper::getStatusAktif()['filter'],
                        'readonly' => !Yii::$app->user->can('admin'),
                        'editableOptions' => [
                            'inputType' => Editable::INPUT_DROPDOWN_LIST,
                            'data' => MyHelper::getStatusAktif()['filter']
                        ],
                        'value' => function ($data) {
                            return MyHelper::getStatusAktif()['label'][$data->status_aktif];
                        },
                    ],
                    [
                        'label' => Yii::t('app', 'Action'),
                        'format' => 'raw',
                        'value' => function ($data) {
                            return Html::a('View', ['view', 'id' => $data->id], ['class' => 'btn btn-sm btn-primary']);
                        },
                    ]
                    // ['class' => 'yii\grid\ActionColumn']
                ]; ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => $gridColumns,
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'headerRowOptions' => ['class' => 'kartik-sheet-style'],
                    'filterRowOptions' => ['class' => 'kartik-sheet-style'],
                    'containerOptions' => ['style' => 'overflow: auto'],
                    'beforeHeader' => [
                        [
                            'columns' => [
                                ['content' => $this->title, 'options' => ['colspan' => 14, 'class' => 'text-center warning']], //cuma satu 
                            ],
                            'options' => ['class' => 'skip-export']
                        ]
                    ],
                    'exportConfig' => [
                        GridView::PDF => ['label' => 'Save as PDF'],
                        GridView::EXCEL => ['label' => 'Save as EXCEL'], //untuk menghidupkan button export ke Excell
                        GridView::HTML => ['label' => 'Save as HTML'], //untuk menghidupkan button export ke HTML
                        GridView::CSV => ['label' => 'Save as CSV'], //untuk menghidupkan button export ke CVS
                    ],

                    'toolbar' =>  [
                        '{export}',

                        '{toggleData}' //uncoment untuk menghidupkan button menampilkan semua data..
                    ],
                    'toggleDataContainer' => ['class' => 'btn-group mr-2'],
                    // set export properties
                    'export' => [
                        'fontAwesome' => true
                    ],
                    'pjax' => true,
                    'bordered' => true,
                    'striped' => true,
                    // 'condensed' => false,
                    // 'responsive' => false,
                    'hover' => true,
                    // 'floatHeader' => true,
                    // 'showPageSummary' => true, //true untuk menjumlahkan nilai di suatu kolom, kebetulan pada contoh tidak ada angka.
                    'panel' => [
                        'type' => GridView::TYPE_PRIMARY
                    ],
                ]); ?>

            </div>
        </div>
    </div>

</div>