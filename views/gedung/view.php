<?php

use app\helpers\MyHelper;
use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\gedung */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Parent'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    .rounded-image {
        width: 200px;
        height: 100px;
        background-size: cover;
        background-position: center;
        border-radius: 10px;
    }
</style>

<div class="block-header">
    <h2><?= Html::encode($this->title) ?></h2>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="panel">
            <div class="panel-heading">
                <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                <?php Html::a('Delete', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>

            <div class="panel-body ">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        // 'id',
                        [
                            'attribute' => 'gambar_path',
                            'format' => 'raw',
                            'value' => function ($model) {
                                $img = '';
                                $img .= '<div class="rounded-image" style="background-image: url(' . $model->gambar_path . ');"></div>';
                                return $img;
                            }
                        ],
                        'nama',
                        'keterangan',
                        [
                            'attribute' => 'status_aktif',
                            'format' => 'raw',
                            'value' => function ($data) {
                                return MyHelper::getStatusAktif()['label'][$data->status_aktif];
                            }
                        ],
                    ],
                ]) ?>

            </div>
        </div>

    </div>
</div>