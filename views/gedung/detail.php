<?php

use app\assets\HighchartAsset;
use app\helpers\MyHelper;
use app\models\gedung;
use app\models\Item;
use app\models\Jam;
use app\models\Peminjaman;
use app\models\Ruangan;
use kartik\date\DatePicker;
use kartik\select2\Select2;
use richardfan\widget\JSRegister;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

HighchartAsset::register($this);

/* @var $this yii\web\View */

$this->title = Yii::t('app', 'Detail ' . $model->nama);
$tanggal = isset($_GET['tanggal']) ? $_GET['tanggal'] : date('Y-m-d');
$gedung = isset($_GET['gedung']) ? $_GET['gedung'] : $model->id;
$gedung = gedung::findOne($gedung);

$tanggal_modify = new DateTime($tanggal);
$tanggal_sebelum = $tanggal_modify->modify('-1 day');
$tanggal_sebelum = $tanggal_sebelum->format('Y-m-d');

$tanggal_setelah = $tanggal_modify->modify('+2 day');
$tanggal_setelah = $tanggal_setelah->format('Y-m-d');

$this->params['breadcrumbs'][] = $this->title;
?>

<style>
    /* .rounded-image {
    width: 600px;
    height: 300px;
    background-image: url();
    background-size: cover;
    background-position: center;
    border-radius: 10px;
  } */
    .rounded-image {
        width: 100%;
        height: auto;
        max-width: 100%;
        border-radius: 10px;
    }

    .tooltip-inner {
        max-width: 300px;
        /* Atur lebar maksimum tooltip */
        white-space: nowrap;
        /* Hindari pemisahan kata */
        overflow: hidden;
        text-align: left;
        /* Sembunyikan konten yang meluap */
        text-overflow: ellipsis;
        /* Tampilkan elipsis (...) jika konten terlalu panjang */
    }
</style>

<div class="row">
    <div class="col-lg-4">
        <div class="panel">
            <div class="panel-heading">
                <?= Html::a('<i class="fa fa-reply"></i> Map', ['peminjaman/map'], ['class' => 'btn btn-xs btn-info']) ?>
                <h3><b><?= $model->nama ?></b></h3>
            </div>
            <div class="panel-body">
                <img src="<?= $model->gambar_path ?>" class="rounded-image" alt="Gambar <?= $model->nama ?>">
                <br><br>

                <table width="100%">
                    <tr>
                        <td width="30%">Nama</td>
                        <td width="70%">: <b><?= $model->nama ?></b></td>
                    </tr>
                    <tr>
                        <td width="30%">Keterangan</td>
                        <td width="70%">: <?= $model->keterangan ?></td>
                    </tr>
                </table>

            </div>
        </div>

    </div>
    <div class="col-lg-8">
        <div class="panel">

            <div class="panel-heading">
                <h3 class="panel-title">Dashboard gedung</h3>
            </div>
            <div class="panel-body">

                <div class="row">
                    <ul class="pager">
                        <li>
                            <h4><b><?= $gedung->nama ?></b></h4>
                        </li>
                    </ul>
                    <ul class="pager">
                        <li class="previous">
                            <?= Html::a('&larr; ' . Yii::t('app', 'Yesterday'), ['gedung/detail', 'id' => $gedung->id, 'tanggal' => $tanggal_sebelum], ['class' => '']); ?>
                        </li>
                        <li class="next">
                            <?= Html::a(Yii::t('app', 'Tomorrow') . ' &rarr;', ['gedung/detail', 'id' => $gedung->id, 'tanggal' => $tanggal_setelah], ['class' => '']); ?>
                        </li>
                    </ul>
                </div>

                <div class="card">
                    <div class="card-body">
                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th style="text-align: center;">Waktu</th>
                                    <?php
                                    $items = Ruangan::find()->where(['gedung_id' => [$gedung->id], 'status_aktif' => 1,])->all();

                                    foreach ($items as $item) :
                                        $label = $listLabel[$item->id]  ?? $item->nama;
                                        $item = Html::a($label . ' | ' . $item->kapasitas, ['ruangan/detail', 'id' => $item->id], ['']);
                                    ?>
                                        <th style="text-align: center;">
                                            <?= $item ?>
                                        </th>
                                    <?php endforeach; ?>
                                    <th style="text-align: center;">Waktu</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach (Jam::find()->orderBy(['urutan' => SORT_ASC])->all() as $jam) : ?>
                                    <tr style="text-align: center;">

                                        <th width="6%" style="text-align: center;"><?= $jam->jam ?></th>
                                        <?php
                                        $jumlah = Ruangan::find()->where([
                                            'status_aktif' => 1,
                                        ])->count();
                                        $hasil = 88 / ($jumlah != null ? $jumlah : 1);

                                        $items = Ruangan::find()->where([
                                            'gedung_id' => [
                                                $gedung->id
                                            ],
                                            'status_aktif' => 1,
                                        ])->all();
                                        foreach ($items as $item) :
                                        ?>

                                            <?php
                                            $peminjamans = Peminjaman::findByItem($item->id, $tanggal, [0, 1])->all();
                                            $style = '';
                                            $avalilable = '';
                                            $avalilable .= '<form action="' . Yii::$app->urlManager->createUrl(['peminjaman/create']) . '" method="post">';
                                            $avalilable .= '<input type="hidden" name="' . Yii::$app->request->csrfParam . '" value="' . Yii::$app->request->csrfToken . '">';
                                            $avalilable .= '<input type="hidden" name="jam" value="' . $jam->id . '">';
                                            $avalilable .= '<input type="hidden" name="ruangan_id" value="' . $item->id . '">';
                                            $avalilable .= '<input type="hidden" name="tanggal" value="' . $tanggal . '">';
                                            $avalilable .= '<button type="submit" class="btn btn-success btn-xs">';
                                            $avalilable .= '<i class="fa fa-home"></i></button></form>';
                                            if (isset($peminjamans)) {
                                                $result = $avalilable;
                                                foreach ($peminjamans as $peminjaman) {

                                                    $pj = $peminjaman->user->unitKerja->singkatan ?? '';

                                                    $tb = '';
                                                    $tb .= '<table><tr><th colspan="4" style="text-align:center;"><h4><b>Detail</b></h4></th></tr>';
                                                    $tb .= '<tr><td><b>Nama Acara </b></td><td>:</td><td> ' . $peminjaman->nama_acara . '</td></tr>';
                                                    $tb .= '<tr><td><b>Unit </b></td><td>:</td><td> ' . $pj . '</td></tr>';
                                                    $tb .= '<tr><td><b>Ketua Acara </b></td><td>:</td><td> ' . $peminjaman->ketua_acara . '</td></tr>';
                                                    $tb .= '<tr><td><b>Status peminjaman </b></td><td>:</td><td> ' . MyHelper::detailPeminjaman()['status_persetujuan'][$peminjaman->status_peminjaman] . '</td></tr>';
                                                    $tb .= '</table>';

                                                    if ($peminjaman->mulai == $jam || $peminjaman->peminjamanItems[0]->selesai->urutan > $jam->urutan && $peminjaman->peminjamanItems[0]->mulai->urutan <= $jam->urutan) {

                                                        if ($peminjaman->status_peminjaman == 0) {

                                                            $result = Html::button('<i class="fa fa-hourglass-half"></i>', [
                                                                'class' => 'btn btn-xs btn-warning btn-booked',
                                                                'data-pinjam' => $peminjaman->id,
                                                                'data' => [
                                                                    'toggle' => 'tooltip',
                                                                    'placement' => 'top',
                                                                    'html' => 'true',
                                                                    'title' => $tb,
                                                                ],
                                                            ]);
                                                        } elseif ($peminjaman->status_peminjaman == 1) {

                                                            $result = Html::button('<i class="fa fa-check-square-o"></i>', [
                                                                'class' => 'btn btn-xs btn-primary btn-booked',
                                                                'data-pinjam' => $peminjaman->id,
                                                                'data' => [
                                                                    'toggle' => 'tooltip',
                                                                    'placement' => 'top',
                                                                    'html' => 'true',
                                                                    // 'title' => '<h4>Booked</h4><p>' . $peminjaman->nama_acara . '</p><p>' . $pj . '</p>',
                                                                    'title' => $tb,
                                                                ],
                                                            ]);
                                                        }
                                                    }
                                                }
                                            } else {
                                                $result = $avalilable;
                                            } ?>

                                            <td width="<?= $hasil ?>%"><?= $result ?></td>

                                        <?php endforeach; ?>

                                        <th width="6%" style="text-align: center;"><?= $jam->jam ?></th>
                                    </tr>
                                <?php endforeach; ?>
                            </tbody>
                            <tbody>
                                <tr style="text-align: center;">
                                    <th style="text-align: center;">Waktu</th>
                                    <?php
                                    $items = Ruangan::find()->where([
                                        'gedung_id' => [
                                            $gedung->id
                                        ],
                                        'status_aktif' => 1,
                                    ])->all();
                                    foreach ($items as $item) :

                                        $label = $listLabel[$item->id]  ?? $item->nama;
                                        $item = Html::a($label . ' | ' . $item->kapasitas, ['ruangan/detail', 'id' => $item->id], ['']);
                                    ?>
                                        <th style="text-align: center;">
                                            <?= $item ?>
                                        </th>
                                    <?php endforeach; ?>
                                    <th style="text-align: center;">Waktu</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Booked Detail</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>

            </div>

            <div class="modal-body">

                <div class="col-md-12">
                    <div class="form-group form-float">
                        <label for="peminjaman_booked_keperluan">Ketua</label>
                        <div class="form-line">
                            <!-- file input with post mode not use model -->
                            <input type="ketua" name="ketua" id="peminjaman_booked_ketua" class="form-control" disabled>
                        </div>

                    </div>

                    <div class="form-group form-float">
                        <label for="peminjaman_booked_keperluan">Unit</label>
                        <div class="form-line">
                            <!-- file input with post mode not use model -->
                            <input type="unit" name="unit" id="peminjaman_booked_unit" class="form-control" disabled>
                        </div>

                    </div>

                    <div class="form-group form-float">
                        <label for="peminjaman_booked_keperluan">Pelindung</label>
                        <div class="form-line">
                            <!-- file input with post mode not use model -->
                            <input type="pelindung" name="pelindung" id="peminjaman_booked_pelindung" class="form-control" disabled>
                        </div>

                    </div>

                    <div class="form-group form-float">
                        <label for="peminjaman_booked_keperluan">Nama Acara/Kegiatan</label>
                        <div class="form-line">
                            <!-- file input with post mode not use model -->
                            <input type="peminjaman" name="peminjaman" id="peminjaman_booked_nama_acara" class="form-control" disabled>
                        </div>

                    </div>

                    <div class="form-group form-float">
                        <label for="peminjaman_booked_keperluan">Status</label>
                        <div class="form-line">
                            <!-- file input with post mode not use model -->
                            <input type="peminjaman" name="status" id="peminjaman_booked_status" class="form-control" disabled>
                        </div>

                    </div>
                </div>

                <p style="padding-left: 3%;"><small>*detail booked data</small></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect" data-dismiss="modal"><i class="fa fa-window-close"></i> Close</button>
            </div>

        </div>
    </div>
</div>

<?php JSRegister::begin() ?>
<script>
    $(document).on("change", "#tanggal-filter, #gedung-filter", function(e) {
        e.preventDefault()
        tanggal = $('#tanggal-filter').val()
        gedung = $('#gedung-filter').val()

        url = '/site/index?'

        if (tanggal != "") url += 'tanggal=' + tanggal
        if (gedung != "") url += '&gedung=' + gedung

        window.location.href = url
    });

    $(document).on("click", ".btn-booked", function(e) {
        e.preventDefault()

        var pinjam = $(this).data("pinjam")


        $.ajax({
            type: 'POST',
            url: '/peminjaman/ajax-get-data',
            data: {
                pinjam: pinjam
            },
            // async: true,
            success: function(data) {
                var hasil = $.parseJSON(data)
                if (hasil.code == 200) {

                    $('#peminjaman_booked_ketua').val(hasil.item.ketua);
                    $('#peminjaman_booked_unit').val(hasil.item.unit);
                    $('#peminjaman_booked_pelindung').val(hasil.item.pelindung);
                    $('#peminjaman_booked_nama_acara').val(hasil.item.nama_acara);
                    $('#peminjaman_booked_status').val(hasil.item.status);

                    $("#exampleModal").modal("show");

                } else {

                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: hasil.message
                    }).then((result) => {
                        if (result.value) {
                            location.reload();
                        }
                    });

                }
            }
        })


    });
</script>
<?php JSRegister::end() ?>