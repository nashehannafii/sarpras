<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RuanganRiwayatSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ruangan-riwayat-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'ruangan_id') ?>

    <?= $form->field($model, 'no_ruangan') ?>

    <?= $form->field($model, 'nama') ?>

    <?= $form->field($model, 'kapasitas') ?>

    <?php // echo $form->field($model, 'gambar_path') ?>

    <?php // echo $form->field($model, 'status_aktif') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
