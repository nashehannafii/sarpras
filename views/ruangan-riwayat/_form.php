<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RuanganRiwayat */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ruangan-riwayat-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'ruangan_id',['options' => ['tag' => false]])->textInput() ?>

    <?= $form->field($model, 'no_ruangan',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true]) ?>

    <?= $form->field($model, 'nama',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true]) ?>

    <?= $form->field($model, 'kapasitas',['options' => ['tag' => false]])->textInput() ?>

    <?= $form->field($model, 'gambar_path',['options' => ['tag' => false]])->textInput(['class'=>'form-control','maxlength' => true]) ?>

    <?= $form->field($model, 'status_aktif',['options' => ['tag' => false]])->textInput() ?>

    <?= $form->field($model, 'created_at',['options' => ['tag' => false]])->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
